# -*- mode: org -*-

* New features
** TODO Make =*itor2nl*= functions return the slided length in cells.
** TODO =<sesh/string.h>= insert null-terminated strings
   : ssize_t sesh_strins (sesh_text_t *, ssize_t len, const char *)
   : ssize_t sesh_wstrins (sesh_text_t *, ssize_t len, const wchar_t *)

** TODO =<sesh/match.h>= match for text, string, regexp
   #+begin_example
   sesh_off_t sesh_matchf_text (sesh_iterator_t *it, sesh_iterator end,
			       ssize_t buflen, const char buf[buflen])

   sesh_off_t sesh_matchb_text (sesh_iterator_t *it, sesh_iterator end,
			       ssize_t buflen, const char buf[buflen])
   #+end_example

   #+begin_example
   sesh_off_t sesh_strmatchf (sesh_iterator_t *it, sesh_iterator end,
			      const char *)

   sesh_off_t sesh_strmatchb (sesh_iterator_t *it, sesh_iterator end,
			      const wchar_t *)
   #+end_example

   Not yet idea for regexps.

** TODO =<sesh/iterate.h>= convenience macro to iterate over a sequence
   #+begin_example
   #define sesh_iterate (it-var, text)

   #define sesh_iterate_from (it-var, text, it-from)
   #define sesh_iterate_to (it-var, text, it-to)
   #define sesh_iterate_range (it-var, it-from, it-to)

   #define sesh_iterate_from_off (it-var, text, off-from)
   #define sesh_iterate_to_off (it-var, text, off-to)
   #define sesh_iterart_off_range (it-var, off-from, off-to)
   #+end_example

** TODO =<sesh/view.h>= Rendering engine
** TODO =<sesh/view-curses.h>= Curses rendering backend
** TODO Transactional text edition
** TODO Access the text commit log

* API breaks
** TODO Use =ssize_t= as return type for =sesh_read= and =sesh_read_s=
   As the return value means a size in bytes and not in cells, =sesh_len_t= is
   not correct.

** TODO Use =ssize_t= instead of =sesh_len_t= in =sesh_insert=
   This function handle size in bytes not in cells.
** TODO Modify full Unicode support
   Currently, we assume that we can handle the complete Unicode table (BMP and
   extra tables).

   I propose to defaultly support only the BMP table and let the user activate
   full Unicode support at the =sesh_make_text=.

** DONE Rename =sesh_create_text= to =sesh_make_text=

** DONE Remove =sesh_widecell= or change its semantic
   As such, the semantic of =sesh_widecell= is not useful. This observation is
   increased by the previous TODO point.

   - Remove this function;
   - Change its semantic: make the read cell converted to UCS4/UTF32.

** DONE Make =sesh_cell= take a =wchar_t *= instead of a =char buf[]=
   All text encoding supported at short or long terms by Sesh should define
   characters which safely fit in a =wchar_t= type.
** DONE Replace =sesh_cellsize_t= by =int8_t=
   I do not think any text encoding could define any character size greater
    than 128.

* Optimizations
** TODO Pass iterator values by const pointer instead of by value
   I do not expect iterator type size to fit in a machine word.

* Cleanups
