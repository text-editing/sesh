# Copyright 2011, 2012 Ludovic Stordeur
#
# This file is part of Sesh.
#
# Sesh is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Sesh is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sesh.  If not, see <http://www.gnu.org/licenses/>.

#+TITLE: Sesh internal reference manual
#+AUTHOR: Ludovic Stordeur
#+EMAIL: ludovic@okazoo.eu

#+STYLE: <link rel="stylesheet" type="text/css" href="org-stylesheet.css" />

#+BEGIN_EXAMPLE
Copyright 2011, 2012 Ludovic Stordeur

This file is part of Sesh.

Sesh is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sesh is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sesh.  If not, see <http://www.gnu.org/licenses/>.
#+END_EXAMPLE

* Piece table
  The core data structure of the Sesh text sequence implementation is a piece
  table as described by Charles Crowley in its reference paper /Data
  Structures for Text Sequences/ from 1998. I personally consider this paper
  as a MUST READ for any people who would like to deal with text sequence
  implementations.

  From this paper, I also used tricks and techniques from:
  - [[http://www.catch22.net/tuts/neatpad][Design & Implementation of a Win32 Text Editor]];
  - [[http://www.abisource.com/wiki/AbiWordDevelopment#PieceTableBackground]] and
    especially: [[http://e98cuenc.free.fr/wordprocessor/piecetable.html]].


  Sesh exposes a clearly defined internal API to handle the piece table. This
  API is defined in =ptable.h=.

  The piece table is implemented using our object-caching allocator to
  allocate piece handles. The use of this allocator should answer the need of
  a high frequency of allocation/deallocation of piece handles.

  Piece handles are currently doubly chained together using a tail queue. The
  tail queue head is implemented by a special piece handle. This last keeps a
  link to the tail of the queue which is also a special piece handle. Both
  these special handles are used to locate respectively the reverse end and
  end of the text sequence.

** Basic definitions
*** =struct phandle= 						       :type:
    Type for a piece handle. It is also used to refer to the piece table
    itself when pointing on the first piece handle.

** Navigating in a piece table
*** =next_phandle= 							:fun:
    : phandle * next_phandle (phandle *)
    Return the next piece handle of the given one. Return NULL if end of the
    tail queue has been reached.

*** =prev_phandle= 							:fun:
    : phandle * prev_phandle (phandle *)
    Return the previous piece handle of the given one. If the given piece
    handle is the first one of the tail queue, return the last piece handle
    (circular approach).

*** =ptable_foreach= 						      :macro:
    : ptable_foreach (ptr, ptable)
    Convenient macro to iterate over all the piece handles of the given piece
    table.

*** =ptable_foreach_piece= 					      :macro:
    : ptable_foreach_piece (ptr, ptable)
    The same than the above one but exclude the first and last piece handles
    which do not refer to regular pieces.

** Modifying the piece table
*** =enqueue_phandle= 							:fun:
    : void enqueue_phandle (struct phandle *ptable, struct phandle *)
    Insert at the end in the given piece table the given piece handle.

*** =insert_phandle_before= 						:fun:
    : void insert_phandle_before (struct phandle *at, struct phandle *phandle)

    Insert the piece handle *phandle* just before *at*.

*** =unlink_phandle= 							:fun:
    : void unlink_phandle (struct phandle *)
    Remove the given piece handle.
    *Warning* The given piece handle cannot be the last of the piece table.

** Modifying the piece table (marker aware approach)
   Functions of the previous section modify the piece table but are not aware
   of markers. This is problematic as piece table mutations require necessary
   markers to be revalidated.

   As a result, there are some wrappers on top of these functions which take
   care of revalidating markers. These new routines should be used instead of
   the ones above. They all take a pointer on the text sequence.

   These functions are declared in =<sesh-int.h>=.

*** =do_text_unlink_phandle= 						:fun:
    : void do_text_unlink_phandle (struct text *, struct phandle *)
    Wrapper on top of =unlink_phandle()=. Moreover, any marker which points on
    the removed piece handle is revalidated on the next piece handle.

*** =do_text_split_phandle= 						:fun:
#+begin_example
    void
    do_text_split_phandle (struct text *, struct phandle *, sesh_ioff_t off)
#+end_example
    Wrapper on top of =split_phandle()=. Moreover, any marker which points on
    the second part of the split (the one which moves on the new piece handle)
    is revalidated on the newly created piece handle.

** Piece accessors
   The core routine is the predicate which indicates you if a given piece
   handle references a file buffer or an add buffer. It should be called first
   before any other related function.

*** =ref_filebuf_p= 							:fun:
    : bool ref_filebuf_p (struct phandle *)
    Return =true= if the given piece handle references a file buffer and
    =false= if it references an add buffer.

*** =FILEBUF= 								:fun:
    : char * FILEBUF (struct phandle *)
    Return a pointer on the file buffer referenced by the given piece
    handle. The piece handle MUST point on the file buffer or behavior is
    undefined.

*** =ADDBUF= 								:fun:
    : vecp_t ADDBUF (p_hdl *)
    Return a vector pointer on the add buffer referenced by the given piece
    handle. The piece handle MUST point on the add buffer or behavior is
    undefined.

* Buffer parsing
  We expose an internal API to easily parse diffent kinds of text buffers
  against different encodings. This API is defined in =<buf.h>=.

  The encoding class =ENC_CLASS_REG= is not handled by this API. The reason is
  that it can be easily handled by clients.

  All these functions take the encoding parser as last argument.

  Last but not least, this API is not piece aware. As a result, iteration
  routines cannot handle piece boundaries. If a pointer (or vector pointer)
  falls at a piece boundary, it gets invalidated. So it is up to the caller to
  revalidate the pointer on the correct piece.

** Safe buffer parsing
*** =cell_parse_buffer= 						:fun:
    : long cell_parse_buffer (long len, const char buf[len], struct enc *)

    Parse the given buffer *buf* of size *len* bytes for the given encoding
    parser. If the buffer is well formed for the given text encoding, return
    the length of the buffer. Else, return the maximal well formed sequence
    length.

** Iterating over a buffer
   These iteration functions assume the given buffer is well formed or their
   behavior is undefined. As a result, they are perfectly suitable for the
   file or add buffer.

*** =filebuf_cell_stepf= 						:fun:
    : void filebuf_cell_stepf (char **, enc *)

    Assuming the given pointer is on the beginning of a cell in a file buffer,
    forward it to the next cell.

*** =filebuf_next_cell= 						:fun:
    : char * filebuf_next_cell (char *, enc *)

    Assuming the given pointer is on the beginning of a cell in a file buffer,
    return a pointer to the next cell.

*** =addbuf_cell_stepf= 						:fun:
    : void addbuf_cell_stepf (vecp_t *, enc *)

    This is the dual of =filebuf_cell_stepf()= for an add buffer.

*** =addbuf_next_cell= 							:fun:
    : vecp_t addbuf_next_cell (vecp_t, enc *)

    This is the dual of =filebuf_next_cell()= for an add buffer.

*** =filebuf_cell_stepb= 						:fun:
    : void filebuf_cell_stepb (char **, enc *)

    Assuming the given pointer is on the last byte of a cell, backward it to
    the beginning of this current cell.

*** =filebuf_cur_cell= 							:fun:
    : char * filebuf_cur_cell (char *, enc *)

    Assuming the given pointer is on the last byte of a cell, return a pointer
    on the beginning of this cell.

*** =addbuf_cell_stepb= 						:fun:
    : addbuf_cell_stepb (vecp_t *, enc *)

    This is the dual of =filebuf_cell_stepb()= for an add buffer.

*** =addbuf_cur_cell= 							:fun:
    : vecp_t addbuf_cur_cell (vecp_t, enc)
    This is the dual of =filebuf_cur_cell()= for an add buffer.

** Cell distance in a buffer
*** =filebuf_cell_rdist= 						:fun:
    : long filebuf_cell_rdist (char *c1, char *c2, enc *)

    Return the relative distance in term of cells between *c1* and *c2*. Both
    these pointers must point on the file buffer in a same piece.

*** =addbuf_cell_rdist= 						:fun:
    : long addbuf_cell_rdist (vecp_t vp1, vecp_t vp2, enc *)
    This is the dual of =addbuf_cell_rdist()= for two vector pointers pointing
    on the add buffer in a same piece.

** Reading a buffer
*** =filebuf_read_cell= 						:fun:
    : sesh_cellsize_t filebuf_read_cell (const char *, len, buf[len], enc *)
    Read one cell in a file buffer at the given pointer. Fill the read cell in
    *buf*. Return the length of the read cell or -1 if the cell is too large
    for *buf*.

*** =addbuf_read_cell= 							:fun:
    : sesh_cellsize_t addbuf_read_cell (vecp_t, len, buf[len], enc *)
    Read one cell in an add buffer at the given vector pointer. Fill the read
    cell in *buf*. Return the length of the read cell of -1 if the cell is too
    large for *buf*.

*** =membuf_cell_len= 							:fun:
    : long membuf_cell_len (long len, const char buf[len], struct enc *)
    Parse the given buffer against the given text encoding. If the buffer is
    well-formed in term of cells, return *len*. Else return an integer
    strictly inferior to *len*. This value matches the length of the maximum
    well-formed sub-buffer of *buf*.

* Vector with persistent iterator
  The add buffer can be efficiently implemented with a vector. However, many
  vector implementations do not provide iterator persistence over vector
  growing. This is inacceptable here as we have to maintain a lot of these
  iterators in piece handles referencing the add buffer.

  That's why we provide an implementation of a vector with persisent
  iterator. This vector interface is defined in the header =<utils/vec.h>=.

** Error handling
*** =vec_errno_t=						       :type:
    Possible error codes which can be thrown by the vector interface.

    - =VEC_SUCCESS= :: operation succeeded;
    - =VEC_EINVAL= :: invalid argument;
    - =VEC_ENOMEM= :: no memory available.

*** =vec_errno= 							:var:
    : extern vec_errno_t vec_errno
    Functions which do not report errors through their returned value store
    the error code in this variable.

** Vector context
   Any vector is necessary owned by exactly one vector context. Reversely, a
   context can own several vectors.

   At a given time, one given context is said to be the current one.

   Any operation dealing with vectors and vector pointers have their defined
   behavior *IF AND ONLY IF* the context owning them is the current one.

*** =create_vec_ctx= 						   :fun:ctor:
    : vec_ctx_t * create_vec_ctx (void)
    Create a new vector context and return a pointer on it. If this is the
    first vector context created, it is automatically made the current one. If
    the vector context could not be created, =NULL= is returned and
    =vec_errno= is set to =VEC_ENOMEM=.

*** =destroy_vec_ctx= 						   :fun:dtor:
    : void destroy_vec_ctx (void)
    Destroy the given vector context if it no longer owns any vector. In such
    case, =VEC_SUCCESS= is returned, else =VEC_EINVAL= is returned.

*** =current_vec_ctx= 					       :fun:accessor:
    : vec_ctx_t * current_vec_ctx (void)
    Return the current vector context.

*** =vec_ctx_switch= 							:fun:
    : void vec_ctx_switch (vec_ctx_t *)
    Make the given vector context the new current one.

** Vector
*** =vec_t= 							       :type:
    Type for a vector.

*** =create_vec= 						   :fun:ctor:
    : =vec_t * create_vec (vec_ctx_t *)
    Create a new vector owned by the given vector context, with an initial
    strictly positive size and return a pointer on it. If the vector could not
    be created, =NULL= is returned and =vec_errno= should be set to
    =VEC_ENOMEM=.

*** =destroy_vec= 						   :fun:dtor:
    : void destroy_vec (vec_t *)
    Destroy the given vector.

** Vector properties
*** =vec_size= 						       :fun:accessor:
    : long vec_size (vec_t *)
    Return the size in bytes of the given vector.

*** =vec_owner= 					       :fun:accessor:
    : vec_ctx_t * vec_owner (vec_t *)
    Return the vector context owning the given vector.

** Changing a vector size
*** =grow_vec= 								:fun:
    : long grow_vec (vec_t *, long size)
    Request the given vector to grow of AT LEAST *size* bytes. Return the
    effective grown size (which is necessary greater-or-equal to *size*).

*** =vec_notify_unused= 						:fun:
    : long vec_notify_unused (vec_t *, long size)
    Notify the given vector that the last *size* bytes are no longer
    used. This lead the vector to shrink of AT MOST *size* bytes. Return the
    effective shrunk size (which is necessary lower-or-equal to *size*).

** Iterator overview
   The vector interface provides an iterator which let you access and modify
   the vector's content. The iterator semantic is completely unsafe regarding to
   vector boundaries. However, the main advantage of this iterator is that it
   does not get invalidated when the vector grows or shrinks.

*** =vecp_t= 							       :type:
    Type for a vector iterator.

*** =vecp_eq_p= 							:fun:
    : bool vecp_eq_p (vecp_t, vecp_t)
    Vector pointers currently cannot be compared using the usual structural
    equality operator (note that a future revision of this interface could
    allow that). So, to compare two given iterators, use this function.

*** =vecp_rdist= 							:fun:
    : long vecp_rdist (vecp_t, vecp_t)
    Return the relative distance between two given iterators.

** Spawning an iterator
*** =vec_begin= 							:fun:
    : vecp_t vec_begin (vec_t *)
    Return an iterator on the beginning of the vector. A vector can never be
    empty. In other words, the beginning of a vector is always defined.

*** =vecp_next= 							:fun:
    : vecp_t vecp_next (vecp_t)
    Return an iterator just after the given one.

*** =vecp_prev= 							:fun:
    : vecp_t vecp_prev (vecp_t)
    Return an iterator just before the given one.

*** =vecp_at_roff= 							:fun:
    : vecp_t vecp_at_roff (vecp_t vp, long roff)
    Return an iterator at =vp + roff=.

** Iterating over a vector.
*** =stepf_vecp= 							:fun:
    : void stepf_vecp (vecp_t)
    Step forward the given iterator.

*** =stepb_vecp= 							:fun:
    : void stepb_vecp (vecp_t)
    Step backward the given iterator.

*** =slide_vecp= 							:fun:
    : void slide_vecp (vecp_t, roff)
    Slide the given iterator of *roff* bytes.

** Writing in a vector
   Writing in a vector is performed with an iterator. You can write everywhere
   in the vector but keep in mind two points:
   - writing is unsafe regarded to vector boundaries;
   - writing in a vector is a replacement operation and not an insertion
     operation.

*** =write_vec= 							:fun:
    : void write_vec (vecp_t, long len, const char buf[len])
    Write *len* bytes from the buffer *buf* at the vector position given by
    the iterator.

** Reading a vector
*** =read_vec= 								:fun:
    : void read_vec (long len, char buf[len], vecp_t)
    Read *len* bytes from the vector position given by the iterator and store
    them in *buf*.

*** =vecp_byte= 							:fun:
    : char vecp_byte (vecp_t)
    Return the byte pointed by the given iterator.

* Object-caching allocator
  This interface, defined in =<utils/obcache.h>=, exposes an object-caching
  memory allocator, as described by Jeff Bonwick in its paper /The Slab
  Allocator: An Object-Caching Kernel Memory Allocator/ (1994).

  From this interface, we added two features:
  - object simple linking :: we allow objects returned by the allocator to be
       simply linked together. This should be a free feature on a decent
       implementation as it relies on an implementation detail;
  - light object handle :: an object returned by the allocator can be
       naturally handled through a native C pointer but we also provide a
       *light object handle* which fits on a 32bits word. This feature relies
       on the notion of *object cache context*: an object cache, when it is
       created can be placed inside an object cache context (OCC). This is not
       mandatory but necessary to be able to use light object handles. An OCC
       can own several object caches but a given object cache can only be
       owned by at most one OCC. At any time, only one OCC can be valid. The
       other are said to be invalid. At a given time, any object cache owned
       by an invalid OCC cannot be reference through a light object handle
       (but still can through a native C pointer).


  The main (and currently only) client of this interface is the Piece Table
  which is the core of the text sequence implementation. This is this client's
  needs which motivated the addiction of these two above features.

  + piece handles will have a high frequency of allocation/deallocation. An
    object caching allocator answers perfectly such need. Indeed,
    object-caching
allocator can be implemented in such way that:
    - allocation/deallocation is highly fast;
    - memory fragmentation is much reduced.
  + piece handles will be allocated in a lot of quantity. As a result, we need
    to take care of the piece handle's size.
    - piece handles will be chained together in tail queue which requires two
      pointers: *object simple linking* feature allows us to save the memory
      cost of one of the pointer;
    - these pointers can be implemented using *light object handle* feature
      which save half of the memory cost against a naïve C-pointers approach.


  Current implementation is very naïve and inefficient. It only aims to
  validate the interface and allows to work on the upper layers of the
  project. A futur implementation will be realized using a slab allocator, as
  described again by Jeff Bonwick in the same paper.
** Error handling
*** =obcache_errno_t= 						       :type:
    Possible error codes returned by the object-caching allocator interface.

*** =obcache_errno= 							:var:
    : extern obcache_errno_t obcache_errno
    When a public function raises an error, error code is either directly
    returned by the function or stored in this global variable.

*** =NULL_OB_HANDLE= 							:def:
    Define an invalid object handle. Such handle cannot be returned by
    =ob_handle()= if the given pointer is valid.

    This does not mean the following properties are true:
    - =NULL = ob_ptr (NULL_OB_HANDLE)=;
    - =NULL_OB_HANDLE = ob_handle (NULL)=.

** Object cache context
*** =obcache_ctx_t= 						       :type:
    Define an object cache context, as returned by =create_obcache_ctx()=.

*** =create_obcache_ctx= 						:fun:
    : obcache_ctx_t * create_obcache_ctx (void)
    Create a new object cache context and return a pointer on it. If this is
    the first object cache context created, it is automatically made valid. If
    the object cache context could not be created, =NULL= is returned and
    =obcache_errno= should be set to =OBCACHE_ENOMEM=.

*** =destroy_obcache_ctx= 						:fun:
    : obcache_errno_t destroy_obcache_ctx (obcache_ctx_t *)
    Destroy the given object cache context if it no longer owns any object
    cache. In such case, =OBCACHE_SUCCESS= is returned, else =OBCACHE_EINVAL=
    is returned.

*** =valid_obcache_ctx= 						:fun:
    : obcache_ctx_t * valid_obcache_ctx (void)
    Return the currently valid object cache context. Return =NULL= if no
    object cache context has yet been created.

*** =obcache_ctx_switch= 						:fun:
    : void obcache_ctx_switch (obcache_ctx_t *)
    Make the given object cache context the new valid one.

** Object cache
*** =obcache_t= 						       :type:
    Define an object cache, as returned by =create_obcache()=.

*** =create_obcache= 							:fun:
#+begin_example
    obcache_t * create_obcache (obcache_ctx_t * ,int obsize, int align,
				void (*ctor)(void *, int size))
#+end_example
    Create a new object cache, storing objects of size *obsize*. When an
    object is allocated, it is constructed using *ctor*.

    If *obcache\_ctx* is not =NULL=, the object cache is created inside the
    given object cache context. The object cache is so said to be
    context-bound to the given context. This allows to use the *light object
    handle* feature, through the functions =next_ob_handle()=,
    =handle_link_ob()=, =ob_ptr()= and =ob_handle()=. Activating this feature
    also invalidates the =next_ob()= and =link_ob()= functions.

    If *obcache\_ctx* is =NULL=, the object cache is said to be context-free.

*** =destroy_obcache= 							:fun:
    : void destroy_obcache (obcache_t *) :
    Destroy the given object cache and all the objects it manages. Any pointer
    or light handle referencing one of this object immediatly gets
    invalidated.

*** =obcache_owner= 							:fun:
    : obcache_ctx_t * obcache_owner (obcache_t *)
    If the given object cache is context-bound, return the object cache
    context owning the object cache. Else, return =NULL=.

** Allocating and freeing an object
   When a client requests to allocate an object from a cache, this last one
   looks for an available object. If it succeeds to find one, it returns it
   directly (and does not call the constructor on it). In the other case, it
   allocates memory for the given object, then constructs it using the object
   cache's constructor function.

*** =ob_handle_t= 						       :type:
    Type for a light object handle.

*** =obcache_alloc= 							:fun:
    : void * obcache_alloc (obcache_t *)
    Allocate an object from the given object cache and return a pointer on
    it. If the object could not be allocated, =NULL= is returned and
    =obcache_errno= is set to =OBCACHE_ENOMEM=.

*** =obcache_free= 							:fun:
    : void obcache_free (obcache_t *, void *ob)

    Free the object *ob* from the given object cache. Behavior is undefined if
    *ob* does not match an object owned by the given object cache.

** Linking objects together
   Depending whether the objects are owned by a context-bound or context-free
   object cache, the interface to use is different:
   - context-free object cache :: use =next_ob()= and =link_ob()=;
   - context-bound object cache :: use =next_ob_handle()= and
        =handle_link_ob()=. Moreover, the context owning the object cache MUST
        BE valid.

*** =next_ob= 								:fun:
    : void * next_ob (void *ob)

    Return the object pointed by *OB*.

*** =link_ob= 								:fun:
    : void link_ob (void *ob, void *to)

    Make the object *ob* points on the object *to*. If both objects are not
    owned by the same object cache, behavior is undefined.

*** =next_ob_handle= 							:fun:
    : ob_handle_t next_ob_handle (ob_handle_t ob)
    Same as =next_ob()= but for a context-bound object cache.

*** =handle_link_ob= 							:fun:
    : handle_link_ob
    Same as =link_ob()= but for a context-bound object cache.

** Switching between native C pointers and light object handles
   These features are only valid for objects owned by context-bound object
   cache where the bound context is valid.

*** =ob_ptr= 								:fun:
    : void * ob_ptr (ob_handle_t)
    Return the C-pointer associated to the given light object handle.

*** =ob_handle= 							:fun:
    : ob_handle_t ob_handle (void *)
    Return the light object handle associated to the given C-pointer.
