/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <string.h>

#include <sesh/sesh.h>
#include <sesh/text.h>
#include <sesh/marker.h>
#include <sesh/iterator.h>
#include <sesh/line.h>

#include <libutils/vec.h>
#include <libutils/obcache.h>
#include <libutils/queue.h>

#include "sesh-int.h"

bool
sesh_nl_p (sesh_iterator_t it)
{
    struct phandle *phandle = it.phandle;
    struct enc *enc = phandle->owner->enc;

    wchar_t c; // Currently, this is enough.

    int8_t len = sesh_cell (it, &c);
    assert (len <= sizeof (wchar_t));

    return (0 == memcmp (&c, enc->nl, (size_t)enc->nlsize));
}


void
sesh_matchf_itor2nl (sesh_iterator_t *it, sesh_iterator_t end)
{
    for (; ! sesh_itor_eq_p (*it, end); sesh_fwd_itor (it))
	if (sesh_nl_p (*it))
	    break;
}

void
sesh_matchb_itor2nl (sesh_iterator_t *it, sesh_iterator_t end)
{
    for (; ! sesh_itor_eq_p (*it, end); sesh_bwd_itor (it))
	if (sesh_nl_p (*it))
	    break;
}

long
sesh_slide_itor2nl (sesh_iterator_t *it, sesh_iterator_t end, long len)
{
    long i = 0;
    if (len < 0)
	while (true) {
	    sesh_matchb_itor2nl (it, end);

	    if (sesh_itor_eq_p (*it, end))
		break;

	    i--;
	    if (i == len)
		break;
	    else
		sesh_bwd_itor (it);
	}
    else
	while (true) {
	    sesh_matchf_itor2nl (it, end);

	    if (sesh_itor_eq_p (*it, end))
		break;

	    i++;
	    if (i == len)
		break;
	    else
		sesh_fwd_itor (it);
	}

    return i;
}


sesh_errno_t
sesh_matchf_marker2nl (sesh_marker_handle_t m)
{
    sesh_text_t *text = sesh_marker_owner (m);
    sesh_errno_t ret = SESH_SUCCESS;

    sesh_mv_marker (m, NULL, {
	    sesh_iterator_t end = sesh_text_end (text);
	    sesh_matchf_itor2nl (&m, end);
	    if (sesh_itor_eq_p (m, end))
		ret = SESH_EINPOS;
	});

    return ret;
}

sesh_errno_t
sesh_matchb_marker2nl (sesh_marker_handle_t m)
{
    sesh_text_t *text = sesh_marker_owner (m);
    sesh_errno_t ret = SESH_SUCCESS;

    sesh_mv_marker (m, NULL, {
	    sesh_iterator_t rend = sesh_text_rend (text);
	    sesh_matchb_itor2nl (&m, rend);
	    if (sesh_itor_eq_p (m, rend))
		ret = SESH_EINPOS;;
	});

    return ret;
}

long
sesh_slide_marker2nl (sesh_marker_handle_t m, long len)
{
    sesh_text_t *text = sesh_marker_owner (m);
    long res = 0;

    sesh_mv_marker (m, NULL, {
	    sesh_iterator_t rend = sesh_text_rend (text);
	    sesh_iterator_t end = sesh_text_end (text);

	    res = (len < 0) ?
		sesh_slide_itor2nl (&m, rend, len) :
		sesh_slide_itor2nl (&m, end, len);
	});

    return res;
}
