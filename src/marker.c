/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>

#include <libutils/queue.h>
#include <libutils/vec.h>
#include <libutils/obcache.h>

#include "sesh/sesh.h"
#include "sesh/text.h"
#include "sesh/marker.h"
#include "sesh/iterator.h"

#include "sesh-int.h"

#include "alloc.h"
#include "ptable.h"
#include "buf.h"

sesh_errno_t
sesh_destroy_marker (sesh_marker_handle_t m)
{
    struct text *text = m->owner;;

    vec_ctx_switch (text->vec_ctx);
    obcache_ctx_switch (text->obcache_ctx);

    // The point cannot be destroyed.
    if (m == TAILQ_FIRST (text->markers))
	return SESH_EPERM;

    tailq_remove (text->markers, m, qe);

    FREE (m);

    return SESH_SUCCESS;
}


sesh_marker_handle_t
sesh_clone_marker (const sesh_marker_handle_t m)
{
    struct phandle *phandle = m->phandle;
    struct text *text = phandle->owner;

    struct marker *ret = do_text_spawn_marker (text);
    if (NULL == ret)
	return NULL;

    *ret = *m;
    tailq_add_tail (text->markers, ret, qe);

    return ret;
}


sesh_marker_handle_t
sesh_mark_roff (const sesh_marker_handle_t m, sesh_off_t cellroff)
{
    struct text *text = m->owner;

    vec_ctx_switch (text->vec_ctx);
    obcache_ctx_switch (text->obcache_ctx);

    struct sesh_iterator it = sesh_iom (m);

    if (cellroff > 0) {
	struct sesh_iterator end = sesh_text_end (text);
	for (;
	     (cellroff > 0) && (! sesh_itor_eq_p (it, end));
	     sesh_fwd_itor (&it), cellroff--);
    } else {
	struct sesh_iterator rend = sesh_text_rend (text);
	cellroff = -cellroff;
	for (;
	     (cellroff > 0) && (! sesh_itor_eq_p (it, rend));
	     sesh_bwd_itor (&it), cellroff--);
    }

    if (cellroff > 0) {
	sesh_errno = SESH_EINPOS;
	return NULL;
    }

    struct marker *ret = do_text_spawn_marker (text);
    if (NULL == ret)
	return NULL;

    ret->phandle = it.phandle;
    ret->off = it.off;

    tailq_add_tail (text->markers, ret, qe);

    return ret;
}


sesh_off_t
sesh_marker_off (const sesh_marker_handle_t m)
{
    return sesh_itor_rdist (sesh_iom (m), sesh_text_begin (m->owner));
}


sesh_text_t *
sesh_marker_owner (const sesh_marker_handle_t m)
{
    return m->owner;
}


sesh_errno_t
sesh_fwd_marker (sesh_marker_handle_t m)
{
    struct phandle *phandle = m->phandle;
    struct text *text = m->owner;

    if (phandle == prev_phandle (text->ptable))
	return SESH_EINPOS;

    sesh_fwd_itor ((struct sesh_iterator *)m);

    return SESH_SUCCESS;
}

sesh_errno_t
sesh_bwd_marker (sesh_marker_handle_t m)
{
    struct phandle *phandle = m->phandle;
    struct text *text = m->owner;

    if (phandle == text->ptable)
	return SESH_EINPOS;

    sesh_bwd_itor ((struct sesh_iterator *)m);

    return SESH_SUCCESS;
}

sesh_off_t
sesh_slide_marker (sesh_marker_handle_t m, sesh_off_t len)
{
    // OPTIMISATION: ENC_CLASS_REG could be handled faster.

    long i = 0;

    if (len > 0) {
	for (; i < len; i++)
	    if (SESH_EINPOS == sesh_fwd_marker (m))
		break;
    } else
	for (; i > len; i--)
	    if (SESH_EINPOS == sesh_bwd_marker (m))
		break;

    return i;
}


sesh_errno_t
sesh_marker_goto_off (sesh_marker_handle_t m, sesh_off_t celloff)
{
    struct text *text = m->owner;

    if (-1 > celloff)
	return SESH_EINVAL;

    struct marker old_marker = *m;

    m->phandle = text->ptable;
    m->off = 0;

    celloff++;

    if (celloff > sesh_slide_marker (m, celloff)) {
	*m = old_marker;
	return SESH_EINPOS;
    }

    return SESH_SUCCESS;
}

sesh_errno_t
sesh_marker_goto_marker (sesh_marker_handle_t m,
			 const sesh_marker_handle_t m2)
{
    // First check whether *m2* is a sibling of *m*.
    if (m->owner != m2->owner)
	return SESH_ESIBLING;

    m->phandle = m2->phandle;
    m->off = m2->off;

    return SESH_SUCCESS;
}


sesh_iterator_t
sesh_iom (const sesh_marker_handle_t m)
{
    struct sesh_iterator ret = {
	.phandle = m->phandle,
	.off = m->off
    };

    return ret;
}
