/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdarg.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>

#include <minmax.h>

#include <libutils/obcache.h>
#include <libutils/vec.h>
#include <libutils/queue.h>

#include "sesh/sesh.h"
#include "sesh/text.h"
#include "sesh/marker.h"
#include "sesh/iterator.h"

#include "sesh-int.h"
#include "alloc.h"
#include "ptable.h"
#include "buf.h"

vec_ctx_t *text_vec_ctx = NULL;
obcache_ctx_t *text_obcache_ctx = NULL;
/* These are singletons. They are created by sesh_create_text() the first time
 * this function is called. */


static int
create_contexts_once (void)
{
    // Create the vector and object cache contexts if necessary.

    // Create the vector context if necessary.
    if (! text_vec_ctx) {
	text_vec_ctx = create_vec_ctx ();
	if (! text_vec_ctx) {
	    enomem_flag = true;
	    return -1;
	}
    }

    // Create the object cache context if necessary.
    if (! text_obcache_ctx) {
	text_obcache_ctx = create_obcache_ctx ();
	if (! text_obcache_ctx) {
	    enomem_flag = true;
	    return -1;
	}
    }

    return 0;
}


struct phandle *
do_text_spawn_phandle (struct text *text)
{
    /* Create and initialize a new piece handle owned by the given text
     * sequence. */

    struct phandle *ret =
	(struct phandle *)obcache_alloc (text->ptable_cache);

    if (NULL == ret) {
	/* Here, we should try to create another object cache context. Migrate
	 * the existing object cache in this new context (such feature is not
	 * yet offered by the obcache interface), then try to reallocate an
	 * object.  Currently, we just abort. */
	enomem_flag = true;
	return NULL;
    }

    ret->stat = 0;
    ret->celloff = 0;
    ret->len = 0;
    ret->tags = SESH_TAG_NONE;
    ret->piece.filebuf = NULL;
    for (int i = 0; i < MAX_PROPS; ret->props[i] = 0, i++);
    ret->owner = text;

    return ret;
}


static int
init_ptable_head (struct text *text)
{
    /* Initialize the piece table of the given text sequence so that it can be
     * handled as a tail queue. This initialization consists in creating the
     * first piece handle (the one which defines the rend of the sequence). */

    struct phandle *ptable_hd = do_text_spawn_phandle (text);

    ptable_hd->len = 1;
    ptable_hd->celloff = -1; // This one will never change.

    ptable_hd->prev = ob_handle (ptable_hd);
    handle_link_ob (ob_handle (ptable_hd), NULL_OB_HANDLE);

    text->ptable = ptable_hd;

    return 0;
}


static int
create_ptable (struct text *text)
{
    text->ptable_cache =
	create_obcache (text_obcache_ctx, sizeof (struct phandle), 0, NULL);
    // MAYBE: see if we can pass a useful constructor for piece handles.
    if (! text->ptable_cache) {
	/* See the vector creation comment sesh_create_text(). The same thing
	 * applies for the slab cache. */
	enomem_flag = true;
	return -1;
    }

    text->obcache_ctx = text_obcache_ctx;

    if (-1 == init_ptable_head (text))
	goto with_obcache;

    // Create the piece table tail.
    struct phandle *ptable_tl = do_text_spawn_phandle (text);
    if (NULL == ptable_tl)
	goto with_obcache;

    ptable_tl->len = 1; // NOTE: why 1 ???
    enqueue_phandle (text->ptable, ptable_tl);

    return 0;

with_obcache:
    destroy_obcache (text->ptable_cache);

    return -1;
}

static struct enc *
find_enc (const char *encname)
{
    /* Return the encoding parser associated to the given encoding
     * name. Return NULL if no suitable parser has been found. */

    for (int i = 0; i < ENC_NUM; i++)
	if (0 == strcmp (enc_factory[i].name, encname))
	    return &enc_factory[i];

    return NULL;
}

static int
mmap_file (struct text *text, const char *file)
{
    int fd = open (file, O_RDONLY);
    if (-1 == fd)
	switch (errno) {
	case ENOENT: sesh_errno = SESH_ENOENT; return -1;
	case EACCES: sesh_errno = SESH_EPERM; return -1;
	}

    struct stat fdstat;
    assert (0 == fstat (fd, &fdstat));

    text->filebuf_size = fdstat.st_size;

    char *mapping = (char *)mmap (NULL,
				  (size_t)text->filebuf_size,
				  PROT_READ, MAP_PRIVATE, fd, 0);
    if (MAP_FAILED == mapping) {
	sesh_errno = SESH_EINVAL;

	if (ENOMEM == errno)
	    enomem_flag = true;

	goto with_fd;
    }

    text->filebuf = mapping;

    struct phandle *phandle = do_text_spawn_phandle (text);
    phandle->len = text->filebuf_size;
    phandle->piece.filebuf = text->filebuf;
    insert_phandle_before (prev_phandle (text->ptable), phandle);

    return 0;

with_fd:
    assert (0 == close (fd));

    return -1;
}

sesh_text_t *
sesh_make_text (const char *encname, const char *filename)
{
    if (-1 == create_contexts_once())
	return NULL;

    // Create the text sequence.
    struct text *text;
    if (-1 == SESH_ALLOC (text))
	return NULL;

    text->enc = find_enc (encname);
    if (! text->enc) {
	sesh_errno = SESH_ENOENC;
	goto with_text;
    }

    text->valid_celloff = -1;

    text->vec_ctx = text_vec_ctx;
    text->addbuf = create_vec (text->vec_ctx);
    text->addbuf_size = 0;
    if (! text->addbuf) {
	/* Currently, if we cannot create the vector, we completely abort the
	 * creation of the text sequence.
	 *
	 * In the future, we will have to try to create another vector
	 * context, then create the `add buffer' in this context. */
	enomem_flag = true;
	goto with_text;
    }

    // Create the piece table.
    if (-1 == create_ptable (text))
	goto with_addbuf;

    // Initialize the marker list.
    init_tailq_head (text->markers);


    // Create the point of the sequence as a marker.
    struct marker *point = do_text_spawn_marker (text);
    if (! point)
	goto with_ptable;

    point->phandle = prev_phandle (text->ptable); // End of the sequence.

    // The point MUST always be the first element of the marker list.
    tailq_add_tail (text->markers, point, qe);

    if (filename) {
	if (-1 ==  mmap_file (text, filename))
	    goto with_ptable;
    } else {
	text->filebuf = NULL;
	text->filebuf_size = 0;
    }

    return text;

with_ptable:
    destroy_obcache (text->ptable_cache);
with_addbuf:
    destroy_vec (text->addbuf);
with_text:
    FREE (text);

    return NULL;
}


void
sesh_destroy_text (sesh_text_t *text)
{
    tailq_foreach_safe (marker, text->markers, qe)
	FREE (marker);

    destroy_obcache (text->ptable_cache);
    destroy_vec (text->addbuf);

    if (text->filebuf)
	munmap (text->filebuf, (size_t)text->filebuf_size);

    FREE (text);
}


const char *
sesh_text_enc (const sesh_text_t *text)
{
    return text->enc->name;
}


sesh_off_t
sesh_text_size (const sesh_text_t * text)
{
    /* MAYBE: this function is linear in the number of pieces. Perharps could
     * we optimize it by maintaining the size of a text sequence as a text
     * attribute. */

    obcache_ctx_switch (text->obcache_ctx);

    sesh_off_t ret = 0;

    /* We exclude both first and last piece handles as rend/end do not
     * contribute to the text size. */
    ptable_foreach_piece (phandle, text->ptable)
	ret += phandle->len;

    return ret;
}

sesh_off_t
sesh_text_len (const sesh_text_t *ctext)
{
    /* MAYBE: revalidate_pieces_off() is not fast. Perharps could we optimize
     * this function by shortcuting the call to revalidate_pieces_off(). That
     * is, by maintaing the length of a text sequence as a text attribute.
     */

    struct text *text = (struct text *)ctext;
    // This is necessary for revalidate_pieces_off().

    vec_ctx_switch (text->vec_ctx);
    obcache_ctx_switch (text->obcache_ctx);

    struct phandle *ptable_tl = prev_phandle (text->ptable);

    if (ptable_tl->celloff > text->valid_celloff)
	revalidate_phandles_celloff (text);

    return ptable_tl->celloff;
}


sesh_marker_handle_t
sesh_point (const sesh_text_t *text)
{
    struct marker *point = TAILQ_FIRST (text->markers);
    assert (point);
    // Marker list should never be empty as it must contains the point.

    return point;
}


struct marker *
do_text_spawn_marker (struct text *text)
{
    // Create and initialize a marker owned by the given text sequence.

    struct marker *m;
    if (-1 == SESH_ALLOC (m)) {
	enomem_flag = true;
	return NULL;
    }

    m->owner = text;

    return m;
}

static struct phandle *
split_phandle (struct phandle *phandle, sesh_ioff_t off)
{
    /* Split the given piece handle in two contiguous piece handles. The split
     * acts as follows: =[begin - off[= and =[off - end[=. The given piece
     * handle is shrunk to the first part. A new piece handle is created just
     * after the given one to match the second one. This piece handle is
     * returned.
     *
     * As a piece handle cannot have a null size, a special behavior occurs
     * when *off* is 0: no split happen and the given piece handle is returned
     * itself. */

    assert (off < phandle->len);

    // Special case: there is nothing to split here.
    if (off == 0)
	return phandle;

    struct text *text = phandle->owner;

    struct phandle *after = do_text_spawn_phandle (text);
    if (! phandle) {
	enomem_flag = true;
	return NULL;
    }

    *after = *phandle;
    after->len = phandle->len - off;
    phandle->len = off;

    if (ref_filebuf_p (after))
	after->piece.filebuf = phandle->piece.filebuf + off;
    else
	after->piece.addbuf = vecp_at_roff (phandle->piece.addbuf, off);

    insert_phandle_before (next_phandle (phandle), after);

    text->valid_celloff = MIN (text->valid_celloff,
			       prev_phandle (phandle)->celloff);

    return after;
}

struct phandle *
do_text_split_phandle (struct text *text,
		       struct phandle *phandle, sesh_ioff_t off)
{
    struct phandle *after = split_phandle (phandle, off);
    if (! after)
	return NULL;

    // Revalidate necessary markers.
    tailq_foreach (m, text->markers, qe)
	if (m->phandle == phandle &&
	    m->off >= off) {
	    m->phandle = after;
	    m->off -= off;
	}

    return after;
}

void
do_text_unlink_phandle (struct text *text, struct phandle *phandle)
{
    assert (phandle != text->ptable);
    assert (phandle != prev_phandle (text->ptable));

    tailq_foreach (m, text->markers, qe)
	if (m->phandle == phandle) {
	    m->phandle = next_phandle (phandle);
	    m->off = 0;
	}

    unlink_phandle (phandle);
}


sesh_marker_handle_t
sesh_mark_off (sesh_text_t *text, sesh_off_t celloff)
{
    if (-1 > celloff) {
	sesh_errno = SESH_EINVAL;
	return NULL;
    }

    vec_ctx_switch (text->vec_ctx);
    obcache_ctx_switch (text->obcache_ctx);

    struct sesh_iterator it = sesh_text_rend (text);
    struct sesh_iterator end = sesh_text_end (text);

    celloff++;

    for (; (celloff > 0) && (! sesh_itor_eq_p (it, end)); celloff--)
	sesh_fwd_itor (&it);

    if (celloff > 0) {
	sesh_errno = SESH_EINPOS;
	return NULL;
    }

    struct marker *ret = do_text_spawn_marker (text);
    if (! ret)
	return NULL;

    ret->phandle = it.phandle;
    ret->off = it.off;

    tailq_add_tail (text->markers, ret, qe);

    return ret;
}

static vecp_t
addbuf_end (struct text *text)
{
    /* Return a vector pointer on the end of the add buffer of the given text
     * sequence. */

    return vecp_at_roff (vec_begin (text->addbuf), text->addbuf_size);
}

static int
fill_addbuf (struct text *text, long len, const char buf[len])
{
    /* Fill *len* bytes from *buf* in the add buffer of the given text
     * sequence. If not all bytes could be filled, -1 is returned. */

    vecp_t end = addbuf_end (text);

    long vec_req_size = text->addbuf_size + len + 1;
    // The trailing '+1' makes 'addbuf_end()' valid.

    if (vec_size (text->addbuf) < vec_req_size) {
	long vec_growing_req = vec_req_size - vec_size (text->addbuf);
	if (vec_growing_req > grow_vec (text->addbuf, vec_growing_req)) {
	    enomem_flag = true;
	    return -1;
	}
    }

    write_vec (end, len, buf);

    text->addbuf_size += len;

    return 0;
}

sesh_len_t
cell_fill_addbuf (struct text *text, sesh_len_t len, const char buf[len])
{
    long ret = 0;

    switch (text->enc->class) {
    case ENC_CLASS_REG:
    {
	// Regular encoding class is easier to handle.
	long mod = len % text->enc->u.reg;
	if (0 != mod)
	    sesh_errno = SESH_EILSEQ;

	ret = len - mod;

	break;
    }
    default:
	ret = cell_parse_buffer (len, buf, text->enc);

	if (ret < len)
	    sesh_errno = SESH_EILSEQ;
    }

    if (-1 == fill_addbuf (text, ret, buf))
	return -1;


    return ret;
}


sesh_len_t
sesh_insert (sesh_text_t *text, sesh_len_t len, const char buf[len])
{
    struct marker *point = sesh_point (text);
    struct phandle *phandle = point->phandle;

    obcache_ctx_switch (text->obcache_ctx);
    vec_ctx_switch (text->vec_ctx);

    struct phandle *after = do_text_split_phandle (text, phandle, point->off);
    vecp_t vp = addbuf_end (text);

    long ret = cell_fill_addbuf (text, len, buf);

    struct phandle *new_piece = do_text_spawn_phandle (text);
    new_piece->stat |= PHANDLE_ADDBUF_P;
    new_piece->len = ret;
    new_piece->piece.addbuf = vp;

    insert_phandle_before (after, new_piece);

    text->valid_celloff = MIN (text->valid_celloff,
			       (prev_phandle (new_piece))->celloff);

    return ret;
}


sesh_len_t
sesh_erase (sesh_text_t *text, sesh_len_t len)
{
    struct marker *point = sesh_point (text);
    struct phandle *phandle = point->phandle;

    obcache_ctx_switch (text->obcache_ctx);

    if (! do_text_split_phandle (text, phandle, point->off))
	return -1;

    struct sesh_iterator it = sesh_iom (point);
    if (sesh_itor_eq_p (it, sesh_text_rend (text)))
	return 0;

    sesh_bwd_itor (&it);

    long i = 0;
    for (;
	 i < len && (! sesh_itor_eq_p (it, sesh_text_rend (text)));
	 i++, it.phandle->len--, sesh_bwd_itor (&it)) {
	if (it.off == it.phandle->len - 1) {
	    struct phandle *next = next_phandle (it.phandle);
	    if (0 == next->len) {
		do_text_unlink_phandle (text, next);
		obcache_free (text->ptable_cache, next);
	    }
	}
    }

    struct phandle *next = next_phandle (it.phandle);
    if (0 == next->len) {
	do_text_unlink_phandle (text, next);
	obcache_free (text->ptable_cache, next);
    }

    if (sesh_itor_rend_p (text, it))
	text->valid_celloff = -1;
    else
	text->valid_celloff = MIN (text->valid_celloff,
				   (prev_phandle (it.phandle))->celloff);

    return i;
}


#define OP_TAG 1
#define OP_UNTAG 2
#define OP_SET_PROP 3

static void
modify_phandle_attrs (struct phandle *phandle, int8_t op, va_list args)
{
    // Modify the attribute of the given piece handle.

    switch (op) {
    case OP_TAG:
	phandle->tags |= va_arg (args, int);
	break;
    case OP_UNTAG:
	phandle->tags &= ~va_arg (args, int);
	break;
    case OP_SET_PROP:
    {
	sesh_prop_t p = va_arg (args, sesh_prop_t);
	phandle->props[sesh_prop_idx (p)] = va_arg (args, int);
    }
    }
}

static sesh_len_t
modify_cell_attrs (struct marker *m, sesh_len_t len, int8_t op, ...)
{
    /* Generic function on top of which sesh_marker_tag(),
     * sesh_marker_untag() and sesh_marker_set_prop() are implemented. */

    struct phandle *phandle = m->phandle;
    struct text *text = m->owner;

    obcache_ctx_switch (text->obcache_ctx);

    if (! do_text_split_phandle (text, phandle, m->off))
	return -1;

    struct sesh_iterator it = sesh_iom (m);
    struct phandle *tr = NULL;

    long i = 0;
    for (;
	 i < len && (! sesh_itor_eq_p (it, sesh_text_end (text)));
	 i++, sesh_fwd_itor (&it))
	if (0 == it.off) {
	    if (tr) {
		va_list args; va_start (args, op);
		modify_phandle_attrs (tr, op, args);
		va_end (args);
	    }
	    tr = it.phandle;
	}

    struct phandle *after_tag =
	do_text_split_phandle (text, it.phandle, it.off);
    if (! after_tag)
	return -1;

    va_list args; va_start (args, op);
    modify_phandle_attrs (prev_phandle (after_tag), op, args);
    va_end (args);

    return i;
}

sesh_len_t
sesh_tag (sesh_text_t *text, sesh_set_t tagset, sesh_len_t len)
{
    return modify_cell_attrs (sesh_point (text), len, OP_TAG, tagset);
}

sesh_len_t
sesh_untag (sesh_text_t *text, sesh_set_t tagset, sesh_len_t len)
{
    return modify_cell_attrs (sesh_point (text), len, OP_UNTAG, tagset);
}

sesh_len_t
sesh_set_prop (sesh_text_t *text, sesh_prop_t p, int8_t v, sesh_len_t len)
{
    return modify_cell_attrs (sesh_point (text), len, OP_SET_PROP, p, v);
}


sesh_iterator_t
sesh_text_begin (const sesh_text_t *text)
{
    struct sesh_iterator ret = {
	.phandle = next_phandle (text->ptable),
	.off = 0
    };

    return ret;
}

sesh_iterator_t
sesh_text_end (const sesh_text_t *text)
{
    struct sesh_iterator ret = {
	.phandle = prev_phandle (text->ptable),
	.off = 0
    };

    return ret;
}

sesh_iterator_t
sesh_text_rend (const sesh_text_t *text)
{
    struct sesh_iterator ret = {
	.phandle = text->ptable,
	.off = 0
    };

    return ret;
}

sesh_iterator_t
sesh_text_rbegin (const sesh_text_t *text)
{
    struct sesh_iterator ret = sesh_text_end (text);

    sesh_bwd_itor (&ret);

    return ret;
}


bool
sesh_itor_rend_p (const sesh_text_t *text, sesh_iterator_t it)
{
    return it.phandle == text->ptable;
}

bool
sesh_itor_end_p (const sesh_text_t *text, sesh_iterator_t it)
{
    return it.phandle == prev_phandle (text->ptable);
}

sesh_off_t
sesh_itor_off (const sesh_text_t *text, sesh_iterator_t it)
{
    return sesh_itor_rdist (it, sesh_text_begin (text));
}


void
revalidate_phandles_celloff (sesh_text_t *text)
{
    // Revalidate the cell offset of all piece handles.

    struct enc *enc = text->enc;

    long readclen = 0;

    switch (enc->class) {
    case ENC_CLASS_REG:
	ptable_foreach_piece (phandle, text->ptable) {
	    phandle->celloff = readclen;
	    readclen += phandle->len / enc->u.reg;
	}
	break;
    default:
	ptable_foreach_piece (phandle, text->ptable) {
	    phandle->celloff = readclen;

	    if (ref_filebuf_p (phandle)) {
		const char *piece_end = FILEBUF (phandle) + phandle->len;
		readclen += filebuf_cell_rdist (piece_end,
						phandle->piece.filebuf, enc);
	    } else {
		vecp_t piece_end = vecp_at_roff (ADDBUF (phandle),
						 phandle->len);
		readclen += addbuf_cell_rdist (piece_end,
					       phandle->piece.addbuf, enc);
	    }
	}
    }

    (prev_phandle (text->ptable))->celloff = readclen;
    text->valid_celloff = readclen;
}
