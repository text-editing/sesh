/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libutils/obcache.h>
#include <libutils/vec.h>
#include <libutils/queue.h>

#include "sesh/sesh.h"

#include "sesh-int.h"


bool enomem_flag = false;
// Declared in `sesh-int.h'.

enum sesh_errno sesh_errno;
// Store the error code of the last failed operation.


bool
sesh_check_enomem (void)
{
    bool tmp = enomem_flag;

    enomem_flag = false;

    return tmp;
}


int
sesh_prop_idx (sesh_prop_t p)
{
    int idx = -1;
    for (; p > 0; p>>=1, idx++);

    return idx;
}

sesh_attr_t
sesh_attr_of_tag (sesh_tag_t tag)
{
    return tag;
}

sesh_attr_t
sesh_attr_of_prop (sesh_prop_t prop)
{
    return prop << 16;
}

sesh_set_t
sesh_attrset (sesh_set_t tagset, sesh_set_t propset)
{
    return (propset << 16) + tagset;
}
