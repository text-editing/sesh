/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PTABLE_H_
#define PTABLE_H_

static inline bool
ref_filebuf_p (struct phandle *phandle) {
    return !(phandle->stat & PHANDLE_ADDBUF_P);
}

static inline const char *
FILEBUF (struct phandle *phandle)
{
    return phandle->piece.filebuf;
}

static inline vecp_t
ADDBUF (struct phandle *phandle)
{
    return phandle->piece.addbuf;
}



static inline struct phandle *
next_phandle (struct phandle *phandle)
{
    ob_handle_t next = next_ob_handle (ob_handle (phandle));

    return (next == NULL_OB_HANDLE) ? NULL : (struct phandle *)ob_ptr (next);
}

static inline struct phandle *
prev_phandle (struct phandle *phandle)
{
    return (struct phandle *)ob_ptr (phandle->prev);
}

#define ptable_foreach(ptr, ptable)		\
    for (struct phandle *ptr = (ptable);	\
	 ptr != NULL;				\
	 ptr = next_phandle (phandle))

#define ptable_foreach_piece(ptr, ptable)		\
    for (struct phandle *ptr = next_phandle (ptable);	\
	 ptr != prev_phandle (ptable);			\
	 ptr = next_phandle (phandle))

/* #define ptable_foreach_from(ptr, ptable_hd, start)		\
 *     for (struct phandle *ptr = (start);			\
 * 	 ptr != prev_phandle (ptable_hd);			\
 * 	 ptr = next_phandle (phandle)) */



void enqueue_phandle (struct phandle *ptable, struct phandle *phandle);
void insert_phandle_before (struct phandle *at, struct phandle *phandle);
void unlink_phandle (struct phandle *phandle);


#endif //!PTABLE_H_
