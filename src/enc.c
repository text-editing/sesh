/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libutils/queue.h>
#include <libutils/obcache.h>
#include <libutils/vec.h>

#include "sesh/sesh.h"

#include "sesh-int.h"

static int8_t
utf8_cur_cell_size (char c, int lvl)
{
    return (c & 0x80) ? (0x80 | 0x1) : 0x1;
}


static int8_t
utf8_next_cell_size (char c, int lvl)
{
    int8_t ret = 1;

    while (c & 0x80) {
	c <<= 1;
	ret++;
    }

    return ret;
}


static char enc_names[ENC_NUM][32] = { "ASCII", "UTF-8", "UTF-16", "UTF-32" };
// We provide the same encoding names than the iconv() ones.

struct enc enc_factory[] = {
    { .name = enc_names[0],
      .class = ENC_CLASS_REG,
      .minsize = 1,
      .maxsize = 1,
      .nl = "\n",
      .nlsize = 1,
      .u.reg = 1
    }, // ASCII

    { .name = enc_names[1],
      .class = ENC_CLASS_HDCHAR,
      .minsize = 1,
      .maxsize = 4,
      .nl = "\n",
      .nlsize = 1,
      .u.hdchar = {
	    .cur_cell_size = utf8_cur_cell_size,
	    .next_cell_size = utf8_next_cell_size
	}
    }, // UTF-8

    { .name = enc_names[2],
      .minsize = 2,
      .maxsize = 4,
      .class = ENC_CLASS_GENERIC },
    // UTF-16. Not yet implemented.

    { .name = enc_names[3],
      .minsize = 4,
      .maxsize = 4,
      .class = ENC_CLASS_REG,
      .u.reg = 4
    }  // UTF-32
};
