/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <string.h>
#include <minmax.h>

#include <libutils/vec.h>
#include <libutils/queue.h>
#include <libutils/obcache.h>

#include "sesh/sesh.h"

#include "sesh-int.h"
#include "buf.h"


static int
membuf_cell_stepf (long buflen, const char **c, struct enc *enc)
{
    /* Move the pointer to the next cell in the given memory buffer (of size
     * *buflen*) for the given encoding.
     *
     * If the next cell can be reached, 0 is return.
     *
     * If the next cell cannot be reached but the last cell is complete, *c is
     * left just after the input buffer. 0 is return.
     *
     * Else, -1 is returned and *c is undetermined.
     *
     * The encoding class ENC_REGULAR is not handled here. It is up to the
     * calling function to handle it. */

    const char *startpos = *c;

    switch (enc->class) {
    case ENC_CLASS_HDCHAR:
    {
	int8_t len;
	int iter = 0;

	do {
	    if (*c - startpos >= buflen)
		return -1;

	    len = enc->u.hdchar.next_cell_size (**c, iter++);
	    *c += len;
	} while (len & 0x80); // Correct while int8_t == int8_t.
    }
    break;
    case ENC_CLASS_GENERIC:
	  enc->u.generic.cell_stepf (c);
	  if (*c - startpos >= buflen)
	      return -1;
    default: break;
    }

    return 0;
}

void
filebuf_cell_stepf (const char **c, struct enc *enc)
{
    membuf_cell_stepf (enc->maxsize, c, enc);
}


void
addbuf_cell_stepf (vecp_t *vp, struct enc *enc)
{
    switch (enc->class) {
    case ENC_CLASS_HDCHAR:
    {
	int8_t len;
	int iter = 0;

	do {
	    len = enc->u.hdchar.next_cell_size (vecp_byte (*vp), iter++);
	    *vp = vecp_at_roff (*vp, len);
	} while (len & 0x80); // Correct while int8_t == int8_t.
    }
    break;
    case ENC_CLASS_GENERIC:
	enc->u.generic.vecp_cell_stepf (vp);
    default: break;
    }
}


void
filebuf_cell_stepb (const char **c, struct enc *enc)
{
    switch (enc->class) {
    case ENC_CLASS_HDCHAR:
    {
	int8_t len;
	int iter = 0;

	len = enc->u.hdchar.cur_cell_size (**c, iter++);
	while (len & 0x80) { // Correct while int8_t == int8_t.
	    *c -= len;
	    len = enc->u.hdchar.cur_cell_size (**c, iter++);
	}
    }
    break;
    case ENC_CLASS_GENERIC:
	enc->u.generic.cell_stepf (c);
    default: break;
    }
}


void
addbuf_cell_stepb (vecp_t *vp, struct enc *enc)
{
    switch (enc->class) {
    case ENC_CLASS_HDCHAR:
    {
	int8_t len;
	int iter = 0;

	char c;

	read_vec (sizeof(char), &c, *vp);
	len = enc->u.hdchar.cur_cell_size (c, iter++);
	while (len & 0x80) { // Correct while int8_t == int8_t.
	    *vp = vecp_at_roff (*vp, -len);
	    read_vec (sizeof (char), &c, *vp);
	    len = enc->u.hdchar.cur_cell_size (c, iter++);
	}
    }
    break;
    case ENC_CLASS_GENERIC:
	enc->u.generic.vecp_cell_stepb (vp);
    default: break;
    }
}


long
filebuf_cell_rdist (const char *c1, const char *c2, struct enc *enc)
{
    sesh_ioff_t ret = 0;
    if (c2 < c1)
	while (c1 != c2) {
	    c2 = filebuf_next_cell (c2, enc);
	    ret++;
	}
    else
	while (c1 != c2) {
	    c2--;
	    c2 = filebuf_cur_cell (c2, enc);
	    ret++;
	}

    return ret;
}

long
addbuf_cell_rdist (vecp_t vp1, vecp_t vp2, struct enc *enc)
{
    sesh_ioff_t ret = 0;
    if (0 < vecp_rdist (vp1, vp2))
	while (! vecp_eq_p (vp1, vp2)) {
	    vp2 = addbuf_next_cell (vp2, enc);
	    ret++;
	}
    else
	while (! vecp_eq_p (vp1, vp2)) {
	    vp2 = vecp_prev (vp2);
	    vp2 = addbuf_cur_cell (vp2, enc);
	    ret++;
	}

    return ret;
}


int8_t
filebuf_read_cell (const char *c,
		   int8_t len, char buf[len],
		   struct enc *enc)
{
    char *next = filebuf_next_cell (c, enc);
    int8_t cellsize = next - c;
    if (len < cellsize)
	return -1;

    memcpy (buf, c, (size_t)cellsize);

    return cellsize;
}

int8_t
addbuf_read_cell (vecp_t vp,
		  int8_t len, char buf[len],
		  struct enc *enc)
{
    vecp_t next = addbuf_next_cell (vp, enc);
    int8_t cellsize = vecp_rdist (next, vp);
    if (len < cellsize)
	return -1;

    read_vec (cellsize, buf, vp);

    return cellsize;
}

long
cell_parse_buffer (long len, const char buf[len], struct enc *enc)
{
    long ret = 0;
    const char *p = buf;

    while (ret < len) {
	const char *next = p;

	if (-1 == membuf_cell_stepf (len - ret, &next, enc))
	    // Truncated cell.
	    return ret;

	int8_t celllen = next - p;
	ret += celllen;
	p += celllen;
    }

    assert (ret == len);

    return ret;
}
