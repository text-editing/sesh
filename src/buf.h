/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BUF_H_
#define BUF_H_


void		filebuf_cell_stepf (const char **, struct enc *);
static char *	filebuf_next_cell (const char *, struct enc *);

void		addbuf_cell_stepf (vecp_t *, struct enc *);
static vecp_t	addbuf_next_cell (vecp_t, struct enc *);

void		filebuf_cell_stepb (const char **, struct enc *);
static char *	filebuf_cur_cell (const char *, struct enc *);

void		addbuf_cell_stepb (vecp_t *, struct enc *);
static vecp_t	addbuf_cur_cell (vecp_t, struct enc *);

long filebuf_cell_rdist (const char *c1, const char *c2, struct enc *);
long addbuf_cell_rdist (vecp_t vp1, vecp_t vp2, struct enc *);

int8_t filebuf_read_cell (const char *, int8_t len, char buf[len],
			  struct enc *);
int8_t addbuf_read_cell (vecp_t, int8_t len, char buf[len],
			 struct enc *);

long cell_parse_buffer (long len, const char buf[len], struct enc *);


// Inline some functions

static inline char *
filebuf_next_cell (const char *c, struct enc *enc)
{
    filebuf_cell_stepf (&c, enc);
    return (char *)c;
}

static inline vecp_t
addbuf_next_cell (vecp_t vp, struct enc *enc)
{
    addbuf_cell_stepf (&vp, enc);
    return vp;
}

static inline char *
filebuf_cur_cell (const char *c, struct enc *enc)
{
    filebuf_cell_stepb (&c, enc);
    return (char *)c;
}

static inline vecp_t
addbuf_cur_cell (vecp_t vp, struct enc *enc)
{
    addbuf_cell_stepb (&vp, enc);
    return vp;
}


#endif //!BUF_H_
