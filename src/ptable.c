/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <assert.h>

#include <libutils/queue.h>
#include <libutils/vec.h>
#include <libutils/obcache.h>

#include "sesh/sesh.h"

#include "sesh-int.h"
#include "ptable.h"

void
enqueue_phandle (struct phandle *ptable, struct phandle *phandle)
{
    struct phandle *tail = prev_phandle (ptable);

    phandle->prev = ob_handle (tail);
    handle_link_ob (ob_handle (phandle), NULL_OB_HANDLE);

    ptable->prev = ob_handle (phandle);
    handle_link_ob (ob_handle (tail), ob_handle (phandle));
}

void
insert_phandle_before (struct phandle *at, struct phandle *phandle)
{
    phandle->prev = at->prev;
    handle_link_ob (ob_handle (phandle), ob_handle (at));
    handle_link_ob (at->prev, ob_handle (phandle));
    at->prev = ob_handle (phandle);
}

void
unlink_phandle (struct phandle *phandle)
{
    (next_phandle (phandle))->prev = phandle->prev;
    handle_link_ob (phandle->prev, next_ob_handle (ob_handle (phandle)));
}
