/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ALLOC_H_
#define ALLOC_H_

/* alloc.{h|c} files cannot be located under utils/ as they depend on the
 * `memalloc_error_flag' which is declared in sesh-int.h. */

#include <safe-alloc.h>

#include "sesh-int.h"

static inline int
sesh_safe_alloc (void *ptrptr, size_t size)
{
    /* This is a light wrapper on top of Gnulib's 'safe_alloc_alloc_n()'. The
     * only addition is that 'enomem_flag' is set if the underlying allocation
     * fails. */
    int ret = safe_alloc_alloc_n (ptrptr, size, 1, 1);
    if (-1 == ret)
	enomem_flag = true;

    return ret;
}

#define SESH_ALLOC(ptr) \
    sesh_safe_alloc (&(ptr), sizeof (*(ptr)))



#endif //!ALLOC_H_
