/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SESH_INT_H_
#define SESH_INT_H_

struct phandle;
struct enc;

/* struct obcache_ctx;
 * struct obcache; */

struct sesh_marker;


extern bool enomem_flag;
// Defined in `utils/misc.c'.

extern vec_ctx_t *text_vec_ctx;
/* Currently, we use a singleton for the vector context used to handle the
 * text add buffers. So, all text sequences share the same vector context. */

extern obcache_ctx_t *text_obcache_ctx;
/* Like for the vector context, we use a singleton for the object cache
 * context. */


struct text
{
    struct enc *enc;
    // Text encoding parser.

    sesh_off_t valid_celloff;
    /* Cell offset limit under which phandle cell offsets are ensured to be
     * valid. This allows to call revalidate_phandles_celloff() only when
     * necessary. */

    char *filebuf;
    sesh_ioff_t filebuf_size;
    /* The file buffer. It should point on a mmapped file next to a call to
     * sesh_load_file(). */

    vec_ctx_t *vec_ctx;
    vec_t *addbuf;
    int addbuf_size;
    /* The add buffer and its size (this is the used size and not the internal
     * size of the vector (as returned by vec_size()). */

    obcache_ctx_t *obcache_ctx;
    obcache_t *ptable_cache;
    struct phandle *ptable;
    /* We use an object cache to implement the piece table.
     *
     * Piece handles of this table are doubly chained together with a tail
     * queue.
     *
     * This first piece handle is special (and is actually called the Piece
     * Table) as it is not used to locate a usual piece but to locate the rend
     * of the sequence. It also keeps a pointer on another special piece
     * handle used to locate the end of the sequence. Both these special
     * handles allow to handle the piece handle sequence as a tail-queue. */

    tailq_head (struct marker) markers;
    // Markers are chained together with a tail queue.
};

void revalidate_phandles_celloff (struct text *);


struct marker
{
    struct phandle *phandle;
    // The handle of the piece this marker points in.

    sesh_ioff_t off;
    // Offset inside the piece.

    /* IMPORTANT: keep both the above fields the first ones in this
     * order. This allows to safely cast a marker to an iterator. This makes
     * some marker related functions easier to write. */

    tailq_entry (struct marker) qe;

    struct text *owner;
    // Text sequence owning this marker.
};

struct marker * do_text_spawn_marker (struct text *);


enum phandle_tags
{
    PHANDLE_ADDBUF_P = 1
};

struct phandle
{
    ob_handle_t prev;
    /* Piece handles are chained together with a tail queue. We use the link
     * offered by the obcache interface as the next pointer. */

    int8_t stat;

    sesh_ioff_t celloff;
    /* Cell offset of the piece. This is used to greatly speed up routines
     * which need the iterator/marker cell offset. However, this offset gets
     * immediatly invalidated by any editing editing operation perturbing this
     * piece. So, revalidate_pieces_off() MUST be called before accessing any
     * piece cell offset. Implementation of this function is currently linear
     * (due to the use of a tail-queue to link piece handles). This is not
     * very good but should be greatly improved when the tail-queue will be
     * replaced by a binary search tree. */

    sesh_ioff_t len;
    // Length of the piece.

    uint16_t tags;

#   define MAX_PROPS 16
    int8_t props[MAX_PROPS];

    union {
	vecp_t addbuf;
	char *filebuf;
    } piece;

    sesh_text_t *owner;
    // Text owning this piece.
};

struct phandle * do_text_spawn_phandle (struct text *);

void		 do_text_unlink_phandle (struct text *, struct phandle *);
struct phandle * do_text_split_phandle (struct text *,
					struct phandle *, sesh_ioff_t off);

enum enc_class
{
    // Define the encoding specification class.

    ENC_CLASS_REG,
    // All cells have a constant size.

    ENC_CLASS_HDCHAR,
    /* Cell size is determined by parsing the first char. The parser is a
     * function which returns the cell size in byte. Optionally, the parser
     * can indicate that this size is unfinalized. So, we have to reexecute
     * the parser on the char indexed by the returned partial size. This
     * process may occur any number of times. */

    ENC_CLASS_GENERIC
    /* The most generic class. It relies on a parser which receives a pointer
     * to an (filebuf or addbuf) iterator and updates this iterator to the
     * next cell. */
};


struct enc
{
    const char *name;
    // Name of the encoding.

    int8_t minsize;
    int8_t maxsize;
    // Respectively the minimum and maximum possible size of a cell.

    enum enc_class class;

    const char *nl;
    int8_t nlsize;
    // Respectively the newline character and the length of this character.

    union {
	int8_t reg;

	struct {
	    int8_t (*next_cell_size) (char c, int lvl);
	    int8_t (*cur_cell_size) (char c, int lvl);
	} hdchar;

	struct {
	    void (*cell_stepf) (const char **);
	    void (*cell_stepb) (const char **);
	    void (*vecp_cell_stepf) (vecp_t *);
	    void (*vecp_cell_stepb) (vecp_t *);
	} generic;
    } u;

};

#define ENC_NUM	4
// The number of currently defined encodings.

extern struct enc enc_factory[];
// Encoding specification set currently implemented.



#endif //!SESH_INT_H_
