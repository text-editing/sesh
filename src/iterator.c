/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <string.h>

#include <minmax.h>

#include <libutils/obcache.h>
#include <libutils/vec.h>
#include <libutils/queue.h>

#include "sesh/sesh.h"
#include "sesh/iterator.h"

#include "sesh-int.h"
#include "ptable.h"
#include "buf.h"


int8_t
sesh_cell (sesh_iterator_t it, wchar_t *w)
{
    struct text *text = it.phandle->owner;
    struct enc *enc = text->enc;
    int8_t ret;

    vec_ctx_switch (text->vec_ctx);
    obcache_ctx_switch (text->obcache_ctx);

    switch (enc->class) {
    case ENC_CLASS_REG:
	ret = enc->u.reg;

	if (ref_filebuf_p (it.phandle))
	    memcpy ((char *)w, FILEBUF (it.phandle) + it.off, (size_t)ret);
	else
	    read_vec (ret, (char *)w,
		      vecp_at_roff (ADDBUF (it.phandle),it.off));

	break;
    default:
	if (ref_filebuf_p (it.phandle))
	    ret = filebuf_read_cell (FILEBUF (it.phandle) + it.off,
				     sizeof (wchar_t), (char *)w,
				     enc);
	else
	    ret = addbuf_read_cell (vecp_at_roff (ADDBUF (it.phandle),it.off),
				    sizeof (wchar_t), (char *)w,
				    enc);

	assert (-1 != ret);
    }

    return ret;
}


sesh_len_t
sesh_read_s (sesh_len_t buflen, char buf[buflen],
	     sesh_iterator_t it, sesh_len_t len)
{
    sesh_len_t ret = 0;

    for (sesh_len_t i = 0; i < len; i++) {
	int8_t cellsize = sesh_cell (it, (wchar_t *)buf);

	if (cellsize == -SESH_EINLEN)
	    return ret;

	buf += cellsize;
	ret += cellsize;
	sesh_fwd_itor (&it);
    }

    return ret;
}

sesh_len_t
sesh_read (char buf[], sesh_iterator_t it, sesh_len_t len)
{
    sesh_len_t ret = 0;

    for (sesh_len_t i = 0; i < len; i++) {
	int8_t cellsize = sesh_cell (it, (wchar_t *)buf);

	buf += cellsize;
	ret += cellsize;
	sesh_fwd_itor (&it);
    }

    return ret;
}


char
sesh_bytecell (sesh_iterator_t it)
{
    struct text *text = it.phandle->owner;
    char ret;

    vec_ctx_switch (text->vec_ctx);
    obcache_ctx_switch (text->obcache_ctx);

    if (ref_filebuf_p (it.phandle))
	ret = *(FILEBUF (it.phandle) + it.off);
    else {
	ret = vecp_byte (vecp_at_roff (ADDBUF (it.phandle), it.off));
    }

    return ret;
}


void
sesh_fwd_itor (sesh_iterator_t *it)
{
    struct text *text = it->phandle->owner;
    struct enc *enc = text->enc;

    vec_ctx_switch (text->vec_ctx);
    obcache_ctx_switch (text->obcache_ctx);

    /* Special case when the iterator points on the rend. Not very
     * beautiful... */
    if (it->phandle == text->ptable) {
	it->phandle = next_phandle (text->ptable);
	return;
    }

    switch (enc->class) {
    case ENC_CLASS_REG:
	it->off += enc->u.reg;
	break;
    default:
	if (ref_filebuf_p (it->phandle)) {
	    const char *c = FILEBUF (it->phandle) + it->off;
	    c = filebuf_next_cell (c, enc);
	    it->off = c - FILEBUF (it->phandle);
	} else {
	    vecp_t vp = vecp_at_roff (ADDBUF (it->phandle), it->off);
	    vp = addbuf_next_cell (vp, enc);
	    it->off = vecp_rdist (vp, ADDBUF (it->phandle));
	}
    }

    /* If the iterator reached end of the piece, we must revalidate it to the
     * next piece. */
    if (it->off == it->phandle->len) {
	it->phandle = next_phandle (it->phandle);
	it->off = 0;
    }
}


void
sesh_bwd_itor (sesh_iterator_t *it)
{
    struct text *text = it->phandle->owner;
    struct enc *enc = text->enc;

    vec_ctx_switch (text->vec_ctx);
    obcache_ctx_switch (text->obcache_ctx);

    /* First, the iterator must byte-step backward, that is just before the
     * current cell. This can cause the iterator to be revalidated on the
     * previous piece. */
    it->off--;
    if (it->off == -1) { /* FIXME: work while sesh_ioff_t is a signed integral
			  * type. */
	it->phandle = prev_phandle (it->phandle);
	it->off = it->phandle->len - 1;
    }

    /* Special case when the iterator points on the begin. Not very
     * beautiful... */
    if (it->phandle == text->ptable)
	return;

    switch (enc->class) {
    case ENC_CLASS_REG:
	it->off -= (enc->u.reg - 1);
	break;
    default:
	if (ref_filebuf_p (it->phandle)) {
	    const char *c = FILEBUF (it->phandle) + it->off;
	    c = filebuf_cur_cell (c, enc);
	    it->off = c - FILEBUF (it->phandle);
	} else {
	    vecp_t vp = vecp_at_roff (ADDBUF (it->phandle), it->off);
	    vp = addbuf_cur_cell (vp, enc);
	    it->off = vecp_rdist (vp, ADDBUF (it->phandle));
	}
    }
}


void
sesh_slide_itor (sesh_iterator_t *it, sesh_off_t len)
{
    // OPTIMISATION: ENC_CLASS_REG can be processed faster.

    if (len > 0)
	for (long i = 0; i < len; i++)
	    sesh_fwd_itor (it);
    else
	for (long i = 0; i < -len; i++)
	    sesh_bwd_itor (it);
}


int
sesh_cmp_itor (sesh_iterator_t it1, sesh_iterator_t it2)
{
    struct text *text = it1.phandle->owner;
    struct enc *enc = text->enc;

    vec_ctx_switch (text->vec_ctx);
    obcache_ctx_switch (text->obcache_ctx);

    if (it1.phandle->celloff > text->valid_celloff ||
    	it2.phandle->celloff > text->valid_celloff)
	revalidate_phandles_celloff (text);

    if (it1.phandle->celloff > it2.phandle->celloff)
	return 1;
    else if (it1.phandle->celloff < it2.phandle->celloff)
	return -1;

    // At this stage, both iterators point on the same piece.

    switch (enc->class) {
    case ENC_CLASS_REG:
	if (it1.off > it2.off)
	    return 1;
	else if (it1.off < it2.off)
	    return -1;
	else
	    return 0;
	break;
    default:
    {
	sesh_ioff_t it1_celloff;
	sesh_ioff_t it2_celloff;

	if (ref_filebuf_p (it1.phandle))
	    it1_celloff = filebuf_cell_rdist (FILEBUF (it1.phandle) + it1.off,
					      FILEBUF (it1.phandle),
					      enc);
	else
	    it1_celloff =
		addbuf_cell_rdist (vecp_at_roff (ADDBUF(it1.phandle),it1.off),
				   ADDBUF (it1.phandle),
				   enc);

	if (ref_filebuf_p (it2.phandle))
	    it2_celloff = filebuf_cell_rdist (FILEBUF (it2.phandle) + it2.off,
					      FILEBUF (it2.phandle),
					      enc);
	else
	    it2_celloff =
		addbuf_cell_rdist (vecp_at_roff (ADDBUF(it2.phandle),it2.off),
				   ADDBUF (it2.phandle),
				   enc);

	if (it1_celloff > it2_celloff)
	    return 1;
	else if (it1_celloff < it2_celloff)
	    return -1;
	else
	    return 0;
    }
    }
}


sesh_off_t
sesh_matchf_itor2itor (sesh_iterator_t *it1, sesh_iterator_t it2)
{
    sesh_off_t len = 0;
    for (; ! sesh_itor_eq_p (*it1, it2); sesh_fwd_itor (it1), len++);

    return len;
}


sesh_off_t
sesh_matchb_itor2itor (sesh_iterator_t *it1, sesh_iterator_t it2)
{
    sesh_off_t len = 0;
    for (; ! sesh_itor_eq_p (*it1, it2); sesh_bwd_itor (it1), len--);

    return len;
}


sesh_off_t
sesh_itor_rdist (sesh_iterator_t it1, sesh_iterator_t it2)
{
    int cmp = sesh_cmp_itor (it1, it2);
    if (0 < cmp)
	return sesh_matchf_itor2itor (&it2, it1);
    else if (0 > cmp)
	return sesh_matchb_itor2itor (&it2, it1);
    else
	return 0;
}


sesh_set_t
sesh_cell_tags (sesh_iterator_t it)
{
    return it.phandle->tags;
}

int8_t
sesh_cell_prop (sesh_iterator_t it, enum sesh_prop p)
{
    return it.phandle->props[sesh_prop_idx (p)];
}

struct sesh_cellattrs
sesh_cell_attrs (sesh_iterator_t it)
{
    struct sesh_cellattrs ret;

    ret.tags = sesh_cell_tags (it);

    ret.props[sesh_prop_idx (SESH_PROP_FGCOLOR)] =
	sesh_cell_prop (it, SESH_PROP_FGCOLOR);
    ret.props[sesh_prop_idx (SESH_PROP_BGCOLOR)] =
	sesh_cell_prop (it, SESH_PROP_BGCOLOR);
    ret.props[sesh_prop_idx (SESH_PROP_UCOLOR)] =
	sesh_cell_prop (it, SESH_PROP_UCOLOR);
    ret.props[sesh_prop_idx (SESH_PROP_USER1)] =
	sesh_cell_prop (it, SESH_PROP_USER1);
    ret.props[sesh_prop_idx (SESH_PROP_USER2)] =
	sesh_cell_prop (it, SESH_PROP_USER2);

    return ret;
}
