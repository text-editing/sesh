/* Copyright 2011, 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VEC_H_
#define VEC_H_

#include <stdbool.h>

// Implementation details. DO NOT USE THEM.
struct vec_ctx;
struct vec;
struct chunk;
struct vecp
{
    struct chunk *chunk;
    int off;
};

enum vec_errno
{
    VEC_SUCCESS,
    VEC_EINVAL,
    VEC_ENOMEM
};



typedef enum vec_errno vec_errno_t;
extern vec_errno_t vec_errno;


typedef struct vec_ctx vec_ctx_t;
// Type for a vector context.

typedef struct vec vec_t;
// Type for a vector.

typedef struct vecp vecp_t;
// Type for a vector pointer.


vec_ctx_t *	create_vec_ctx (void);
vec_errno_t	destroy_vec_ctx (vec_ctx_t *);

vec_ctx_t *	current_vec_ctx (void);
void		vec_ctx_switch (vec_ctx_t *);

vec_t *	create_vec (vec_ctx_t *);
void	destroy_vec (vec_t *);

long		vec_size (const vec_t *);
vec_ctx_t *	vec_owner (const vec_t *);

long	grow_vec (vec_t *, long size);
long	vec_notify_unused (vec_t *, long size);
/* int	pages_of_bytes (long len); */

vecp_t	vec_begin (const vec_t *);

void		stepf_vecp (vecp_t *);
static vecp_t	vecp_next (vecp_t vp);

void		stepb_vecp (vecp_t *);
static vecp_t	vecp_prev (vecp_t vp);

void		slide_vecp (vecp_t *, long);
static vecp_t	vecp_at_roff (vecp_t vp, long roff);

long vecp_rdist (vecp_t, vecp_t);

void		write_vec (vecp_t, long len, const char buf[len]);
void		read_vec (long len, char buf[len], vecp_t);
static char	vecp_byte (vecp_t vp);

static bool vecp_eq_p (vecp_t, vecp_t);



// Inline some trivial functions
static inline vecp_t
vecp_next (vecp_t vp)
{
    stepf_vecp (&vp);
    return vp;
}

static inline vecp_t
vecp_prev (vecp_t vp)
{
    stepb_vecp (&vp);
    return vp;
}

static inline vecp_t
vecp_at_roff (vecp_t vp, long roff)
{
    slide_vecp (&vp, roff);
    return vp;
}

static inline char
vecp_byte (vecp_t vp)
{
    // Trick: this is an optimized version.
    return *((char *)vp.chunk + vp.off);
}

static inline bool
vecp_eq_p (vecp_t vp1, vecp_t vp2)
{
    return (vp1.chunk == vp2.chunk &&
	    vp1.off == vp2.off);
}

#endif //!VEC_H_
