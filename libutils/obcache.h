/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OBCACHE_H_
#define OBCACHE_H_

// Implementation detail. DO NOT USE THIS.
struct obcache_ctx;
struct obcache;
enum obcache_errno
{
    OBCACHE_SUCCESS,
    OBCACHE_EINVAL,
    OBCACHE_ENOMEM,
};



typedef enum obcache_errno obcache_errno_t;
// Error codes for the object cache interface.

extern enum obcache_errno obcache_errno;

typedef struct obcache_ctx obcache_ctx_t;
// Type for an object cache context.

typedef struct obcache obcache_t;
// Type for an object cache.

typedef void * ob_handle_t;
/* Current naïve implementation makes light object handles have the same
 * memory consumption than a C pointer. A future implementation will allow to
 * shrink this size type to a 32bits word. */

#define NULL_OB_HANDLE ((ob_handle_t) NULL)
// Invalid object handle.


obcache_ctx_t *	create_obcache_ctx (void);
obcache_errno_t	destroy_obcache_ctx (obcache_ctx_t *);

obcache_ctx_t *	valid_obcache_ctx (void);
void		obcache_ctx_switch (obcache_ctx_t *);

obcache_t *	create_obcache (obcache_ctx_t *, int obsize, int align,
				void (*ctor)(void *, int size));
void		destroy_obcache (obcache_t *);

obcache_ctx_t *	obcache_owner (obcache_t *);

void *	obcache_alloc (obcache_t *);
void	obcache_free (obcache_t *, void *);

// Only valid if the concerned object cache is context-free.
void *	next_ob (void *);
void	link_ob (void *, void *to);

/* Only valid if the concerned object cache is context-bound and its
 * associated context is valid. */
ob_handle_t	next_ob_handle (ob_handle_t);
void		handle_link_ob (ob_handle_t, ob_handle_t);

/* Obviously only valid for a context-bound object cache with the associated
 * context valid. */
void *		ob_ptr (ob_handle_t);
ob_handle_t	ob_handle (void *);


#endif //!OBCACHE_H_
