/* Copyright 2011, 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/* Here are defined several generic queue implementations. These
 * implementations are much inspired from the BSD ones which can be found
 * under <sys/queue.h> (see their associated manpage queue(3)). In particular,
 * they are all written using macros to provide their generic flavor. */


/* A simple tail queue is headed by a pair of pointers, one to the head of the
 * list and the other to the tail of the list. The elements are singly linked
 * to save space, so elements can only be removed from the head of the
 * list. New elements can be added to the list after an existing element, at
 * the head of the list, or at the end of the list. A simple queue may only be
 * traversed in the forward direction. (description borrowed from
 * <sys/queue.h>) */


/* A tail queue is headed by a pair of pointers, one to the head of the list
 * and the other to the tail of the list. The elements are doubly linked so
 * that an arbitrary element can be removed without a need to traverse the
 * list. New elements can be added to the list before or after an existing
 * element, at the head of the list, or at the end of the list. A tail queue
 * may be traversed in either direction. */


#ifndef QUEUE_H_
#define QUEUE_H_


// Simple tail queue

#define	stailq_head(type)			\
    struct {					\
	type *first;				\
	type **last;				\
    }

#define	STAILQ_HEAD_LITTERAL(head)		\
    { NULL, &(head).first }

#define STAILQ_FIRST(head) \
    (head).first

#define init_stailq_head(head)					\
    head = (__typeof__ (head)) STAILQ_HEAD_LITTERAL(head)

#define	stailq_add_tail(head, elm, field)	\
    do {					\
	(elm)->field = NULL;			\
	*(head).last = (elm);			\
	(head).last = &(elm)->field;		\
    } while (0)

#define	stailq_foreach(ptr, head, field)			\
    for (__typeof__ ((head).first) ptr = ((head).first);	\
	 ptr;							\
	 ptr = ptr->field)

#define	stailq_foreach_safe(ptr, head, field)				  \
    for (__typeof__ ((head).first) ptr = ((head).first), fw = ptr->field; \
	 ptr;								  \
	 ptr = fw, fw = fw->field)

#define stailq_foreach_safe_from(ptr, elm, field)	\
    for (__typeof__ (elm) ptr = elm, fw = ptr->field;	\
	 ptr;						\
	 ptr = fw, fw = fw->field)



// Tail queue

#define	tailq_head(type)			\
    struct {					\
	type *first;				\
	type **last;				\
    }

#define	tailq_entry(type)			\
    struct {					\
	type *next;				\
	type **prev;				\
    }

#define QE_PREV(elm, field)			\
    *(((tailq_entry(__typeof__ (*elm)) *)((elm)->field.prev))->prev)
#define QE_NEXT(elm, field)			\
    (elm)->field.next

#define	TAILQ_HEAD_LITTERAL(head)		\
    { NULL, &(head).first }

#define TAILQ_FIRST(head)			\
    (head).first
#define TAILQ_LAST(head)			\
    *(((tailq_entry(__typeof__ (*head.first)) *)((head).last))->prev)

#define init_tailq_head(head)					\
    head = (__typeof__ (head)) TAILQ_HEAD_LITTERAL(head)

#define	tailq_add_tail(head, elm, field)	\
    do {					\
	(elm)->field.next = NULL;		\
	(elm)->field.prev = (head).last;	\
	*(head).last = (elm);			\
	(head).last = &(elm)->field.next;	\
    } while (0)

#define	tailq_insert_before(listelm, elm, field)			\
    do {								\
	(elm)->field.prev = (listelm)->field.prev;			\
	(elm)->field.next = (listelm);					\
	*(listelm)->field.prev = (elm);					\
	(listelm)->field.prev = &(elm)->field.next;			\
    } while (0)

#define	tailq_insert_after(head, listelm, elm, field)			\
    do {								\
	if (((elm)->field.next = (listelm)->field.next) != NULL)	\
	    (elm)->field.next->field.prev =				\
		&(elm)->field.next;					\
	else								\
	    (head).last = &(elm)->field.next;				\
	(listelm)->field.next = (elm);					\
	(elm)->field.prev = &(listelm)->field.next;			\
    } while (0)

#define	tailq_remove(head, elm, field)					\
    do {								\
	if (((elm)->field.next) != NULL)				\
	    (elm)->field.next->field.prev =				\
		(elm)->field.prev;					\
	else								\
	    (head).last = (elm)->field.prev;				\
	*(elm)->field.prev = (elm)->field.next;				\
    } while (0)

#define	tailq_foreach(ptr, head, field)				\
    for (__typeof__ ((head).first) ptr = ((head).first);	\
	 ptr;							\
	 ptr = ptr->field.next)

#define tailq_foreach_from(ptr, elm, field)			\
    for (__typeof__ (elm) ptr = (elm);				\
	 ptr;							\
	 ptr = ptr->field.next)

#define	tailq_foreach_reverse(ptr, head, field)			\
    for (__typeof__ ((head).first) ptr = (*(head).last);	\
	 ptr;							\
	 ptr = QE_PREV(ptr, field))

#define	tailq_foreach_reverse_from(ptr, elm, field)		\
    for (__typeof__ (elm) ptr = (elm);				\
	 ptr;							\
	 ptr = QE_PREV(ptr, field))

#define	tailq_foreach_safe(ptr, head, field)				\
    for (__typeof__ ((head).first) ptr = ((head).first),		\
	     fw = ptr->field.next;					\
	 ptr;								\
	 ptr = fw, fw = fw ? fw->field.next : NULL)


#endif // !QUEUE_H_
