/* Copyright 2011, 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <assert.h>
#include <string.h>

#include <minmax.h>
#include <safe-alloc.h>

#include "vec.h"
#include "queue.h"


struct vec_ctx
{
    long owned_vectors;
    // The current number of owned vectors.
};

struct vec
{
    struct vec_ctx *owner;
    // Context owning this vector.

    tailq_head(struct chunk) chunks;
    /* Chunks are chained with a doubly-linked list.  This should evolve to a
     * binary a tree in a near future, improving time complexity of
     * slide_vecp(). */

    int pagesize;
    // Size of a system page. Avoid to call `sysconf (_SC_PAGE_SIZE)' each
    // time.
};

struct chunk
{
    tailq_entry(struct chunk) qe;

    struct vec *owner;
    // Vector owning this chunk.

    int nbpages;
    // Current number of pages in this chunk.

    long off;
    // Vector offset of this chunk. This is the offset as "seen" by the
    // user. So, chunk headers are not taken into account.
};


// Definition of the vecp structure.
/*
 * struct vecp
 * {
 *	struct chunk *chunk;
 *	// This is the current chunk pointed by this vector pointer.
 *
 *	int off;
 *	// Offset inside the chunk. Chunk header is taken into account while
 *	computing this offset.
 * };
 */

enum vec_errno vec_errno;

struct vec_ctx *cur_vec_ctx = NULL;
// The current vector context;


// Return the real length (including header size) in bytes of the given chunk.
static inline long
CHUNK_REAL_LEN (const struct chunk *chunk)
{
    return chunk->nbpages * chunk->owner->pagesize;
}

/* Return the length of this chunk "as seen" by the user. This is the real
 * length without taking into account the chunk header size. */
static inline long
CHUNK_USER_LEN (const struct chunk *chunk)
{
    return CHUNK_REAL_LEN (chunk) - (int)sizeof (struct chunk);
}

// Return the vector offset as seen by the user of the given vector pointer.
static inline long
VECP_AOFF (vecp_t vecp)
{
    return vecp.chunk->off + vecp.off - (int)sizeof (struct chunk);
}


struct vec_ctx *
create_vec_ctx (void)
{
    /* Vector context are currently not implemented but the vector interface
     * forbids us to simply return NULL. */
    struct vec_ctx *vec_ctx;
    if (-1 == ALLOC (vec_ctx)) {
	vec_errno = VEC_ENOMEM;
	return NULL;
    }

    vec_ctx->owned_vectors = 0;

    // The first created vector context is automatically made the current one.
    if (! cur_vec_ctx)
	cur_vec_ctx = vec_ctx;

    return vec_ctx;
}

vec_errno_t
destroy_vec_ctx (vec_ctx_t *vec_ctx)
{
    if (vec_ctx->owned_vectors)
	return VEC_EINVAL;

    FREE (vec_ctx);

    return VEC_SUCCESS;
}


vec_ctx_t *
current_vec_ctx (void)
{
    return cur_vec_ctx;
}

void
vec_ctx_switch (vec_ctx_t *vec_ctx)
{
    cur_vec_ctx = vec_ctx;
}


vec_t *
create_vec (vec_ctx_t *vec_ctx)
{
    // vec_ctx is currently ignored.

    struct vec *vec;
    if (-1 == ALLOC (vec)) {
	vec_errno = VEC_ENOMEM;
	return NULL;
    }

    vec->owner = vec_ctx;
    vec->pagesize = sysconf (_SC_PAGE_SIZE);
    init_tailq_head (vec->chunks);

    // Initialize the vector with one chunk of one page.
    struct chunk *chunk = (struct chunk *) mmap (NULL, (size_t) vec->pagesize,
						 PROT_READ|PROT_WRITE,
						 MAP_ANONYMOUS|MAP_PRIVATE,
						 -1,
						 0);

    if (MAP_FAILED == chunk)
	goto free_vec;

    chunk->owner = vec;
    chunk->nbpages = 1;
    chunk->off = 0;
    tailq_add_tail (vec->chunks, chunk, qe);

    vec_ctx->owned_vectors++;

    return vec;

free_vec:
    free (vec);

    vec_errno = VEC_ENOMEM;
    return NULL;
}

void
destroy_vec (vec_t *vec)
{
    tailq_foreach_safe (chunk, vec->chunks, qe) {
	tailq_remove (vec->chunks, chunk, qe);
	assert (-1 != munmap (chunk, (size_t)CHUNK_REAL_LEN (chunk)));
	// There is no reason for munmap() to fail here.
    }

    vec->owner->owned_vectors--;

    FREE (vec);
}


long
vec_size (const vec_t *_vec)
{
    struct vec *vec = (struct vec *)_vec;
    struct chunk *last_chunk = TAILQ_LAST (vec->chunks);

    return last_chunk->off + CHUNK_USER_LEN (last_chunk);
}

vec_ctx_t *
vec_owner (const vec_t *vec)
{
    return vec->owner;
}


long
grow_vec (vec_t *vec, long size)
{
    if (size < 0) {
	vec_errno = VEC_EINVAL;
	return -1;
    }

    /* The vector is requested to grow of at least *size* bytes. But, if a new
     * chunk has to be created, a little part of it will be consumed by the
     * chunk header. So, to ensure we can offer the requested size, we
     * internally grow of (size + sizeof (struct chunk)) bytes. */
    int nbpages = (size + (long)sizeof (struct chunk)) / vec->pagesize + 1;

    struct chunk *last_chunk = TAILQ_LAST (vec->chunks);
    char *wished_addr = (char *)last_chunk + CHUNK_REAL_LEN (last_chunk);

    struct chunk *chunk =
	(struct chunk *) mmap (wished_addr,
			       (size_t)(nbpages * vec->pagesize),
			       PROT_READ|PROT_WRITE,
			       MAP_ANONYMOUS|MAP_PRIVATE,
			       -1,
			       0);

    if (! chunk) {
	vec_errno = VEC_ENOMEM;
	return -1;
    }

    /* Check whether one succeeds to mmap the created chunk just after the
     * last one. In such case, the newly created chunk is merged into the
     * previous one. */
    if ((char *)chunk == wished_addr) {
	last_chunk->nbpages += nbpages;

	return nbpages * vec->pagesize;
    } else {
	chunk->owner = vec;
	chunk->nbpages = nbpages;
	chunk->off = last_chunk->off + CHUNK_USER_LEN (last_chunk);
	tailq_add_tail (vec->chunks, chunk, qe);

	return CHUNK_USER_LEN (chunk);
    }

    // We must not reach this point.
    /* assert (0); */
}


long
vec_notify_unused (vec_t *vec, long size)
{
    int nbpages = size / vec->pagesize - 1;
    int left_nbpages = nbpages;
    long shrunk_size = 0;

    /* The specification says that a vector cannot be shrunk to the empty
     * vector. We currently naively implement this by not handling the first
     * chunk of the vector in the shrinking process (chunk != TAILQ_FIRST
     * (vec->chunks)).
     * This does not prevent the first chunk to grow in the future if the
     * newly mmapped region can be merged in the first chunk. */
    for (struct chunk *chunk = TAILQ_LAST (vec->chunks),
	     *bw = QE_PREV (chunk, qe);
	 left_nbpages && chunk != TAILQ_FIRST (vec->chunks);
	 chunk = bw, bw = QE_PREV (bw, qe)) {
	int valid_nbpages = MIN (left_nbpages, chunk->nbpages);

	/* If the current chunk must be completely unmapped:
	 * - it must be cleanly unlinked before;
	 * - shrunk_size must be increased without taking into account the
	 * chunk header size. */
	if (valid_nbpages == chunk->nbpages) {
	    tailq_remove (vec->chunks, chunk, qe);
	    shrunk_size += CHUNK_USER_LEN (chunk);
	} else
	    shrunk_size += valid_nbpages * vec->pagesize;

	munmap ((char *)chunk + (chunk->nbpages-valid_nbpages)*vec->pagesize,
		(size_t)(valid_nbpages * vec->pagesize));

	left_nbpages -= valid_nbpages;
    }

    return shrunk_size;
}


vecp_t
vec_begin (const vec_t *vec)
{
    vecp_t vecp = {
	.chunk = TAILQ_FIRST (vec->chunks),
	.off = sizeof (struct chunk)
    };

    return vecp;
}


void
stepf_vecp (vecp_t *vecp)
{
    struct chunk *chunk = vecp->chunk;

    // MAYBE: we could optimize this branchment.
    if (++vecp->off == CHUNK_REAL_LEN (chunk)) {
	vecp->chunk = QE_NEXT (chunk, qe);
	vecp->off = sizeof (struct chunk);
    }
}

void
stepb_vecp (vecp_t *vecp)
{
    struct chunk *chunk = vecp->chunk;

    // MAYBE: we could optimize this branchment.
    if (--vecp->off == -1) {
	vecp->chunk = QE_PREV (chunk, qe);
	vecp->off = CHUNK_REAL_LEN (vecp->chunk) - 1;
    }
}


static void
forward_vecp (vecp_t *vecp, long off)
{
    struct chunk *chunk = vecp->chunk;

    while (VECP_AOFF (*vecp) + off > chunk->off + CHUNK_USER_LEN (chunk))
	chunk = QE_NEXT (chunk, qe);

    // Mutation order is relevant.
    vecp->off =
	VECP_AOFF (*vecp) + off - chunk->off + (int)sizeof (struct chunk);
    vecp->chunk = chunk;
}

static void
backward_vecp (vecp_t *vecp, long off)
{
    struct chunk *chunk = vecp->chunk;

    // FIXME: UNCORRECT. I do not remember why this is not correct...

    while (chunk->off > VECP_AOFF (*vecp) - off)
	chunk = QE_PREV (chunk, qe);

    // Mutation order is relevant.
    vecp->off =
	VECP_AOFF (*vecp) - off - chunk->off + (int)sizeof (struct chunk);
    vecp->chunk = chunk;
}

void
slide_vecp (vecp_t *vecp, long off)
{
    if (off >= 0)
	forward_vecp (vecp, off);
    else
	backward_vecp (vecp, -off);
}

long
vecp_rdist (vecp_t vecp1, vecp_t vecp2)
{
    struct chunk *chunk1 = vecp1.chunk;
    struct chunk *chunk2 = vecp2.chunk;

    return (chunk1->off + vecp1.off) - (chunk2->off + vecp2.off);
}


void
read_vec (long len, char buf[len], vecp_t vecp)
{
    long leftlen = len;

    /* User must ensure it does not request reading to much, else the behavior
     * is undefined. Current behavior is to stop at the end of the vector. */
    for (struct chunk *chunk = vecp.chunk;
	 leftlen && chunk;
	 chunk = QE_NEXT (chunk, qe)) {
	long vlen = MIN (leftlen, CHUNK_REAL_LEN (chunk) - vecp.off);

	memcpy (buf, (char *)chunk + vecp.off, (size_t)vlen);

	leftlen -= vlen;
	buf += vlen;

	vecp = vecp_at_roff (vecp, vlen);
    }
}

void
write_vec (vecp_t vecp, long len, const char buf[len])
{
    long leftlen = len;

    /* User must ensure it does not request writing too much, else the
     * behavior is undefined. Current behavior is to stop at the end of the
     * vector. */
    for (struct chunk *chunk = vecp.chunk;
	 leftlen && chunk;
	 chunk = QE_NEXT (chunk, qe)) {
	long vlen = MIN (leftlen, CHUNK_REAL_LEN (chunk) - vecp.off);

	memcpy ((char *)chunk + vecp.off, buf, (size_t)vlen);

	leftlen -= vlen;
	buf += vlen;

	vecp = vecp_at_roff (vecp, vlen);
    }
}
