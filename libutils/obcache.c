/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

#include <stdlib.h>

#include <safe-alloc.h>

#include "obcache.h"

struct obcache_ctx
{
    long owned_obcaches;
    // The current number of owned object caches.
};

struct obcache
{
    struct obcache_ctx *owner;
    // Context owning this object cache.

    int obsize;
    // The size of an object.

    long numobs;
    // The number of currently managed objects.

    void (*ctor)(void *, int size);
    // Constructor used to construct objects.
};

enum obcache_errno obcache_errno;

struct obcache_ctx *cur_obcache_ctx = NULL;


obcache_ctx_t *
create_obcache_ctx (void)
{
    struct obcache_ctx *obcache_ctx;
    if (-1 == ALLOC (obcache_ctx)) {
	obcache_errno = OBCACHE_ENOMEM;
	return NULL;
    }

    obcache_ctx->owned_obcaches = 0;

    if (! cur_obcache_ctx)
	cur_obcache_ctx = obcache_ctx;

    return obcache_ctx;
}


obcache_errno_t
destroy_obcache_ctx (obcache_ctx_t *obcache_ctx)
{
    if (obcache_ctx->owned_obcaches)
	return OBCACHE_EINVAL;

    FREE (obcache_ctx);

    return OBCACHE_SUCCESS;
}


obcache_ctx_t *
valid_obcache_ctx (void)
{
    return cur_obcache_ctx;
}


void
obcache_ctx_switch (obcache_ctx_t *obcache_ctx)
{
    cur_obcache_ctx = obcache_ctx;
}


obcache_t *
create_obcache (obcache_ctx_t *obcache_ctx,
		int obsize,
		int align, /* Currently unsupported. */
		void (*ctor)(void *, int size))
{
    struct obcache *obcache;
    if (-1 == ALLOC (obcache)) {
	obcache_errno = OBCACHE_ENOMEM;
	return NULL;
    }

    obcache->owner = obcache_ctx;
    obcache->obsize = obsize;
    obcache->ctor = ctor;
    if (obcache_ctx)
	obcache_ctx->owned_obcaches++;

    return obcache;
}


void
destroy_obcache (obcache_t *obcache)
{
    if (obcache->owner)
	obcache->owner->owned_obcaches--;

    /* FIXME: currently, we do not release the acquired slots. As these slots
     * are acquired with a classical malloc(), we do not free the memory
     * allocated for them. */

    FREE (obcache);
}


obcache_ctx_t *
obcache_owner (obcache_t *obcache)
{
    return obcache->owner;
}


static size_t
link_size (obcache_t *obcache)
{
    /* If the object cache is context-bound, objects are linked together with
     * light object handle. Else, we use classical C pointers. */
    return (obcache->owner) ? sizeof (void *) : sizeof (ob_handle_t);
}


void *
obcache_alloc (obcache_t *obcache)
{
    size_t linksize = link_size (obcache);
    void *ob = malloc (((size_t)obcache->obsize + linksize));
    if (! ob) {
	obcache_errno = OBCACHE_ENOMEM;
	return NULL;
    }

    // Take care to skip the link memory place.
    void *user_ob = (char *)ob + linksize;

    if (obcache->ctor)
	obcache->ctor (user_ob, obcache->obsize);

    return (user_ob);
}


void
obcache_free (obcache_t *obcache, void *ob)
{
    size_t linksize = link_size (obcache);
    void *internal_ob = (char *)ob - linksize;
    FREE (internal_ob);
}


void *
next_ob (void *ob)
{
    return *(void **)((char *)ob - sizeof (void *));
}

void
link_ob (void *ob, void *to)
{
    *(void **)((char *)ob - sizeof (void *)) = to;
}


void
handle_link_ob (ob_handle_t ob, ob_handle_t to)
{
    *(ob_handle_t *)ob = to;
}


ob_handle_t
next_ob_handle (ob_handle_t ob)
{
    return *(ob_handle_t *)ob;
}


void *
ob_ptr (ob_handle_t ob)
{
    return (char *)ob + sizeof (ob_handle_t);
}

ob_handle_t
ob_handle (void *ptr)
{
    return (char *)ptr - sizeof (ob_handle_t);
}
