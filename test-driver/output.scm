;; sesh - A text sequence implementation library
;; Copyright (C) 2012 Ludovic Stordeur

(define-module (test-driver output)
  #:export (init-output-system

	    output-pass-test
	    output-skip-test
	    output-fail-test

	    output-unexecuted-test

	    output-begin-testsuite-status
	    output-total-pass-tests
	    output-total-skip-tests
	    output-total-fail-tests
	    output-total-unexecuted-tests

	    output-testsuite-status)

  #:use-module (ice-9 format))

(define reset-seq "[m")
(define green-seq "[32m")
(define yellow-seq "[33m")
(define red-seq "[31m")

(define (activate-smart-output)
  (set! output-pass-test
	(lambda (test-name)
	  (display (string-append green-seq "PASS" reset-seq ": " test-name))
	  (newline)))

  (set! output-skip-test
	(lambda (test-name)
	  (display (string-append yellow-seq "SKIP" reset-seq ": " test-name))
	  (newline)))

  (set! output-fail-test
	(lambda (test-name)
	  (display (string-append red-seq "FAIL" reset-seq ": " test-name))
	  (newline)))


  (set! output-unexecuted-test
	(lambda (test-name)
	  (display (string-append red-seq "The following test was not declared in the testsuite: "
				  test-name reset-seq))
	  (newline)))


  (set! output-begin-testsuite-status
	(lambda (some-failed?)
	  (display (string-append (if some-failed? red-seq green-seq)
				  "*******************" reset-seq))
	  (newline)))

  (set! output-total-pass-tests
	(lambda (count)
	  (format #t "~a~d tests passed~a\n" green-seq count reset-seq)))

  (set! output-total-skip-tests
	(lambda (count)
	  (format #t "~a~d tests skipped~a\n" yellow-seq count reset-seq)))

  (set! output-total-fail-tests
	(lambda (count)
	  (format #t "~a~d tests failed~a\n"
		  (if (> count 0) red-seq green-seq) count reset-seq)))

  (set! output-total-unexecuted-tests
	(lambda (count)
	  (format #t "~a~d unexecuted tests~a\n"
		  (if (> count 0) red-seq green-seq) count reset-seq))))




(define (activate-smart-error-output) #f)


(define (init-output-system)
  "Initialize the output system according to the filetypes connected to stdout
and stderr."

  (let ((stdout-stat (stat (current-output-port)))
	(stderr-stat (stat (current-error-port))))


    (when (eq? (stat:type stdout-stat) 'char-special)
	  (activate-smart-output))

    (when (eq? (stat:type stderr-stat) 'char-special)
	  (activate-smart-error-output))))



(define (output-pass-test test-name)
  (display (string-append "PASS: " test-name))
  (newline))

(define (output-skip-test test-name)
  (display (string-append "SKIP: " test-name))
  (newline))

;; MAYBE: consider outputting this on `current-error-port'.
(define (output-fail-test test-name)
  (display (string-append "FAIL: " test-name))
  (newline))


;; TODO: output this on `current-error-port'.
(define (output-unexecuted-test test-name)
  (display (string-append "The following test was not declared in the testsuite: " test-name))
  (newline))



(define (output-begin-testsuite-status some-failed?)
  (display "*******************")
  (newline))

(define (output-total-pass-tests count)
  (format #t "~d tests passed\n" count))

(define (output-total-skip-tests count)
  (format #t "~d tests skipped\n" count))

;; MAYBE: consider outputting this on `current-error-port'.
(define (output-total-fail-tests count)
  (format #t "~d tests failed\n" count))

;; MAYBE: consider outputting this on `current-error-port'.
(define (output-total-unexecuted-tests count)
  (format #t "~d unexecuted tests\n" count))
