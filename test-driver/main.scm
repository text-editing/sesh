;; sesh - A text sequence implementation library
;; Copyright (C) 2012 Ludovic Stordeur

(define-module (test-driver main)
  #:export (define-testsuite define-test)

  #:use-module (test-driver core))


(define-syntax define-testsuite
  (syntax-rules ()
    ((define-testsuite test ...)
     ((begin-testsuite (program-arguments))
      test ...
      (finalize-testsuite)))))

(define-syntax define-test
  (syntax-rules ()
    ((define-test test-req)
     (if (string? test-req)
	 (do-test test-req)
	 (begin
	   (do-test test-req)
	   (finalize-test))))
    ((define-test test-req test-expand ...)
     (begin
       (do-test test-req)
       (expand-test (lambda () test-expand ...))
       (finalize-test)))))
