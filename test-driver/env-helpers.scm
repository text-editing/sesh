;; sesh - A text sequence implementation library
;; Copyright (C) 2012 Ludovic Stordeur

(define-module (test-driver env-helpers)
  #:export (mktemp))

(define (mktemp)
  (let* ((temp-port (mkstemp! (string-copy "/tmp/mktemp-XXXXXX")))
	 (temp-name (port-filename temp-port)))
    (close-port temp-port)
    temp-name))
