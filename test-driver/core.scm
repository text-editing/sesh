;; sesh - A text sequence implementation library
;; Copyright (C) 2012 Ludovic Stordeur

(define-module (test-driver core)
  #:export (begin-testsuite finalize-testsuite do-test expand-test finalize-test)

  #:use-module (srfi srfi-1)		; fold
  #:use-module (srfi srfi-9)		; define-record-type
  #:use-module (ice-9 getopt-long)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 popen)		; open-pipe*

  #:use-module (test-driver output))


(use-modules (ice-9 pretty-print)) ; for debugging.


;; Type for a testsuite request
(define-record-type ts-req-type
  (make-ts-req test-names dir verbosity do-all-tests?)
  ts-req?
  (test-names ts-req-test-names set-ts-req-test-names!)
  (dir ts-req-dir)
  (verbosity ts-req-verbosity)
  (do-all-tests? ts-req-do-all-tests?))

;; Type for a test request
(define-record-type tr-type
  (make-tr name program args tempfiles do-finalize? result unreq?)
  tr?
  (name tr-name)
  (program tr-program)
  (args tr-args)
  (tempfiles tr-tempfiles)
  (do-finalize? tr-do-finalize?)
  (result tr-result set-tr-result!)
  (unreq? tr-unreq? set-tr-unreq?!))


;; Global state variables.
(define old-cwd #f)
(define ts-req #f)
(define cur-tr #f)
(define current-test-env #f)
(define exec-tests '())


;;; User requests processing.

(define (process-ts-ureq ts-ureq)
  "Check the given TS-UREQ and build the corresponding low-level testsuite
request."

  (let* ((dir (assq-ref ts-ureq 'directory))
	 (verbosity (assq-ref ts-ureq 'verbosity))
	 (parsed-verbosity (string->number verbosity)))

    ;; Check verbosity value.
    (when (eq? parsed-verbosity #f)
	  (throw 'bad-verbosity-type verbosity))

    ;; Check directory target existence if explicitly given.
    (case dir
      ((#f #t) '())
      (else (when (or (not (file-exists? dir))
		      (not (eq? 'directory (stat:type (stat dir)))))
		  (throw 'target-dir-inexistence dir))))

    ;; Note: verbosity levels [3 4] are currently not handled.
    (when (or (< parsed-verbosity 0) (> parsed-verbosity 4))
	  (throw 'bad-verbosity-range parsed-verbosity))

    (make-ts-req (assq-ref ts-ureq 'test-names)		; test-names
		 dir					; dir
		 parsed-verbosity			; verbosity
		 (null? (assq-ref ts-ureq 'test-names))	; do-all-tests?
		 )))


(define (process-test-ureq test-ureq)
  "Check the given TEST-UREQ and build the corresponding low-level test
request."

  (if (string? test-ureq)
      ;; Simple user test request. Recurse while providing a more regular test
      ;; user request.
      (process-test-ureq `((name . ,test-ureq) (do-finalize? . #t)))

      (begin
	;; Give optional entries a default value.
	(unless (assq 'program test-ureq)
		(set! test-ureq (acons 'program (assq-ref test-ureq 'name) test-ureq)))
	(unless (assq 'args test-ureq)
		(set! test-ureq (acons 'args '() test-ureq)))
	(unless (assq 'tempfiles test-ureq)
		(set! test-ureq (acons 'tempfiles '() test-ureq)))
	(unless (assq 'do-finalize? test-ureq)
		(set! test-ureq (acons 'do-finalize? #f test-ureq)))

	(let ((test-name (assq-ref test-ureq 'name))
	      (program (assq-ref test-ureq 'program))
	      (args (assq-ref test-ureq 'args))
	      (tempfiles (assq-ref test-ureq 'tempfiles)))

	  ;; Check types
	  (make-tr (cond ((string? test-name) test-name)
			 (else throw 'bad-tr-name-type test-name)) ; name
		   (cond ((string? program) program)
			 (else throw 'bad-program-type program)) ; program
		   (cond ((string? args) (list args))
			 ((list? args) args)
			 (else throw 'bad-tr-args-type args)) ; args
		   (cond ((string? tempfiles) (list tempfiles))
			 ((list? tempfiles) tempfiles)
			 (else throw 'bad-tr-tempfiles-type tempfiles)) ; tempfiles

		   ;; The following field are created internally so their type
		   ;; are correct.
		   (assq-ref test-ureq 'do-finalize?)	; do-finalize?
		   -1					; result
		   #f					; unreq?
		   )))))



(define (test-requested? tr)
  "Return #t if the given TR has to be executed and #f if it must be silently
skipped."

  (if (ts-req-do-all-tests? ts-req)
      #t
      (case (member (tr-name tr) (ts-req-test-names ts-req))
	((#f) #f)
	(else #t))))



;;; Test execution

;; TODO: to implement
(define (setup-test-env tr) '())
(define (clean-test-env test-env) '())

;; TODO: fork/exec to dup stdin/stdout/stderr
(define (run-payload tr test-env)
  ;; Note: do not know how to catch system* errors (unfoundable program)
  ;; Note: perhars should I wrap the return status with `status:exit-val'.
  (system (string-join (cons (string-append "./" (tr-program tr)) (tr-args tr)))))



;;; Test suite status

(define (enumerate-failed-tests)
  (fold (lambda (tr sum)
	  (case (tr-result tr)
	    ((0 77) sum)
	    (else (+ sum 1))))
	0
	exec-tests))

(define (enumerate-skipped-tests)
  (fold (lambda (tr sum)
	  (case (tr-result tr)
	    ((77) (+ sum 1))
	    (else sum)))
	0
	exec-tests))

(define (enumerate-passed-tests)
  (fold (lambda (tr sum)
	  (case (tr-result tr)
	    ((0) (+ sum 1))
	    (else sum)))
	0
	exec-tests))



;;; Public interface

;; TODO: print usage if -h given.
(define (begin-testsuite args)
  "Initialize the test driver and configure it with arguments ARGS. This must
be the first function to be called before any other one."

  (init-output-system)

  (let* ((option-spec '((directory (single-char #\C) (value optional))
			(verbose (single-char #\V) (value #t))))
	 (cl-req (getopt-long args option-spec))

	 ;; Build a testsuite user request from the command line request. Give
	 ;; options their default value.
	 (ts-ureq (list `(directory	. ,(option-ref cl-req 'directory #f))
			`(test-names	. ,(option-ref cl-req '() '()))
			`(verbosity	. ,(option-ref cl-req 'verbose "2")))))

    ;; Create the final test request from the user test request.
    ;; Perform option parsing, semantic checking, ...
    ;; TODO: catch bad-verbosity-req
    (set! ts-req (process-ts-ureq ts-ureq)))

  ;; Switch to the specified target directory if requested
  (case (ts-req-dir ts-req)
    ((#f) '())
    ((#t) (let* ((p (open-pipe* OPEN_READ "readlink" "-f" (car args)))
		 (mypath (read-line p))
		 (mydir (dirname mypath)))
	    (close-pipe p)
	    (set! old-cwd (getcwd))
	    (chdir mydir)))
    (else (set! old-cwd (getcwd))
	  (chdir (ts-req-dir ts-req)))))


(define (finalize-testsuite)
  "Print a status on the test suite execution then exit."

  (let ((unexecuted-tests
	 (fold (lambda (test-name sum)
		 (output-unexecuted-test test-name)
		 (+ sum 1))
	       0
	       (ts-req-test-names ts-req)))
	(failed-tests (enumerate-failed-tests))
	(skipped-tests (enumerate-skipped-tests))
	(passed-tests (enumerate-passed-tests)))

    ;; Print the testsuite status according to the verbosity level.
    (if (> (ts-req-verbosity ts-req) 0)
	(begin
	  (output-begin-testsuite-status (or (> failed-tests 0) (> unexecuted-tests 0)))
	  (output-total-unexecuted-tests unexecuted-tests)
	  (output-total-fail-tests failed-tests)
	  (output-total-skip-tests skipped-tests)
	  (output-total-pass-tests passed-tests))
	(begin
	  (when (or (> failed-tests 0) (> unexecuted-tests 0))
		(output-begin-testsuite-status #t))
	  (when (> failed-tests 0)
		(output-total-fail-tests failed-tests))
	  (when (> unexecuted-tests 0)
		(output-total-unexecuted-tests unexecuted-tests))))

    ;; Restore the old working directory if necessary
    (case old-cwd
      ((#f) '())
      (else (chdir old-cwd)))

    ;; Return status of the calling script.
    (exit (and (= 0 unexecuted-tests) (= 0 failed-tests)) 0 1)))



(define (do-test test-ureq)
  "Perform one test described by TEST-REQ by executing the associated payload."

  (let ((tr (process-test-ureq test-ureq)))
    (if (test-requested? tr)
	(begin
	  (set! current-test-env (setup-test-env tr))
	  (set-tr-result! tr (run-payload tr current-test-env))

	  (set-ts-req-test-names! ts-req
				  (delete (tr-name tr) (ts-req-test-names ts-req)))
	  (set! exec-tests (cons tr exec-tests)))

	(set-tr-unreq?! tr #t))

    (set! cur-tr tr)

    ;; Finalize phase must be performed even when the test has not been executed.
    (when (tr-do-finalize? tr)
	  (finalize-test))))




(define (expand-test test-fun)
  "Expand a test started with `do-test' by executing the function
TEST-FUN. This expansion is performed only when the payload has successfully
returned."

  (unless (procedure? test-fun)
	  (throw 'bad-expand-test-type test-fun))

  (when (and (not (tr-unreq? cur-tr))
	     (= 0 (tr-result cur-tr)))
	(set-tr-result! cur-tr (test-fun))))


(define (finalize-test)
  "Print the test status (according to the verbosity level and the returned
code) and perform some cleanups necessary to start a new test."

  (if (not (tr-unreq? cur-tr))
      (begin
	(clean-test-env current-test-env)

	;; Tempfiles are deleted only if the test did not fail. Else, the user
	;; may wish to inspect these files.
	(case (tr-result cur-tr)
	  ((0 77) (for-each delete-file (tr-tempfiles cur-tr))))

	;; Print test status if the verbosity level grants it.
	(let ((test-name (tr-name cur-tr))
	      (verbosity (ts-req-verbosity ts-req)))
	  (case (tr-result cur-tr)
	    ((0) (when (> verbosity 1)
		       (output-pass-test test-name)))
	    ((77) (when (> verbosity 1)
			(output-skip-test test-name)))
	    (else (output-fail-test test-name))))

	;; Reset necessary state variables.
	(set! current-test-env #f))

      (begin
      ;; Tempfiles must be deleted when the test has not been requested for
      ;; execution because they have yet been created.
	(for-each delete-file (tr-tempfiles cur-tr))

	;; `cur-tr' must always be resetted.
	(set! cur-tr #f))))
