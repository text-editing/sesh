;; sesh - A text sequence implementation library
;; Copyright (C) 2012 Ludovic Stordeur

(define-module (test-driver test-helpers)
  #:export (files=?))

(define (files=? file1 file2)
  (system* "diff" "-q" file1 file2))
