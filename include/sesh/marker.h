/* Copyright 2011, 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SESH_MARKER_H_
#define SESH_MARKER_H_

sesh_errno_t sesh_destroy_marker (sesh_marker_handle_t m);

sesh_marker_handle_t sesh_clone_marker (const sesh_marker_handle_t m);
sesh_marker_handle_t sesh_mark_roff (const sesh_marker_handle_t m,
				     sesh_off_t roff);

sesh_off_t	sesh_marker_off (const sesh_marker_handle_t m);
sesh_text_t *	sesh_marker_owner (const sesh_marker_handle_t m);

sesh_errno_t sesh_fwd_marker (sesh_marker_handle_t m);
sesh_errno_t sesh_bwd_marker (sesh_marker_handle_t m);
sesh_off_t   sesh_slide_marker (sesh_marker_handle_t m, sesh_off_t off);
sesh_errno_t sesh_marker_goto_off (sesh_marker_handle_t m, sesh_off_t off);
sesh_errno_t sesh_marker_goto_marker (sesh_marker_handle_t m,
				      const sesh_marker_handle_t m2);

/* `iom' suffix stands for `it_of_marker'. I use this short form because
 * `sesh_iom' may be frequently called. */
sesh_iterator_t sesh_iom (const sesh_marker_handle_t m);

#define sesh_mv_marker(m, varptr, stmt)				\
    do {							\
	sesh_off_t __sesh_len;					\
	{							\
	    sesh_iterator_t __sesh_start = sesh_iom (m);	\
	    sesh_iterator_t m = __sesh_start;			\
								\
	    stmt;						\
								\
	    __sesh_len = sesh_itor_rdist (m, __sesh_start);	\
	}							\
								\
	sesh_slide_marker (m, __sesh_len);			\
								\
	if (varptr)						\
	    *(sesh_off_t *)varptr = __sesh_len;			\
    } while (0)

#endif // !SESH_MARKER_H_
