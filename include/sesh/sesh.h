/* Copyright 2011, 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SESH_H_
#define SESH_H_

#include <stdint.h>	// [u]int*_t type family.
#include <stdbool.h>	// bool type.
#include <stddef.h>	// wchar_t type.

#include "sesh-impl-details.h"

typedef enum sesh_errno sesh_errno_t;
// Type for an error code.

extern sesh_errno_t sesh_errno;
// Global variable storing the code of last occured error.

bool sesh_check_enomem (void);


typedef long sesh_off_t;
/* Type suitable to represent an offset or a (relative) distance inside a text
 * sequence. */

typedef long sesh_len_t;
// Type suitable to represent a length inside a text sequence.


typedef struct text sesh_text_t;
// Type for a text sequence.

typedef struct marker * sesh_marker_handle_t;
// Type for a marker descriptor.

#define SESH_NULL_MARKER NULL
// Value for an invalid marker descriptor.

typedef struct sesh_iterator sesh_iterator_t;
// Type for an iterator.


typedef uint32_t sesh_set_t;
// Generic type for a set of elements.

typedef uint32_t sesh_attr_t;
// Type for a cell attribute.

typedef enum sesh_tag sesh_tag_t;
// Type for a cell tag.

#define SESH_MAXPROPS 8
// Maximum number of supported properties.

typedef enum sesh_prop sesh_prop_t;
// Type for a cell property.

struct sesh_cellattrs
{
    sesh_set_t tags;
    int8_t props[SESH_MAXPROPS];
};

int sesh_prop_idx (sesh_prop_t p);

sesh_attr_t	sesh_attr_of_tag (sesh_tag_t tag);
sesh_attr_t	sesh_attr_of_prop (sesh_prop_t p);
sesh_set_t	sesh_attrset (sesh_set_t tag, sesh_set_t p);


int8_t sesh_max_cell_size (const char *enc);
int8_t sesh_min_cell_size (const char *enc);

#endif // !SESH_H_
