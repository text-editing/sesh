/* Copyright 2011, 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SESH_TEXT_H_
#define SESH_TEXT_H_

sesh_text_t *	sesh_make_text (const char *enc, const char *filename);
void		sesh_destroy_text (sesh_text_t *text);

const char *	 sesh_text_enc (const sesh_text_t *text);
sesh_len_t	 sesh_text_size (const sesh_text_t *text);
sesh_len_t	 sesh_text_len (const sesh_text_t *text);

sesh_marker_handle_t	sesh_point (const sesh_text_t *text);
sesh_marker_handle_t	sesh_mark_off (sesh_text_t *text, sesh_off_t off);

sesh_len_t sesh_insert (sesh_text_t *text,
			sesh_len_t len, const char buf[len]);
sesh_len_t sesh_erase (sesh_text_t *text, sesh_len_t len);

sesh_len_t sesh_tag (sesh_text_t *text, sesh_set_t tags, sesh_len_t len);
sesh_len_t sesh_untag (sesh_text_t *text, sesh_set_t tags, sesh_len_t len);
sesh_len_t sesh_set_prop (sesh_text_t *text,
			  sesh_prop_t p, int8_t v, sesh_len_t len);

sesh_iterator_t sesh_text_begin (const sesh_text_t *text);
sesh_iterator_t sesh_text_end (const sesh_text_t *text);
sesh_iterator_t sesh_text_rend (const sesh_text_t *text);
sesh_iterator_t sesh_text_rbegin (const sesh_text_t *text);

sesh_off_t sesh_itor_off (const sesh_text_t *text, sesh_iterator_t it);
bool sesh_itor_rend_p (const sesh_text_t *text, sesh_iterator_t it);
bool sesh_itor_end_p (const sesh_text_t *text, sesh_iterator_t it);


// NOT YET IMPLEMENTED.

sesh_set_t sesh_set_barriers (sesh_text_t *text, sesh_set_t attrs);
sesh_set_t sesh_unset_barriers (sesh_text_t *text, sesh_set_t attrs);

#endif // !SESH_TEXT_H_
