/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SESH_LINE_H_
#define SESH_LINE_H_

bool sesh_nl_p (sesh_iterator_t it);

void sesh_matchf_itor2nl (sesh_iterator_t *it, sesh_iterator_t end);
void sesh_matchb_itor2nl (sesh_iterator_t *it, sesh_iterator_t end);
long sesh_slide_itor2nl (sesh_iterator_t *it, sesh_iterator_t end, long len);

sesh_errno_t	sesh_matchf_marker2nl (sesh_marker_handle_t m);
sesh_errno_t	sesh_matchb_marker2nl (sesh_marker_handle_t m);
long		sesh_slide_marker2nl (sesh_marker_handle_t m, long len);

#endif //!SESH_LINE_H_
