/* Copyright 2011, 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SESH_DIFF_H_
#define SESH_DIFF_H_

sesh_hunks_iterator_t	sesh_hunks_itor_next (sesh_hunks_iterator_t);

struct sesh_hunk_hdr	sesh_hunk_hdr (sesh_hunks_iterator_t);
sesh_iterator_t		sesh_hunk_itor (sesh_hunks_iterator_t);


#endif //!SESH_DIFF_H_
