/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SESH_IMPL_DETAILS_H_
#define SESH_IMPL_DETAILS_H_

struct text;
struct marker;
typedef int sesh_ioff_t;
struct phandle;
struct sesh_iterator
{
    struct phandle *phandle;
    sesh_ioff_t off;
};

enum sesh_errno
{
    SESH_SUCCESS = 0,
    SESH_ENOMEM = 1,
    SESH_ENOENT = 2,
    SESH_ENOENC = 3,
    SESH_EINVAL = 4,
    SESH_EINPOS = 5,
    SESH_EINLEN = 6,
    SESH_EPERM = 7,
    SESH_EILSEQ = 8,
    SESH_ESIBLING = 9,
    SESH_EINSTATE = 10
};

enum sesh_tag
{
    SESH_TAG_NONE = 0,
    SESH_TAG_BOLD = (1<<0),
    SESH_TAG_ITALIC = (1<<1),
    SESH_TAG_GHOST = (1<<2),
    SESH_TAG_FGCOLOR = (1<<3),
    SESH_TAG_BGCOLOR = (1<<4),
    SESH_TAG_UCOLOR = (1<<5),
    SESH_TAG_USER1 = (1<<14),
    SESH_TAG_USER2 = (1<<15),
    SESH_TAG_ALL = 65535
};

enum sesh_prop
{
    SESH_PROP_NONE = 0,
    SESH_PROP_FGCOLOR = (1<<0),
    SESH_PROP_BGCOLOR = (1<<1),
    SESH_PROP_UCOLOR = (1<<2),
    SESH_PROP_USER1 = (1<<3),
    SESH_PROP_USER2 = (1<<4)
};

#endif //!SESH_IMPL_DETAILS_H_
