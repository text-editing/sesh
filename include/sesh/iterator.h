/* Copyright 2011, 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SESH_ITERATOR_H_
#define SESH_ITERATOR_H_

int8_t	sesh_cell (sesh_iterator_t it, wchar_t *w);
char	sesh_bytecell (sesh_iterator_t it);

sesh_set_t		sesh_cell_tags (sesh_iterator_t it);
int8_t			sesh_cell_prop (sesh_iterator_t it, sesh_prop_t p);
struct sesh_cellattrs	sesh_cell_attrs (sesh_iterator_t it);
sesh_set_t		sesh_cell_barriers (sesh_iterator_t it);

int		sesh_cmp_itor (sesh_iterator_t it1, sesh_iterator_t it2);
sesh_off_t	sesh_matchf_itor2itor (sesh_iterator_t *it1,
				       sesh_iterator_t it2);
sesh_off_t	sesh_matchb_itor2itor (sesh_iterator_t *it1,
				       sesh_iterator_t it2);

void sesh_fwd_itor (sesh_iterator_t *it);
void sesh_bwd_itor (sesh_iterator_t *it);
void sesh_slide_itor (sesh_iterator_t *it, sesh_off_t len);

sesh_off_t sesh_itor_rdist (sesh_iterator_t it1, sesh_iterator_t it2);

static inline bool
sesh_itor_eq_p (sesh_iterator_t it1, sesh_iterator_t it2)
{
    return (it1.phandle == it2.phandle &&
	    it1.off == it2.off);
}

#define sesh_itor_excursion(it, varptr, stmt)			\
    do {							\
	sesh_off_t __sesh_len;					\
	{							\
	    sesh_iterator_t __sesh_start = sesh_iom (m);	\
	    sesh_iterator_t it = __sesh_start;			\
								\
	    stmt;						\
								\
	    __sesh_len = sesh_itor_rdist (it, __sesh_start);	\
	}							\
								\
	if (varptr)						\
	    *(sesh_off_t *)varptr = __sesh_len;			\
    } while (0)


// TODO: move to <sesh/stream.h>
sesh_len_t sesh_read_s (sesh_len_t buflen, char buf[buflen],
			sesh_iterator_t it, sesh_len_t len);
sesh_len_t sesh_read (char buf[], sesh_iterator_t it, sesh_len_t len);

#endif // !SESH_ITERATOR_H_
