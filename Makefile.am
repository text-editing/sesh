# Copyright 2011, 2012 Ludovic Stordeur
#
# This file is part of Sesh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

ACLOCAL_AMFLAGS = -I m4

SUBDIRS = gnulib . tests

EXTRA_DIST = m4/gnulib-cache.m4
CLEANFILES = *.gcda *.gcno

AM_CPPFLAGS = -I$(top_srcdir)/gnulib -I$(top_srcdir)/include

AM_CFLAGS  += -pedantic
# `-pedantic' cannot be applied to the whole project. As an example, tests
# rely on the Check framework which use in its headers some non ISO C
# compliant features. So, we only enforce strict ISO compliance for the core
# libraries.

noinst_LIBRARIES =	\
	src/libsesh.a

src_libsesh_a_SOURCES =		\
	libutils/vec.c		\
	libutils/obcache.c	\
	src/buf.c		\
	src/enc.c		\
	src/ptable.c		\
	src/misc.c		\
	src/iterator.c		\
	src/text.c		\
	src/marker.c		\
	src/line.c

# noinst_PROGRAMS = examples/pager
# examples_pager_SOURCES = examples/pager.c examples/std-cmds.c
# examples_pager_LDADD = lib/libsesh.a gnulib/libgnu.a -lncurses

# Generate A HTML report of the code coverage data.
if COVERAGE
if HAVE_LCOV
coverage-report:
	$(AM_V_GEN)$(LCOV_PROG_PATH) -c -d $(builddir) -o coverage.info
	$(AM_V_GEN)$(GENHTML_PROG_PATH) coverage.info -o coverage-html
endif
endif
