/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include "test-text.h"

START_TEST (forward_marker_over_nl_error)
{
    /* This test forwards a marker for a newline until the end of the sequence
     * while there is no more newline to match. */

    sesh_marker_handle_t point = sesh_point (text);
    sesh_marker_goto_off (point, sesh_text_len (text));

    fail_unless (SESH_EINPOS == sesh_matchf_marker2nl (point));
}
END_TEST

START_TEST (backward_marker_over_nl_error)
{
    /* This test forwards a marker for a newline until the rend of the
     * sequence while there is no more newline to match. */

    sesh_marker_handle_t point = sesh_point (text);
    sesh_marker_goto_off (point, -1);

    fail_unless (SESH_EINPOS == sesh_matchb_marker2nl (point));
}
END_TEST

START_TEST (slide_marker_over_nl_error)
{
    /* This test slides a marker over newlines with a given step value inside
     * a region which does not contain as many as newlines. */

    sesh_marker_handle_t point = sesh_point (text);

    sesh_marker_goto_off (point, sesh_text_len (text) - 1);
    fail_unless (1 == sesh_slide_marker2nl (point, 2));
    fail_unless (sesh_itor_end_p (text, sesh_iom (point)));

    sesh_marker_goto_off (point, 46); // Offset of the first newline.
    fail_unless (-1 == sesh_slide_marker2nl (point, -2));
    fail_unless (sesh_itor_rend_p (text, sesh_iom (point)));
}
END_TEST

void
marker_nl_matching_errors_tcase (TCase *tc, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tc, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tc, setup_gpl3_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tc, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tc, setup_gpl3_utf8_text, teardown_text);
    }

    tcase_add_test (tc, forward_marker_over_nl_error);
    tcase_add_test (tc, backward_marker_over_nl_error);
    tcase_add_test (tc, slide_marker_over_nl_error);
}
