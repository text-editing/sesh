/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

// Test case for object cache related features.

#include <check.h>

#include "test-obcache.h"

START_TEST (ctx_free_obcache_ctx_accessor)
{
    // Test object cache context accessor for a context free object cache.
    fail_unless (NULL == obcache_owner (obcache));
}
END_TEST

TCase *
ctx_free_obcache_tcase (void)
{
    static TCase *tcase = NULL;

    if (tcase)
	return tcase;

    tcase = tcase_create ("ctx-free-obcache");
    tcase_add_checked_fixture (tcase,
			       setup_ctx_free_obcache, teardown_obcache);

    tcase_add_test (tcase, ctx_free_obcache_ctx_accessor);

    return tcase;
}



START_TEST (ctx_bound_obcache_ctx_accessor)
{
    // Test object cache context accessor for a context-bound object cache.
    fail_unless (obcache_ctx == obcache_owner (obcache));
}
END_TEST

START_TEST (not_empty_obcache_ctx_undestroyable)
{
    // A non-empty object cache context is undestroyable.
    fail_unless (OBCACHE_EINVAL == destroy_obcache_ctx (obcache_ctx));
}
END_TEST

TCase *
ctx_bound_obcache_tcase (void)
{
    static TCase *tcase = NULL;

    if (tcase)
	return tcase;

    tcase = tcase_create ("ctx-bound-obcache");
    tcase_add_checked_fixture (tcase,
			       setup_obcache_ctx, teardown_obcache_ctx);
    tcase_add_checked_fixture (tcase,
			       setup_ctx_bound_obcache, teardown_obcache);

    tcase_add_test (tcase, ctx_bound_obcache_ctx_accessor);
    tcase_add_test (tcase, not_empty_obcache_ctx_undestroyable);

    return tcase;
}
