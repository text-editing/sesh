/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

#include <check.h>

#include "test-text.h"


START_TEST (end_is_markable)
{
    // End is markable.

    sesh_iterator_t rend_it = checked_text_rend (text);
    sesh_marker_handle_t end = checked_mark_off (text, 0);

    fail_unless (1 == sesh_itor_rdist (sesh_iom (end), rend_it));

    fail_unless (SESH_SUCCESS == sesh_destroy_marker (end));
}
END_TEST

START_TEST (rend_is_markable)
{
    // Rend is markable.

    sesh_iterator_t end_it = checked_text_end (text);
    sesh_marker_handle_t rend = checked_mark_off (text, -1);

    fail_unless (-1 == sesh_itor_rdist (sesh_iom (rend), end_it));

    fail_unless (SESH_SUCCESS == sesh_destroy_marker (rend));

}
END_TEST

void
mark_off_tcase (TCase *tcase, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tcase, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_empty_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tcase, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_empty_utf8_text, teardown_text);

	break;
    }

    tcase_add_test (tcase, end_is_markable);
    tcase_add_test (tcase, rend_is_markable);
}
