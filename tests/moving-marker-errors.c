/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

#include <check.h>

#include "test-text.h"

START_TEST (goto_after_end)
{
    // We cannot move after the end.

    fail_unless (SESH_EINPOS == sesh_marker_goto_off (sesh_point (text), 1));
}
END_TEST

START_TEST (goto_before_rend)
{
    // We cannot move before the rend.

    fail_unless (SESH_EINVAL == sesh_marker_goto_off (sesh_point (text), -2));
}
END_TEST

START_TEST (goto_not_sibling_marker)
{
    // We cannot move to a marker which is not a sibling of us.

    sesh_text_t *text2 = sesh_make_text (enc, NULL);
    fail_if (NULL == text2);

    fail_unless
	(SESH_ESIBLING == sesh_marker_goto_marker (sesh_point (text),
						   sesh_point (text2)));

    sesh_destroy_text (text2);
}
END_TEST

void
moving_marker_errors_tcase (TCase *tcase, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tcase, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_empty_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tcase, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_empty_utf8_text, teardown_text);
	break;
    }

    tcase_add_test (tcase, goto_after_end);
    tcase_add_test (tcase, goto_before_rend);
    tcase_add_test (tcase, goto_not_sibling_marker);
}
