/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

#include <check.h>

#include "test-text.h"

START_TEST (mark_after_end)
{
    // We cannot mark after the end.

    fail_unless (NULL == sesh_mark_off (text, 1));
    fail_unless (sesh_errno == SESH_EINPOS);
}
END_TEST

START_TEST (mark_before_rend)
{
    // We cannot mark before the rend.

    fail_unless (NULL == sesh_mark_off (text, -2));
    fail_unless (sesh_errno == SESH_EINVAL);
}
END_TEST

void
mark_off_errors_tcase (TCase *tcase, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tcase, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_empty_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tcase, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_empty_utf8_text, teardown_text);

	break;
    }

    tcase_add_test (tcase, mark_after_end);
    tcase_add_test (tcase, mark_before_rend);
}
