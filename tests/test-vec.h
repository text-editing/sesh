/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef TEST_VEC_H_
#define TEST_VEC_H_

// Common stuff used for vector interface testing.

#include <check.h>

#include <libutils/vec.h>

#include "test-common.h"

/* void	read_vec_in_one_shot (vecp_t vecp, int fd, long size);
 * void	read_vec_char_per_char (vecp_t vecp, int fd, long size); */

/*
 * - read_vec_in_one_shot (vecp, fd, size)
 * Read all the vector slice [vecp, vecp+size[ in one call to read_vec() and
 * write this slice in the file pointed by [fd].
 *
 * - read_vec_char_per_char (vecp, fd, size)
 * Perform the same than `read_vec_in_one_shot' but do it by calling [size]
 * times read_vec(), each call reading one cell of the vector.
 */

// Fixture global states.
extern struct vec_ctx *vec_ctx;
extern struct vec *vec;

static inline void
setup_vec_ctx (void)
{
    vec_ctx = create_vec_ctx ();
    fail_if (NULL == vec_ctx);
}

static inline void
teardown_vec_ctx (void)
{
    fail_unless (VEC_SUCCESS == destroy_vec_ctx (vec_ctx));
}

static inline void
setup_vec (void)
{
    vec = create_vec (vec_ctx);
    fail_if (NULL == vec);
}

static inline void
teardown_vec (void)
{
    destroy_vec (vec);
}

#endif //!TEST_VEC_H_
