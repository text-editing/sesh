/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

// This test program performs some basic tests on the vector interface.

#include <check.h>
#include <unistd.h>

#include "test-vec.h"

START_TEST (vec_ctx_accessor)
{
    // Check the vector context accessor.
    fail_unless (vec_ctx == vec_owner (vec));
}
END_TEST

START_TEST (vec_initial_size)
{
    fail_unless (0 < vec_size (vec));
}
END_TEST

START_TEST (not_empty_vec_ctx_undestroyable)
{
    // Let's try to destroy the vector context. It should not be possible.
    fail_unless (VEC_EINVAL == destroy_vec_ctx (vec_ctx));
}
END_TEST

START_TEST (grow_shrink_vec)
{
    int pagesize = sysconf (_SC_PAGE_SIZE);

    /* Request the vector to grow of at least one page and check how much the
     * vector has grown. */
    fail_unless (pagesize <= grow_vec (vec, pagesize));

    /* Notify the vector that the last page is unused and can be freed and
     * check how much the vector has shrunk. */
    fail_unless (pagesize >= vec_notify_unused (vec, pagesize));

    /* Now, we notify the vector that more than its size is unused. We
     * will check after that the vector still has a strictly positive size and
     * that we can always get a vector pointer on the beginning of the
     * vector. */
    fail_unless
	(vec_size (vec) > vec_notify_unused (vec, vec_size (vec) + 1));
    fail_unless (0 < vec_size (vec));
    vec_begin (vec);
}
END_TEST


TCase *
vec_basic_tcase (void)
{
    static TCase *tcase = NULL;

    if (tcase)
	return tcase;

    tcase = tcase_create ("vec-basic");
    tcase_add_checked_fixture (tcase, setup_vec_ctx, teardown_vec_ctx);
    tcase_add_checked_fixture (tcase, setup_vec, teardown_vec);

    tcase_add_test (tcase, vec_ctx_accessor);
    tcase_add_test (tcase, vec_initial_size);
    tcase_add_test (tcase, not_empty_vec_ctx_undestroyable);
    tcase_add_test (tcase, grow_shrink_vec);
    return tcase;
}
