/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include "test-text.h"

START_TEST (iterate_over_nl)
{
    /* This test iterates over newline in the text, using
     * `sesh_stepf_itor2nl()' and compares results with the offset table
     * `gpl3_nl_offsets'. */

    sesh_iterator_t it = sesh_text_begin (text);
    sesh_iterator_t end = sesh_text_end (text);
    long i = 0;
    long nlcount = sizeof (gpl3_nl_offsets) / sizeof (sesh_off_t);

    sesh_matchf_itor2nl (&it, end);
    for (; ! sesh_itor_eq_p (it, end); sesh_matchf_itor2nl (&it, end)) {
	fail_unless (sesh_nl_p (it));

	sesh_off_t off = sesh_itor_off (text, it);
	fail_unless (off == gpl3_nl_offsets[i]);
	i++;
	if (nlcount == i)
	    break;

	sesh_fwd_itor (&it);
    }

    fail_unless (nlcount == i);
}
END_TEST

START_TEST (backward_iterate_over_nl)
{
    /* This test backward iterates over newline in the text, using
     * `sesh_stepb_itor2nl()' and compares results with the offset table
     * `gpl3_nl_offsets'. */

    sesh_iterator_t it = sesh_text_rbegin (text);
    sesh_iterator_t rend = sesh_text_rend (text);
    long nlcount = sizeof (gpl3_nl_offsets) / sizeof (sesh_off_t);
    long i = nlcount - 1;

    sesh_matchb_itor2nl (&it, rend);
    for (; ! sesh_itor_eq_p (it, rend); sesh_matchb_itor2nl (&it, rend)) {
	fail_unless (sesh_nl_p (it));

	sesh_off_t off = sesh_itor_off (text, it);
	fail_unless (off == gpl3_nl_offsets[i]);
	i--;
	if (-1 == i)
	    break;

	sesh_bwd_itor (&it);
    }

    fail_unless (-1 == i);
}
END_TEST

START_TEST (slide_over_nl)
{
    /* This test iterates twice over newline, the first time in the forward
     * direction, the second time in the backward direction. For each
     * iteration, it slides over newlines using `sesh_slide_itor2nl()' using a
     * step of 2. */

    sesh_iterator_t it = sesh_text_begin (text);
    sesh_iterator_t rend = sesh_text_rend (text);
    sesh_iterator_t end = sesh_text_end (text);
    long i = 1;
    long slide_step = 2;
    long nlcount = sizeof (gpl3_nl_offsets) / sizeof (sesh_off_t);

    fail_unless (slide_step == sesh_slide_itor2nl (&it, end, slide_step));
    for (;
	 ! sesh_itor_eq_p (it, end);
	 fail_unless (
	     slide_step == sesh_slide_itor2nl (&it, end, slide_step))) {
	fail_unless (sesh_nl_p (it));

	sesh_off_t off = sesh_itor_off (text, it);
	fail_unless (off == gpl3_nl_offsets[i]);
	i += slide_step;
	if (i >= nlcount)
	    break;

	sesh_fwd_itor (&it);
    }

    i -= slide_step;
    for (;
	 ! sesh_itor_eq_p (it, rend);
	 fail_unless (
	     -slide_step == sesh_slide_itor2nl (&it, rend, -slide_step))) {
	fail_unless (sesh_nl_p (it));

	sesh_off_t off = sesh_itor_off (text, it);
	fail_unless (off == gpl3_nl_offsets[i]);
	i -= slide_step;
	if (i < 0)
	    break;

	sesh_bwd_itor (&it);
    }
}
END_TEST

void
iterator_nl_matching_tcase (TCase *tc, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tc, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tc, setup_gpl3_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tc, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tc, setup_gpl3_utf8_text, teardown_text);
    }

    tcase_add_test (tc, iterate_over_nl);
    tcase_add_test (tc, backward_iterate_over_nl);
    tcase_add_test (tc, slide_over_nl);
}
