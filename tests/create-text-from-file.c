/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

#include <check.h>
#include <sys/stat.h>

#include <config.h>

#include "test-text.h"

START_TEST (cmp_text_file_size)
{
    // Get the size of the file and compare it to sesh_text_size().

    struct stat st;
    fail_unless (-1 != stat (GPL3_FILE, &st));

    fail_unless (st.st_size == sesh_text_size (text));
}
END_TEST

START_TEST (length_matches_size)
{
    // For an ASCII file, length and size are the same.
    fail_unless (sesh_text_size (text) == sesh_text_len (text));
}
END_TEST

START_TEST (point_initial_state)
{
    /* The point initially lives at the end of the sequence. Here, that
     * means at [filesize]. */

    sesh_marker_handle_t point = sesh_point (text);

    struct stat st;
    fail_unless (-1 != stat (GPL3_FILE, &st));

    fail_unless (st.st_size == sesh_itor_rdist (sesh_iom (point),
						sesh_text_begin (text)));
}
END_TEST


void
create_text_from_file_tcase (TCase *tcase, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tcase, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_gpl3_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tcase, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_gpl3_utf8_text, teardown_text);
	break;
    }

    tcase_add_test (tcase, cmp_text_file_size);
    tcase_add_test (tcase, length_matches_size);
    tcase_add_test (tcase, point_initial_state);
}
