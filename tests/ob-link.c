/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

/* Test object link feature with a context-free object cache.
 * Depend on obcache, obcache_ctx feature. */

#include <check.h>

#include "test-obcache.h"

START_TEST (ob_link_ctx_free)
{
    struct foo *obs[2];

    // Acquire a first ob and fill it.
    obs[0] = obcache_alloc (obcache);
    fail_unless (NULL != obs[0]);
    obs[0]->bar = 0;
    obs[0]->baz = 'a';

    // Acquire a second ob and fill it.
    obs[1] = obcache_alloc (obcache);
    fail_unless (NULL != obs[1]);
    obs[1]->bar = 1;
    obs[1]->baz = 'b';

    // Link the first object to the second one.
    link_ob (obs[0], obs[1]);

    // Check the linkage did not perturb both obs' content.
    fail_unless (0 == obs[0]->bar);
    fail_unless ('a' == obs[0]->baz);
    fail_unless (1 == obs[1]->bar);
    fail_unless ('b' == obs[1]->baz);

    // Dereference link of the first ob and check we get the second ob.
    void *tmp = next_ob (obs[0]);
    fail_unless (tmp == obs[1]);

    obcache_free (obcache, obs[0]);
    obcache_free (obcache, obs[1]);
}
END_TEST

TCase *
ob_link_ctx_free_tcase (void)
{
    static TCase *tcase = NULL;

    if (tcase)
	return tcase;

    tcase = tcase_create ("ob-link-ctx-free");
    tcase_add_checked_fixture (tcase,
			       setup_ctx_free_obcache, teardown_obcache);

    tcase_add_test (tcase, ob_link_ctx_free);

    return tcase;
}



START_TEST (ob_link_ctx_bound)
{
    struct foo *obs[2];
    ob_handle_t ob_hdls[2];

    // Acquire a first object and fill it.
    obs[0] = obcache_alloc (obcache);
    fail_unless (NULL != obs[0]);
    obs[0]->bar = 0;
    obs[0]->baz = 'a';
    ob_hdls[0] = ob_handle (obs[0]);

    // Acquire a second object and fill it.
    obs[1] = obcache_alloc (obcache);
    fail_unless (NULL != obs[1]);
    obs[1]->bar = 1;
    obs[1]->baz = 'b';
    ob_hdls[1] = ob_handle (obs[1]);

    // Link the first object to the second one.
    handle_link_ob (ob_hdls[0], ob_hdls[1]);

    // Check the linkage did not perturb both ob_hdls' content.
    fail_unless (0 == obs[0]->bar);
    fail_unless ('a' == obs[0]->baz);
    fail_unless (1 == obs[1]->bar);
    fail_unless ('b' == obs[1]->baz);

    // Dereference link of the first object and check we get the second ob.
    ob_handle_t tmp = next_ob_handle (ob_hdls[0]);
    fail_unless (tmp == ob_hdls[1]);

    obcache_free (obcache, obs[0]);
    obcache_free (obcache, obs[1]);
}
END_TEST

TCase *
ob_link_ctx_bound_tcase (void)
{
    static TCase *tcase = NULL;

    if (tcase)
	return tcase;

    tcase = tcase_create ("ob-link-ctx-bound");
    tcase_add_checked_fixture (tcase,
			       setup_obcache_ctx, teardown_obcache_ctx);
    tcase_add_checked_fixture (tcase,
			       setup_ctx_bound_obcache, teardown_obcache);

    tcase_add_test (tcase, ob_link_ctx_bound);

    return tcase;
}
