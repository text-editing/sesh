/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

#include <check.h>

#include "test-text.h"

#define NBCHUNK 100

static sesh_tag_t tag_array[] = {
    SESH_TAG_BOLD,
    SESH_TAG_ITALIC,
    SESH_TAG_GHOST,
    SESH_TAG_FGCOLOR,
    SESH_TAG_BGCOLOR,
    SESH_TAG_UCOLOR,
    SESH_TAG_USER1,
    SESH_TAG_USER2
};

START_TEST (sequential_tag_then_untag)
{
    /* This test sequentially tags chunks of a text sequence then untag them
     * and check the result. */

    sesh_marker_handle_t point = sesh_point (text);
    sesh_marker_goto_off (point, 0);

    long chunk_len = gpl3.st.st_size / NBCHUNK;

    for (long chunk_counter = 0; chunk_counter < NBCHUNK; chunk_counter++) {
	 int8_t tag_idx = chunk_counter % NB_SESH_TAGS;

	 fail_unless (
	     chunk_len == sesh_tag (text, tag_array[tag_idx], chunk_len));
	 fail_unless (chunk_len == sesh_slide_marker (point, chunk_len));
    }

    // Now untag the sequence.
    sesh_marker_goto_off (point, 0);
    for (long chunk_counter = 0; chunk_counter < NBCHUNK; chunk_counter++) {
	int8_t tag_idx = chunk_counter % NB_SESH_TAGS;

	fail_unless (
	    chunk_len == sesh_untag (text, tag_array[tag_idx], chunk_len));
	 fail_unless (chunk_len == sesh_slide_marker (point, chunk_len));
    }

    // Check no cell is tagged.
    sesh_iterator_t it = sesh_text_begin (text);
    for (long chunk_counter = 0; chunk_counter < NBCHUNK; chunk_counter++)
	 for (sesh_len_t i = 0; i < chunk_len; sesh_fwd_itor (&it), i++)
	     fail_unless (SESH_TAG_NONE == sesh_cell_tags (it));
}
END_TEST

START_TEST (superposal_tag_then_untag)
{
    /* This test successively tags-then-untags growing chunks starting from
     * the beginning of the sequence and check the result for each
     * iteration. */

    sesh_marker_goto_off (sesh_point (text), 0);

    long chunk_len = gpl3.st.st_size / NBCHUNK;

    for (long chunk_counter = 0; chunk_counter < NBCHUNK; chunk_counter++) {
	int8_t tag_idx = chunk_counter % NB_SESH_TAGS;
	sesh_len_t len = chunk_len * chunk_counter;

	fail_unless (len == sesh_tag (text, tag_array[tag_idx], len));
	fail_unless (len == sesh_untag (text, tag_array[tag_idx], len));

	sesh_off_t off = 0;
	for (sesh_iterator_t it = sesh_text_begin (text);
	     off < len;
	     sesh_fwd_itor (&it), off++)
	    fail_if (tag_array[tag_idx] && sesh_cell_tags (it));
    }
}
END_TEST

void
untag_tcase (TCase *tcase, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tcase, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_gpl3_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tcase, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_gpl3_utf8_text, teardown_text);
    }

    tcase_add_checked_fixture (tcase,
			       setup_gpl3_sample, teardown_gpl3_sample);

    tcase_add_test (tcase, sequential_tag_then_untag);
    tcase_add_test (tcase, superposal_tag_then_untag);
}
