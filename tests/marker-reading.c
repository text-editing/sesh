/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

/* This test program loads an ASCII-encoded file with ASCII and UTF-8
 * encodings and try to reread it in an output file using a marker. */

#include <check.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include <safe-alloc.h>

#include "test-text.h"

START_TEST (marker_forward_reading)
{
    char *output;
    fail_if (-1 == ALLOC_N (output, (size_t)gpl3.st.st_size));

    sesh_marker_handle_t m = checked_mark_off (text, 0);

    for (sesh_off_t i = 0; i < gpl3.st.st_size; sesh_fwd_marker (m), i++)
	output[i] = noattr_bytecell (sesh_iom (m));

    fail_unless (sesh_itor_eq_p (checked_text_end (text), sesh_iom (m)));

    fail_unless (0 == memcmp (gpl3.ptr, output, (size_t)gpl3.st.st_size));

    FREE (output);
}
END_TEST

START_TEST (marker_backward_reading)
{
    char *output;
    fail_if (-1 == ALLOC_N (output, (size_t)gpl3.st.st_size));

    sesh_marker_handle_t m = checked_mark_off (text, gpl3.st.st_size - 1);

    for (sesh_off_t i = 0; i < gpl3.st.st_size; sesh_bwd_marker (m), i++)
	output[gpl3.st.st_size-1-i] = noattr_bytecell (sesh_iom (m));

    fail_unless (sesh_itor_eq_p (checked_text_rend (text), sesh_iom (m)));

    fail_unless (0 == memcmp (gpl3.ptr, output, (size_t)gpl3.st.st_size));

    FREE (output);
}
END_TEST

void
marker_reading_tcase (TCase *tcase, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tcase, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_gpl3_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tcase, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_gpl3_utf8_text, teardown_text);
	break;
    }

    tcase_add_checked_fixture (tcase,
			       setup_gpl3_sample, teardown_gpl3_sample);

    tcase_add_test (tcase, marker_forward_reading);
    tcase_add_test (tcase, marker_backward_reading);
}
