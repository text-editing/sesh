/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

#include <check.h>
#include <sys/stat.h>

#include "test-text.h"

START_TEST (marker_double_scan)
{
    // This test performs a double scanning using a marker.

    struct stat st;
    fail_unless (-1 != stat (GPL3_FILE, &st));

    sesh_iterator_t begin = sesh_text_begin (text);

    sesh_marker_handle_t m = checked_mark_off (text, 0);

    fail_unless (' ' == noattr_bytecell (sesh_iom (m)));
    fail_unless (0 == sesh_itor_rdist (sesh_iom (m), begin));

    fail_unless (SESH_SUCCESS == sesh_fwd_marker (m));
    fail_unless (' ' == noattr_bytecell (sesh_iom (m)));
    fail_unless (1 == sesh_itor_rdist (sesh_iom (m), begin));

    fail_unless (19 == sesh_slide_marker (m, 19));
    fail_unless ('G' == noattr_bytecell (sesh_iom (m)));
    fail_unless (20 == sesh_itor_rdist (sesh_iom (m), begin));

    // Try to slide over the end.
    fail_unless
	(st.st_size - 20 == sesh_slide_marker (m, st.st_size - 20 + 1));
    fail_unless (sesh_itor_eq_p (sesh_iom (m), checked_text_end (text)));
    fail_unless (st.st_size == sesh_itor_rdist (sesh_iom (m), begin));

    fail_unless (SESH_SUCCESS == sesh_bwd_marker (m));
    fail_unless ('\n' == noattr_bytecell (sesh_iom (m)));
    fail_unless (st.st_size - 1 == sesh_itor_rdist (sesh_iom (m), begin));

    fail_unless (-2 == sesh_slide_marker (m, -2));
    fail_unless ('w' == noattr_bytecell (sesh_iom (m)));
    fail_unless (st.st_size - 3 == sesh_itor_rdist (sesh_iom (m), begin));

    // Try to slide over the rend.
    sesh_off_t off = sesh_itor_rdist (sesh_iom (m), begin);
    fail_unless (-(off + 1) == sesh_slide_marker (m, -(off + 1 + 1)));
    fail_unless (sesh_itor_eq_p (sesh_iom (m), checked_text_rend (text)));
    fail_unless (sesh_itor_rdist (sesh_iom (m), begin));

    sesh_destroy_marker (m);
}
END_TEST

void
marker_double_scan_tcase (TCase *tcase, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tcase, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_gpl3_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tcase, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_gpl3_utf8_text, teardown_text);
	break;
    }

    tcase_add_test (tcase, marker_double_scan);
}
