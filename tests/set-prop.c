/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

#include <check.h>

#include "test-text.h"

#define NBCHUNK 100

static sesh_prop_t prop_array[] = {
    SESH_PROP_FGCOLOR,
    SESH_PROP_BGCOLOR,
    SESH_PROP_UCOLOR,
    SESH_PROP_USER1,
    SESH_PROP_USER2
};

START_TEST (sequential_set_props)
{
    /* This test sequentially set chunk properties of a text sequence then
     * check the result. */

    sesh_marker_handle_t point = sesh_point (text);
    sesh_marker_goto_off (point, 0);

    long chunk_len = gpl3.st.st_size / NBCHUNK;

    for (long chunk_counter; chunk_counter < NBCHUNK; chunk_counter++) {
	int8_t prop_idx = chunk_counter % NB_SESH_PROPS;

	fail_unless (
	    chunk_len == sesh_set_prop (text, prop_array[prop_idx], prop_idx,
					chunk_len));

	fail_unless (chunk_len == sesh_slide_marker (point, chunk_len));
    }

    // Now, reread the sequence and check the properties.
    sesh_iterator_t it = sesh_text_begin (text);
    for (long chunk_counter = 0; chunk_counter < NBCHUNK; chunk_counter++) {
	 int8_t prop_idx = chunk_counter % NB_SESH_PROPS;

	 for (sesh_len_t i = 0; i < chunk_len; sesh_fwd_itor (&it), i++)
	     fail_unless (
		 prop_idx == sesh_cell_prop (it, prop_array[prop_idx]));
    }
}
END_TEST

START_TEST (superposal_set_props)
{
    /* This test successively set properties of growing chunks starting from
     * the beginning of the sequence and check the result for each
     * iteration. */

    sesh_marker_goto_off (sesh_point (text), 0);

    long chunk_len = gpl3.st.st_size / NBCHUNK;

    for (long chunk_counter = 0; chunk_counter < NBCHUNK; chunk_counter++) {
	int8_t prop_idx = chunk_counter % NB_SESH_PROPS;
	sesh_len_t len = chunk_len * chunk_counter;

	fail_unless (
	    len == sesh_set_prop (text, prop_array[prop_idx], prop_idx, len));

	sesh_off_t off = 0;
	for (sesh_iterator_t it = sesh_text_begin (text);
	     off < len;
	     sesh_fwd_itor (&it), off++)
	    fail_unless (
		prop_idx == sesh_cell_prop (it, prop_array[prop_idx]));
    }
}
END_TEST

void
set_prop_tcase (TCase *tcase, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tcase, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_gpl3_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tcase, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_gpl3_utf8_text, teardown_text);
    }

    tcase_add_checked_fixture (tcase,
			       setup_gpl3_sample, teardown_gpl3_sample);

    tcase_add_test (tcase, sequential_set_props);
    tcase_add_test (tcase, superposal_set_props);
}
