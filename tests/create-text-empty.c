/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

#include <check.h>

#include "test-text.h"


START_TEST (enc_accessor)
{
    // Check the encoding name accessor.
    fail_unless (0 == strcmp (sesh_text_enc (text), enc));
}
END_TEST

START_TEST (initial_size)
{
    // Check the initial size.
    fail_unless (0 == sesh_text_size (text));
}
END_TEST

START_TEST (initial_length)
{
    // Check the initial length.
    fail_unless (0 == sesh_text_len (text));
}
END_TEST

START_TEST (point_initial_state)
{
    // Check initial properties of the point.

    sesh_marker_handle_t point = sesh_point (text);

    // The point initially lives at the end of the sequence.
    fail_unless (sesh_itor_eq_p (checked_text_end (text), sesh_iom (point)));

    // The point is owned by the text sequence.
    fail_unless (sesh_marker_owner (point) == text);
}
END_TEST

START_TEST (initially_begin_is_end)
{
    // For an empty text sequence, the begin position matches the end.

    sesh_iterator_t begin_it = sesh_text_begin (text);
    sesh_iterator_t end_it = checked_text_end (text);
    fail_unless (sesh_itor_eq_p (begin_it, end_it));
    fail_unless (0 == sesh_itor_rdist (begin_it, end_it));
}
END_TEST

START_TEST (initially_rbegin_is_rend)
{
    // For an empty text sequence, the rbegin position matches the rend.

    sesh_iterator_t rbegin_it = sesh_text_rbegin (text);
    sesh_iterator_t rend_it = checked_text_rend (text);
    fail_unless (sesh_itor_eq_p (rbegin_it, rend_it));
    fail_unless (0 == sesh_itor_rdist (rbegin_it, rend_it));
}
END_TEST

START_TEST (rend_end_initial_distance)
{
    // For an empty text sequence, distance between end and rend is 1.
    sesh_iterator_t end_it = checked_text_end (text);
    sesh_iterator_t rend_it = checked_text_rend (text);

    fail_unless (1 == sesh_itor_rdist (end_it, rend_it));
    fail_unless (-1 == sesh_itor_rdist (rend_it, end_it));
}
END_TEST


void
create_text_empty_tcase (TCase *tcase, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tcase, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_empty_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tcase, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_empty_utf8_text, teardown_text);

	break;
    }

    tcase_add_test (tcase, enc_accessor);
    tcase_add_test (tcase, initial_size);
    tcase_add_test (tcase, initial_length);
    tcase_add_test (tcase, point_initial_state);
    tcase_add_test (tcase, initially_begin_is_end);
    tcase_add_test (tcase, initially_rbegin_is_rend);
    tcase_add_test (tcase, rend_end_initial_distance);
}
