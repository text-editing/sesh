/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

#include <check.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#include <unistd.h>

#include <safe-alloc.h>

#include "test-text.h"

static bool
my_strcmp (sesh_iterator_t it, sesh_iterator_t end, const char *string)
{
    unsigned matched_len = 0;

    for (; ! sesh_itor_eq_p (it, end); sesh_fwd_itor (&it))
	if (sesh_bytecell (it) == string[matched_len]) {
	    ++matched_len;

	    if (matched_len == strlen (string))
		return true;
	} else
	    break;

    return false;
}

bool
match_string_forward (sesh_marker_handle_t m, const char *string)
{
    sesh_iterator_t start_it = sesh_iom (m);
    sesh_iterator_t end_it = checked_text_end (text);

    for (sesh_iterator_t it = start_it;
	 ! sesh_itor_eq_p (it, end_it);
	 sesh_fwd_itor (&it))
	if (sesh_bytecell (it) == string[0])
	    if (my_strcmp (it, end_it, string)) {
		sesh_slide_marker (m, sesh_itor_rdist (it, start_it));
		return true;
	    }

    return false;
}


bool
match_string_backward (sesh_marker_handle_t m, const char *string)
{
    sesh_iterator_t start_it = sesh_iom (m);
    sesh_iterator_t rend_it = checked_text_rend (text);


    for (sesh_iterator_t it = start_it;
	 ! sesh_itor_eq_p (it, rend_it);
	 sesh_bwd_itor (&it))
	if (sesh_bytecell (it) == string[0])
	    if (my_strcmp (it, start_it, string)) {
		sesh_slide_marker (m, sesh_itor_rdist (it, start_it));
		return true;
	    }

    return false;
}

START_TEST (bidirectionnal_upcase)
{
#   define FWD_STRING "the"
#   define FWD_STRING_SUBST "THE"
#   define FWD_STRING_LEN ((long)strlen (FWD_STRING))

#   define BWD_STRING "to"
#   define BWD_STRING_SUBST "TO"
#   define BWD_STRING_LEN ((long)strlen (BWD_STRING))

    sesh_marker_handle_t point = sesh_point (text);
    sesh_marker_handle_t fwd_m = sesh_mark_off (text, 0);
    sesh_marker_handle_t bwd_m = sesh_mark_off (text, sesh_text_len (text)-1);

    bool fwd_scan = true;
    int scan_completed = 0;

    while (scan_completed < 3)
	if (fwd_scan) {
	    bool ret = match_string_forward (fwd_m, FWD_STRING);
	    if (ret) {
		sesh_slide_marker (fwd_m, FWD_STRING_LEN);
		sesh_marker_goto_marker (point, fwd_m);
		sesh_erase (text, FWD_STRING_LEN);
		sesh_insert (text, FWD_STRING_LEN, FWD_STRING_SUBST);
	    } else
		scan_completed |= 1;

	    fwd_scan = false;
	} else {
	    bool ret = match_string_backward (bwd_m, BWD_STRING);
	    if (ret) {
		sesh_slide_marker (bwd_m, BWD_STRING_LEN);
		sesh_marker_goto_marker (point, bwd_m);
		sesh_erase (text, BWD_STRING_LEN);
		sesh_insert (text, BWD_STRING_LEN, BWD_STRING_SUBST);
		sesh_slide_marker (bwd_m, -(BWD_STRING_LEN+1));
	    } else
		scan_completed |= 2;

	    fwd_scan = true;
	}

    fail_unless (gpl3.st.st_size == sesh_text_size (text));
    fail_unless (gpl3.st.st_size == sesh_text_len (text));
    // Check text size and length did not change.

    char *output = byte_dump_text (text);

    int expected_fd = open (BIDIRECTIONNAL_UPCASE_FILE, O_RDONLY);
    fail_unless (expected_fd != 1);

    struct stat expected_stat;
    fail_unless (-1 != fstat (expected_fd, &expected_stat));

    const char *expected =
	(const char *)mmap (NULL, (size_t)expected_stat.st_size,
			    PROT_READ, MAP_SHARED, expected_fd, 0);
    fail_unless (MAP_FAILED != expected);
    close (expected_fd);

    fail_unless (0 == memcmp (output, expected, (size_t)gpl3.st.st_size));

    FREE (output);
    munmap ((void *)expected, (size_t)expected_stat.st_size);
}
END_TEST

void
bidirectionnal_upcase_tcase (TCase *tcase, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tcase, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_gpl3_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tcase, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_gpl3_utf8_text, teardown_text);
    }

    tcase_add_checked_fixture (tcase,
			       setup_gpl3_sample, teardown_gpl3_sample);

    tcase_add_test (tcase, bidirectionnal_upcase);
}
