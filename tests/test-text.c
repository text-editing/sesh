/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <check.h>
/* #include <assert.h> */
#include <stdlib.h>
#include <string.h>

#include <safe-alloc.h>

#include "test-text.h"

sesh_off_t gpl3_nl_offsets[] = {
    46, 93, 94, 163, 225, 284, 285, 322, 323, 388, 423, 424, 496, 567, 639,
    712, 783, 855, 925, 945, 946, 1013, 1084, 1155, 1224, 1293, 1351, 1352,
    1421, 1494, 1567, 1633, 1634, 1701, 1766, 1838, 1910, 1929, 1930, 2000,
    2069, 2135, 2136, 2208, 2279, 2347, 2417, 2447, 2448, 2515, 2588, 2651,
    2717, 2789, 2859, 2932, 3001, 3074, 3129, 3130, 3201, 3268, 3340, 3410,
    3482, 3537, 3538, 3603, 3624
};

char
checked_bytecell (sesh_iterator_t it)
{
    wchar_t wcell;

    // Read using sesh_cell().
    fail_unless (1 == sesh_cell (it, &wcell));
    char c1 = *(char *)&wcell;

    // Read using sesh_read_s() with len == 1.
    char c2;
    fail_unless (1 == sesh_read_s (1, &c2, it, 1));

    // Read using sesh_read() with len == 1.
    char c3;
    fail_unless (1 == sesh_read (&c3, it, 1));

    // Read using bytecell().
    char c4 = sesh_bytecell (it);

    // Compare results of the four methods.
    fail_unless (c1 == c2);
    fail_unless (c1 == c3);
    fail_unless (c1 == c4);

    return c1;
}

char
noattr_bytecell (sesh_iterator_t it)
{
    char c = checked_bytecell (it);

    // Check tags of the cell.
    fail_unless (SESH_TAG_NONE == sesh_cell_tags (it));

    // Check the property values.
    fail_unless (0 == sesh_cell_prop (it, SESH_PROP_FGCOLOR));
    fail_unless (0 == sesh_cell_prop (it, SESH_PROP_BGCOLOR));
    fail_unless (0 == sesh_cell_prop (it, SESH_PROP_UCOLOR));
    fail_unless (0 == sesh_cell_prop (it, SESH_PROP_USER1));
    fail_unless (0 == sesh_cell_prop (it, SESH_PROP_USER2));

    // Recheck attributes using sesh_cell_attrs.
    struct sesh_cellattrs attrs = sesh_cell_attrs (it);
    fail_unless (SESH_TAG_NONE == attrs.tags);
    fail_unless (0 == attrs.props[sesh_prop_idx (SESH_PROP_FGCOLOR)]);
    fail_unless (0 == attrs.props[sesh_prop_idx (SESH_PROP_BGCOLOR)]);
    fail_unless (0 == attrs.props[sesh_prop_idx (SESH_PROP_UCOLOR)]);
    fail_unless (0 == attrs.props[sesh_prop_idx (SESH_PROP_USER1)]);
    fail_unless (0 == attrs.props[sesh_prop_idx (SESH_PROP_USER2)]);

    return c;
}


char *
byte_dump_text (sesh_text_t *text)
{
    enum enc_id enc_id = get_enc_id (enc);
    sesh_len_t text_size = sesh_text_size (text);

    char *output;
    fail_if (-1 == ALLOC_N (output, ((size_t)text_size + 1)));

    sesh_iterator_t it = sesh_text_begin (text);
    for (sesh_off_t i = 0; i < text_size; sesh_fwd_itor (&it), i++) {
	/* sesh_itor_rdist() is currently too slow for non regular text
	 * encoding. */
	if (ENC_ID_ASCII == enc_id)
	    fail_unless (i == sesh_itor_off (text, it));

	// TODO: use checked_bytecell() here which is sufficient.
	output[i] = noattr_bytecell (it);

	if ('\n' == output[i])
	    fail_unless (sesh_nl_p (it));
    }

    fail_unless (sesh_itor_end_p (text, it));
    fail_unless (sesh_itor_eq_p (it, sesh_text_end (text)));

    return output;
}


sesh_iterator_t
checked_text_end (sesh_text_t *text)
{
    sesh_iterator_t it = sesh_text_end (text);

    fail_unless (sesh_itor_end_p (text, it));
    fail_unless (sesh_text_len (text) == sesh_itor_off (text, it));

    return it;
}

sesh_iterator_t
checked_text_rend (sesh_text_t *text)
{
    sesh_iterator_t it = sesh_text_rend (text);

    fail_unless (sesh_itor_rend_p (text, it));
    fail_unless (-1 == sesh_itor_off (text, it));

    return it;
}

sesh_marker_handle_t
checked_mark_off (sesh_text_t *text, sesh_off_t off)
{
    sesh_marker_handle_t m = sesh_mark_off (text, off);
    fail_if (SESH_NULL_MARKER == m);

    fail_unless (text == sesh_marker_owner (m));
    fail_unless (off == sesh_marker_off (m));

    return m;
}

sesh_marker_handle_t
checked_mark_roff (const sesh_marker_handle_t m, sesh_off_t roff)
{
    sesh_marker_handle_t m2 = sesh_mark_roff (m, roff);
    fail_if (SESH_NULL_MARKER == m2);

    fail_unless (sesh_marker_owner (m) == sesh_marker_owner (m2));
    fail_unless (sesh_marker_off (m) == sesh_marker_off (m));

    return m2;
}
