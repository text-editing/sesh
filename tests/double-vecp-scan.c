/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

/* This test program is similar to [vecp-double-scan] but uses two vector
 * pointers both performing a simple scan in opposite order. Distance and
 * order comparison checks are performed at each step. */

#include <check.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

#include "test-common.h"
#include "test-vec.h"

START_TEST (double_vecp_scan)
{
    fail_unless (gpl3.st.st_size <= grow_vec (vec, gpl3.st.st_size));
    fail_unless (vec_size (vec) >= gpl3.st.st_size);

    /* char c; */

    vecp_t vp1 = vec_begin (vec);
    vecp_t vp2 = vecp_at_roff (vp1, gpl3.st.st_size - 1);

    write_vec (vp1, gpl3.st.st_size, gpl3.ptr);

    fail_unless (0 > vecp_rdist (vp1, vp2));
    fail_unless (gpl3.st.st_size - 1 == vecp_rdist (vp2, vp1));

    vp1 = vecp_at_roff (vp1, 1812);
    vp2 = vecp_at_roff (vp2, -1812);
    fail_unless (0 == vecp_rdist (vp1, vp2));

    vp1 = vecp_at_roff (vp1, 1812);
    vp2 = vecp_at_roff (vp2, -1812);
    fail_unless (0 < vecp_rdist (vp1, vp2));
    fail_unless (1 - gpl3.st.st_size == vecp_rdist (vp2, vp1));
}
END_TEST

TCase *
double_vecp_scan_tcase (void)
{
    static TCase *tcase = NULL;

    if (tcase)
	return tcase;

    tcase = tcase_create ("double-vecp-scan");

    tcase_add_checked_fixture (tcase, setup_vec_ctx, teardown_vec_ctx);
    tcase_add_checked_fixture (tcase, setup_vec, teardown_vec);
    tcase_add_checked_fixture (tcase, setup_gpl3_sample,
			       teardown_gpl3_sample);

    tcase_add_test (tcase, double_vecp_scan);

    return tcase;
}
