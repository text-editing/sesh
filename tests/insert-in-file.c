/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

#include <check.h>

#include <safe-alloc.h>

#include "test-text.h"

START_TEST (insert_after)
{
    // Insert the content of a file after the content of the text sequence.

    fail_unless (gpl3.st.st_size == sesh_insert (text,
						 gpl3.st.st_size,
						 gpl3.ptr));

    fail_unless (2 * gpl3.st.st_size == sesh_text_len (text));
    // Check length.

    fail_unless (2 * gpl3.st.st_size == sesh_text_size (text));
    // Check size.

    char *output = byte_dump_text (text);
    fail_if (NULL == output);

    fail_unless (0 == memcmp (output, gpl3.ptr, (size_t)gpl3.st.st_size));
    fail_unless (0 == memcmp (output + gpl3.st.st_size, gpl3.ptr,
			      (size_t)gpl3.st.st_size));
    // Check the content of the text sequence.

    FREE (output);
}
END_TEST

START_TEST (insert_before)
{
    // Insert the content of a file before the content of the text sequence.

    sesh_len_t textlen = sesh_text_len (text);

    sesh_marker_handle_t point = sesh_point (text);
    fail_unless (-textlen == sesh_slide_marker (point, -textlen));

    fail_unless (gpl3.st.st_size == sesh_insert (text,
						 gpl3.st.st_size,
						 gpl3.ptr));

    fail_unless (2 * gpl3.st.st_size == sesh_text_len (text));
    // Check length.

    fail_unless (2 * gpl3.st.st_size == sesh_text_size (text));
    // Check size.

    char *output = byte_dump_text (text);
    fail_if (NULL == output);

    fail_unless (0 == memcmp (output, gpl3.ptr, (size_t)gpl3.st.st_size));
    fail_unless (0 == memcmp (output + gpl3.st.st_size, gpl3.ptr,
			      (size_t)gpl3.st.st_size));
    // Check the content of the text sequence.

    FREE (output);
}
END_TEST

START_TEST (insert_in_middle)
{
    /* Insert the content of a file in the middle of the content of the text
     * sequence. */

    sesh_marker_handle_t point = sesh_point (text);
    fail_unless (-200 == sesh_slide_marker (point, -200));

    fail_unless (gpl3.st.st_size == sesh_insert (text,
						 gpl3.st.st_size,
						 gpl3.ptr));

    fail_unless (2 * gpl3.st.st_size == sesh_text_len (text));
    // Check length.

    fail_unless (2 * gpl3.st.st_size == sesh_text_size (text));
    // Check size.
}
END_TEST


void
insert_in_file_tcase (TCase *tcase, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tcase, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_gpl3_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tcase, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_gpl3_utf8_text, teardown_text);
    }

    tcase_add_checked_fixture (tcase,
			       setup_gpl3_sample, teardown_gpl3_sample);

    tcase_add_test (tcase, insert_after);
    tcase_add_test (tcase, insert_before);
    tcase_add_test (tcase, insert_in_middle);
}
