/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

#include <check.h>

#include "test-text.h"

START_TEST (point_goto_end)
{
    /* Make the point move to the end of the sequence which already is its
     * current position. */

    sesh_marker_handle_t point = sesh_point (text);

    fail_unless (sesh_itor_eq_p (sesh_iom (point), checked_text_end (text)));

    fail_unless (SESH_SUCCESS == sesh_marker_goto_off (point, 0));
    fail_unless (sesh_itor_eq_p (sesh_iom (point), checked_text_end (text)));
}
END_TEST

START_TEST (point_goto_rend)
{
    // Make the point move to the rend of the sequence.

    sesh_marker_handle_t point = sesh_point (text);

    fail_unless (SESH_SUCCESS == sesh_marker_goto_off (point, -1));
    fail_unless (sesh_itor_eq_p (sesh_iom (point), checked_text_rend (text)));
}
END_TEST

START_TEST (goto_point)
{
    /* Taking a marker pointing on the rend of the sequence, request it to
     * move to the point. */

    sesh_marker_handle_t m = checked_mark_off (text, -1);

    fail_unless (SESH_SUCCESS == sesh_marker_goto_marker (m,
							  sesh_point (text)));
    fail_unless (sesh_itor_eq_p (sesh_iom (m), sesh_iom (sesh_point (text))));

    fail_unless (SESH_SUCCESS == sesh_destroy_marker (m));
}
END_TEST

void
moving_marker_tcase (TCase *tcase, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tcase, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_empty_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tcase, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_empty_utf8_text, teardown_text);
	break;
    }

    tcase_add_test (tcase, point_goto_end);
    tcase_add_test (tcase, point_goto_rend);
    tcase_add_test (tcase, goto_point);
}
