/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

// Test vector context feature.

#include <check.h>

#include "test-vec.h"

START_TEST (current_vec_ctx_at_start)
{
    // At the beginning, no vector context has been created.
    fail_unless (NULL == current_vec_ctx ());
}
END_TEST

START_TEST (first_vec_ctx_auto_current)
{
    struct vec_ctx *vec_ctx = vec_ctx = create_vec_ctx ();
    fail_if (NULL == vec_ctx);

    /* The firstly vector context created is automatically make the current
     * one. */
    fail_unless (current_vec_ctx () == vec_ctx);
}
END_TEST

START_TEST (empty_vec_ctx_destroyable)
{
    struct vec_ctx *vec_ctx = vec_ctx = create_vec_ctx ();
    fail_if (NULL == vec_ctx);

    /* As the created vector context do not yet own any vector, it can be
     * destroyed. */
    fail_unless (VEC_SUCCESS == destroy_vec_ctx (vec_ctx));

    // FIXME: what should be the state of current_vec_ctx() ???
}
END_TEST

TCase *
vec_ctx_tcase (void)
{
    static TCase *tcase = NULL;

    if (tcase)
	return tcase;

    tcase = tcase_create ("vec-ctx");
    tcase_add_test (tcase, current_vec_ctx_at_start);
    tcase_add_test (tcase, first_vec_ctx_auto_current);
    tcase_add_test (tcase, empty_vec_ctx_destroyable);

    return tcase;
}
