/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include "test-text.h"

START_TEST (forward_marker_over_nl)
{
    /* This test moves forward a marker over newline in the text, using
     * `sesh_stepf_marker2nl()' and compares results with the offset table
     * `gpl3_nl_offsets'. */

    sesh_marker_handle_t point = sesh_point (text);
    sesh_marker_goto_off (point, 0);

    long i = 0;
    long nlcount = sizeof (gpl3_nl_offsets) / sizeof  (sesh_off_t);

    sesh_errno_t ret;
    ret = sesh_matchf_marker2nl (point);
    for (; ret != SESH_EINPOS; ret = sesh_matchf_marker2nl (point)) {
	fail_unless (SESH_SUCCESS == ret);
	fail_unless (sesh_nl_p (sesh_iom (point)));

	fail_unless (sesh_marker_off (point) == gpl3_nl_offsets[i]);
	i++;
	if (nlcount == i)
	    break;

	sesh_fwd_marker (point);
    }

    fail_unless (i == nlcount);
}
END_TEST

START_TEST (backward_marker_over_nl)
{
    /* This test moves backward a marker over newline in the text, using
     * `sesh_stepf_marker2nl()' and compares results with the offset table
     * `gpl3_nl_offsets'. */

    sesh_marker_handle_t point = sesh_point (text);
    sesh_marker_goto_off (point, sesh_text_len (text) - 1);

    long nlcount = sizeof (gpl3_nl_offsets) / sizeof  (sesh_off_t);
    long i = nlcount - 1;

    sesh_errno_t ret;
    ret = sesh_matchb_marker2nl (point);
    for (; ret != SESH_EINPOS; ret = sesh_matchb_marker2nl (point)) {
	fail_unless (SESH_SUCCESS == ret);
	fail_unless (sesh_nl_p (sesh_iom (point)));

	fail_unless (sesh_marker_off (point) == gpl3_nl_offsets[i]);
	i--;
	if (-1 == i)
	    break;

	sesh_bwd_marker (point);
    }

    fail_unless (-1 == i);
}
END_TEST

START_TEST (slide_marker_over_nl)
{
    /* This test moves a marker over newline twice on the whole sequence, the
     * first time in the forward direction, the second time in the backward
     * direction. For each scan, it slides over newlines using
     * `sesh_slide_marker2nl()' using a step of 2. */

    sesh_marker_handle_t point = sesh_point (text);
    sesh_marker_goto_off (point, 0);

    long i = 1;
    long slide_step = 2;
    long nlcount = sizeof (gpl3_nl_offsets) / sizeof (sesh_off_t);

    long ret;
    ret = sesh_slide_marker2nl (point, slide_step);
    fail_unless (slide_step == ret);
    for (;
	 ret == slide_step;
	 ret = sesh_slide_marker2nl (point, slide_step)) {
	fail_unless (sesh_nl_p (sesh_iom (point)));

	fail_unless (sesh_marker_off (point) == gpl3_nl_offsets[i]);
	i += slide_step;
	if (i >= nlcount)
	    break;

	sesh_fwd_marker (point);
    }

    ret = slide_step;
    i -= slide_step;
    for (;
	 ret == -slide_step;
	 ret = sesh_slide_marker2nl (point, -slide_step)) {
	fail_unless (sesh_nl_p (sesh_iom (point)));

	fail_unless (sesh_marker_off (point) == gpl3_nl_offsets[i]);
	i -= slide_step;
	if (i < 0)
	    break;

	sesh_bwd_marker (point);
    }
}
END_TEST

void
marker_nl_matching_tcase (TCase *tc, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tc, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tc, setup_gpl3_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tc, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tc, setup_gpl3_utf8_text, teardown_text);
    }

    tcase_add_test (tc, forward_marker_over_nl);
    tcase_add_test (tc, backward_marker_over_nl);
    tcase_add_test (tc, slide_marker_over_nl);
}
