/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

// Test case for a basic reading using an iterator.

#include <check.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include <safe-alloc.h>

#include "test-text.h"

START_TEST (iterator_forward_reading)
{
    // cell-by-cell forward reading.

    char *output = byte_dump_text (text);

    fail_unless (0 == memcmp (gpl3.ptr, output, (size_t)gpl3.st.st_size));

    FREE (output);
}
END_TEST

START_TEST (iterator_backward_reading)
{
    // cell-by-cell backward reading.

    char *output;
    fail_if (-1 == ALLOC_N (output, (size_t)gpl3.st.st_size));

    sesh_iterator_t it = sesh_text_rbegin (text);

    for (sesh_off_t i = 0; i < gpl3.st.st_size; sesh_bwd_itor (&it), i++)
	output[gpl3.st.st_size-1-i] = noattr_bytecell (it);

    fail_unless (sesh_itor_rend_p (text, it));
    fail_unless (sesh_itor_eq_p (checked_text_rend (text), it));

    fail_unless (0 == memcmp (gpl3.ptr, output, (size_t)gpl3.st.st_size));

    FREE (output);
}
END_TEST

START_TEST (iterator_block_reading)
{
    // Read the whole text sequence in one shot using sesh_read().

    char *output;
    fail_if (-1 == ALLOC_N (output, (size_t)gpl3.st.st_size));

    fail_unless (gpl3.st.st_size == sesh_read (output,
					       sesh_text_begin (text),
					       sesh_text_size (text)));

    fail_unless (0 == memcmp (gpl3.ptr, output, (size_t)gpl3.st.st_size));

    FREE (output);
}
END_TEST

START_TEST (iterator_block_safe_reading)
{
    // Safely read the whole text sequence in one shot using sesh_read_s().

    char *output;
    fail_if (-1 == ALLOC_N (output, (size_t)gpl3.st.st_size));

    fail_unless (gpl3.st.st_size == sesh_read_s (gpl3.st.st_size, output,
						 sesh_text_begin (text),
						 sesh_text_size (text)));

    fail_unless (0 == memcmp (gpl3.ptr, output, (size_t)gpl3.st.st_size));

    FREE (output);
}
END_TEST


void
iterator_reading_tcase (TCase *tcase, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tcase, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_gpl3_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tcase, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_gpl3_utf8_text, teardown_text);
	break;
    }

    tcase_add_checked_fixture (tcase,
			       setup_gpl3_sample, teardown_gpl3_sample);

    tcase_add_test (tcase, iterator_forward_reading);
    tcase_add_test (tcase, iterator_backward_reading);
    tcase_add_test (tcase, iterator_block_reading);
    tcase_add_test (tcase, iterator_block_safe_reading);
}
