/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

#include <stdlib.h>
#include <check.h>

#include "test-text.h"

START_TEST (check_size)
{
    /* This test inserts the content of a file in a text sequence through its
     * point. Then, it erases the content of the buffer and check the size. */

    fail_unless (gpl3.st.st_size == sesh_erase (text, gpl3.st.st_size));

    fail_unless (0 == sesh_text_size (text));
}
END_TEST

START_TEST (check_len)
{
    /* This test inserts the content of a file in a text sequence through its
     * point. Then, it erases the content of the buffer and check the
     * length. */

    fail_unless (gpl3.st.st_size == sesh_erase (text, gpl3.st.st_size));

    fail_unless (0 == sesh_text_len (text));
}
END_TEST

START_TEST (erase_too_much)
{
    // Check returned value when one tries to erase too much than permitted.

    fail_unless (gpl3.st.st_size == sesh_erase (text, gpl3.st.st_size+1));
}
END_TEST

START_TEST (marker_stability_after_erase)
{
    // This test checks for marker stability after an erased region.

    fail_unless (gpl3.st.st_size == sesh_erase (text, gpl3.st.st_size));

    fail_unless (sesh_itor_eq_p (sesh_iom (sesh_point (text)),
				 checked_text_end (text)));
}
END_TEST

START_TEST (marker_stability_before_erase)
{
    // This test checks for marker stability before an erased region.

    sesh_marker_handle_t m = checked_mark_off (text, -1);

    fail_unless (gpl3.st.st_size == sesh_erase (text, gpl3.st.st_size));

    fail_unless (sesh_itor_eq_p (sesh_iom (m), checked_text_rend (text)));

    fail_unless (SESH_SUCCESS == sesh_destroy_marker (m));
}
END_TEST

START_TEST (marker_persistance_over_erase)
{
    /* This test checks that a marker which lived in an erased region survived
     * just after this region. */

    sesh_marker_handle_t m = checked_mark_off (text, 200);

    fail_unless (gpl3.st.st_size == sesh_erase (text, gpl3.st.st_size));

    fail_unless (sesh_itor_eq_p (sesh_iom (m), checked_text_end (text)));

    fail_unless (SESH_SUCCESS == sesh_destroy_marker (m));
}
END_TEST

void
erase_tcase (TCase *tcase, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tcase, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_empty_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tcase, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_empty_utf8_text, teardown_text);
    }

    tcase_add_checked_fixture (tcase,
			       setup_gpl3_sample, teardown_gpl3_sample);
    tcase_add_checked_fixture (tcase, setup_insert_gpl3, NULL);

    tcase_add_test (tcase, check_size);
    tcase_add_test (tcase, check_len);
    tcase_add_test (tcase, erase_too_much);
    tcase_add_test (tcase, marker_stability_after_erase);
    tcase_add_test (tcase, marker_stability_before_erase);
    tcase_add_test (tcase, marker_persistance_over_erase);
}
