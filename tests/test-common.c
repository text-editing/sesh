/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <check.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>

#include "test-common.h"

void
setup_gpl3_sample (void)
{
    gpl3.fd = open (GPL3_FILE, O_RDONLY);
    fail_if (-1 == gpl3.fd);

    fail_if (-1 == fstat (gpl3.fd, &gpl3.st));

    gpl3.ptr = mmap (NULL, (size_t)gpl3.st.st_size,
		     PROT_READ, MAP_PRIVATE, gpl3.fd, 0);
    fail_if (MAP_FAILED == gpl3.ptr);
}

void
teardown_gpl3_sample (void)
{
    fail_if (-1 == munmap ((void *)gpl3.ptr, (size_t)gpl3.st.st_size));
    fail_if (-1 == close (gpl3.fd));
}
