/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

/* Test object handling feature with a context-free object cache.
 * Depend on obcache, obcache_ctx feature. */

#include <check.h>

#include "test-obcache.h"


START_TEST (ob_ctx_free)
{
    struct foo *obs[2];

    // Allocate a first object and fill it.
    obs[0] = obcache_alloc (obcache);
    fail_unless (NULL != obs[0]);
    obs[0]->bar = 0;
    obs[0]->baz = 'a';

    // Acquire a second ob and fill it.
    obs[1] = obcache_alloc (obcache);
    fail_unless (NULL != obs[1]);
    obs[1]->bar = 1;
    obs[1]->baz = 'b';


    /* Check that acquisition of the second object did not perturb the first
     * one. */
    fail_unless (0 == obs[0]->bar);
    fail_unless ('a' == obs[0]->baz);

    /* Release the second ob and check again it did not perturb the first
     * one. */
    obcache_free (obcache, obs[1]);
    fail_unless (0 == obs[0]->bar);
    fail_unless ('a' == obs[0]->baz);
}
END_TEST

TCase *
ob_ctx_free_tcase (void)
{
    static TCase *tcase = NULL;

    if (tcase)
	return tcase;

    tcase = tcase_create ("ob-ctx-free");
    tcase_add_checked_fixture (tcase,
			       setup_ctx_free_obcache, teardown_obcache);

    tcase_add_test (tcase, ob_ctx_free);

    return tcase;
}


START_TEST (ob_ctx_bound)
{
    struct foo *obs[2];

    // Allocate a first object and fill it.
    obs[0] = obcache_alloc (obcache);
    fail_unless (NULL != obs[0]);
    obs[0]->bar = 0;
    obs[0]->baz = 'a';

    // Acquire a second object and fill it.
    obs[1] = obcache_alloc (obcache);
    fail_unless (NULL != obs[1]);
    obs[1]->bar = 1;
    obs[1]->baz = 'b';

    // Check ob_cptr (ob_hdl) is the identity operation for both objectss.
    fail_unless (ob_ptr (ob_handle (obs[0])) == obs[0]);
    fail_unless (ob_ptr (ob_handle (obs[1])) == obs[1]);

    /* Check that acquisition of the second object did not perturb the first
     * one. */
    fail_unless (0 == obs[0]->bar);
    fail_unless ('a' == obs[0]->baz);

    /* Release the second ob and check again it did not perturb the first
     * one. */
    obcache_free (obcache, obs[1]);
    fail_unless (0 == obs[0]->bar);
    fail_unless ('a' == obs[0]->baz);

    obcache_free (obcache, obs[0]);
}
END_TEST

TCase *
ob_ctx_bound_tcase (void)
{
    static TCase *tcase = NULL;

    if (tcase)
	return tcase;

    tcase = tcase_create ("ob-ctx-bound");
    tcase_add_checked_fixture (tcase,
			       setup_obcache_ctx, teardown_obcache_ctx);
    tcase_add_checked_fixture (tcase,
			       setup_ctx_bound_obcache, teardown_obcache);

    tcase_add_test (tcase, ob_ctx_bound);

    return tcase;
}
