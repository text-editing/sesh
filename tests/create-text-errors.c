/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

#include <check.h>

#include "test-text.h"

START_TEST (enoenc)
{
    // Check error handling when trying to create a text with a bad encoding.

    sesh_text_t *text = sesh_make_text ("FOO-ENCODING", NULL);
    fail_unless (NULL == text);
    fail_unless (SESH_ENOENC == sesh_errno);
}
END_TEST

START_TEST (enoent)
{
    /* Check error handling when trying to create a text sequence with an
     * inexistent file. */

#   define BAD_FILE "/foo/bar/baz/a-probably-inexistent-file"

    sesh_text_t *text = sesh_make_text ("ASCII", BAD_FILE);
    fail_unless (NULL == text);
    fail_unless (SESH_ENOENT == sesh_errno);
}
END_TEST


void
create_text_errors_tcase (TCase *tcase)
{
    tcase_add_test (tcase, enoenc);
    tcase_add_test (tcase, enoent);
}
