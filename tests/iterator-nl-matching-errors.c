/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include "test-text.h"

START_TEST (iterate_over_nl_error)
{
    /* This test steps forward for a newline inside a region which does not
     * contain them. */

    sesh_iterator_t it = sesh_text_begin (text);

    sesh_iterator_t end_region = sesh_text_begin (text);
    sesh_fwd_itor (&end_region);

    sesh_matchf_itor2nl (&it, end_region);
    fail_unless (sesh_itor_eq_p (it, end_region));
    fail_if (sesh_nl_p (it));
}
END_TEST

START_TEST (backward_iterate_over_nl_error)
{
    /* This test steps backward for a newline inside a region which does not
     * contain them. */

    sesh_iterator_t it = sesh_text_begin (text);
    sesh_fwd_itor (&it);

    sesh_iterator_t end_region = sesh_text_begin (text);

    sesh_matchb_itor2nl (&it, end_region);
    fail_unless (sesh_itor_eq_p (it, end_region));
    fail_if (sesh_nl_p (it));
}
END_TEST

START_TEST (slide_over_nl_error)
{
    /* This test slides over newline with a given step value inside a region
     * which does not contain as many as newlines. */

    sesh_iterator_t it = sesh_text_begin (text);

    sesh_iterator_t end_region = sesh_text_begin (text);
    sesh_slide_itor (&end_region, 47); // This is just after the first nl.

    fail_unless (1 == sesh_slide_itor2nl (&it, end_region, 2));
    fail_unless (sesh_itor_eq_p (it, end_region));
    fail_if (sesh_nl_p (it));

    end_region = sesh_text_begin (text);

    fail_unless (-1 == sesh_slide_itor2nl (&it, end_region, -2));
    fail_unless (sesh_itor_eq_p (it, end_region));
    fail_if (sesh_nl_p (it));
}
END_TEST

void
iterator_nl_matching_errors_tcase (TCase *tc, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tc, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tc, setup_gpl3_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tc, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tc, setup_gpl3_utf8_text, teardown_text);
    }

    tcase_add_test (tc, iterate_over_nl_error);
    tcase_add_test (tc, backward_iterate_over_nl_error);
    tcase_add_test (tc, slide_over_nl_error);
}
