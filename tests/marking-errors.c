/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

#include <check.h>

#include "test-text.h"

START_TEST (point_is_undestroyable)
{
    // Point is undestroyable.

    // The point is undestroyable.
    fail_unless (SESH_EPERM == sesh_destroy_marker (sesh_point (text)));
}
END_TEST

START_TEST (mark_after_end)
{
    // We cannot mark after the end.

    sesh_marker_handle_t m = sesh_mark_roff (sesh_point (text), 1);
    fail_unless (SESH_NULL_MARKER == m);
    fail_unless (SESH_EINPOS == sesh_errno);
}
END_TEST

START_TEST (mark_before_rend)
{
    // We cannot mark before the end.

    sesh_marker_handle_t m = sesh_mark_roff (sesh_point (text), -2);
    fail_unless (SESH_NULL_MARKER == m);
    fail_unless (SESH_EINPOS == sesh_errno);
}
END_TEST


void
marking_errors_tcase (TCase *tcase, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tcase, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_empty_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tcase, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_empty_utf8_text, teardown_text);
	break;
    }

    tcase_add_test (tcase, point_is_undestroyable);
    tcase_add_test (tcase, mark_after_end);
    tcase_add_test (tcase, mark_before_rend);
}
