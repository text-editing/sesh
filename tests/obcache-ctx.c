/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

// Test case for object cache context related features.

#include <check.h>

#include "test-obcache.h"

START_TEST (valid_obcache_ctx_at_start)
{
    // No object cache context is valid at the beginning.
    fail_unless (NULL == valid_obcache_ctx ());
}
END_TEST

START_TEST (first_obcache_ctx_auto_valid)
{
    /* The firstly created object cache context is automatically made the
     * valid one. */

    struct obcache_ctx *obcache_ctx = create_obcache_ctx ();
    fail_if (NULL == obcache_ctx);

    fail_unless (obcache_ctx == valid_obcache_ctx());
}
END_TEST

START_TEST (empty_obcache_ctx_destroyable)
{
    // An empty object cache context is destroyable.

    struct obcache_ctx *obcache_ctx = create_obcache_ctx ();
    fail_if (NULL == obcache_ctx);

    fail_unless (OBCACHE_SUCCESS == destroy_obcache_ctx (obcache_ctx));
}
END_TEST


TCase *
obcache_ctx_tcase (void)
{
    static TCase *tcase = NULL;

    if (tcase)
	return tcase;

    tcase = tcase_create ("obcache-ctx");
    tcase_add_test (tcase, valid_obcache_ctx_at_start);
    tcase_add_test (tcase, first_obcache_ctx_auto_valid);
    tcase_add_test (tcase, empty_obcache_ctx_destroyable);

    return tcase;
}
