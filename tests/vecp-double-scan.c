/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

/* This test program:
 * - create a vector;
 * - fill the vector with the file-s content given in first argument;
 * - make a vector pointer scans forward the vector until the end;
 * - make a second vector pointer scans backward from the reverse begin of the
 * vector to the reverse end.
 * - destroy the vector. */

#include <check.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "test-common.h"
#include "test-vec.h"

START_TEST (vecp_double_scan)
{
    fail_unless (gpl3.st.st_size <= grow_vec (vec, gpl3.st.st_size));
    fail_unless (vec_size (vec) >= gpl3.st.st_size);

    char c;
    vecp_t vp = vec_begin (vec);

    write_vec (vp, gpl3.st.st_size, gpl3.ptr);

    // Forward scanning.

    read_vec (1, &c, vp);
    fail_unless (' ' == c);

    vp = vecp_next (vp);
    read_vec (1, &c, vp);
    fail_unless (' ' == c);

    vp = vecp_at_roff (vp, 19);
    read_vec (1, &c, vp);
    fail_unless ('G' == c);

    vp = vecp_at_roff (vp, gpl3.st.st_size - 21); /* Should be at the reverse
						   * begin of the sequence. */
    read_vec (1, &c, vp);
    fail_unless ('\n' == c);


    // Backward scanning.

    vp = vecp_prev (vp);
    read_vec (1, &c, vp);
    fail_unless ('.' == c);

    vp = vecp_at_roff (vp, -10);
    read_vec (1, &c, vp);
    fail_unless ('i' == c);

    vp = vecp_at_roff (vp, -(gpl3.st.st_size -1 -1 -10)); /* Should be at the
							   * begin of the
							   * sequence. */
    read_vec (1, &c, vp);
    fail_unless (' ' == c);
}
END_TEST


TCase *
vecp_double_scan_tcase (void)
{
    static TCase *tcase = NULL;

    if (tcase)
	return tcase;

    tcase = tcase_create ("vecp-double-scan");

    tcase_add_checked_fixture (tcase, setup_vec_ctx, teardown_vec_ctx);
    tcase_add_checked_fixture (tcase, setup_vec, teardown_vec);
    tcase_add_checked_fixture (tcase,
			       setup_gpl3_sample, teardown_gpl3_sample);

    tcase_add_test (tcase, vecp_double_scan);

    return tcase;
}
