/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

#include <check.h>
#include <sys/stat.h>

#include "test-text.h"

START_TEST (iterator_double_scan)
{
    // Perform a double scan of a text sequence using an iterator.

    struct stat st;
    fail_unless (-1 != stat (GPL3_FILE, &st));

    sesh_iterator_t begin = sesh_text_begin (text);
    sesh_iterator_t it = begin;

    fail_unless (' ' == noattr_bytecell (it));
    fail_unless (0 == sesh_itor_rdist (it, begin));

    sesh_fwd_itor (&it);
    fail_unless (' ' == noattr_bytecell (it));
    fail_unless (1 == sesh_itor_rdist (it, begin));

    sesh_slide_itor (&it, 19);
    fail_unless ('G' == noattr_bytecell (it));
    fail_unless (20 == sesh_itor_rdist (it, begin));

    sesh_slide_itor (&it, st.st_size - 20); // Should be at end.
    fail_unless (sesh_itor_eq_p (checked_text_end (text), it));
    fail_unless (st.st_size == sesh_itor_rdist (it, begin));

    sesh_bwd_itor (&it);
    fail_unless ('\n' == noattr_bytecell (it));
    fail_unless (st.st_size-1 == sesh_itor_rdist (it, begin));

    sesh_slide_itor (&it, -2);
    fail_unless ('w' == noattr_bytecell (it));
    fail_unless (st.st_size-3 == sesh_itor_rdist (it, begin));

    sesh_slide_itor (&it, -(sesh_itor_rdist (it, begin) + 1));
    fail_unless (sesh_itor_eq_p (checked_text_rend (text), it));
    fail_unless (-1 == sesh_itor_rdist (it, begin));
}
END_TEST

void
iterator_double_scan_tcase (TCase *tcase, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tcase, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_gpl3_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tcase, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_gpl3_utf8_text, teardown_text);
	break;
    }

    tcase_add_test (tcase, iterator_double_scan);
}
