/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

#include <check.h>

#include "test-text.h"

/* <check.h> defines mark_point as a macro which conflicts with the name of
 * the test below. As I do not need this macro, I undefine it. */
#undef mark_point

START_TEST (clone_point)
{
    // Try to clone the point.

    sesh_marker_handle_t point = sesh_point (text);
    sesh_marker_handle_t m = sesh_clone_marker (point);
    fail_if (SESH_NULL_MARKER == m);

    fail_unless (sesh_marker_owner (point) == sesh_marker_owner (m));
    fail_unless (sesh_marker_off (point) == sesh_marker_off (m));

    fail_unless (sesh_itor_eq_p (sesh_iom (point), sesh_iom (m)));

    fail_unless (SESH_SUCCESS == sesh_destroy_marker (m));
}
END_TEST

START_TEST (mark_itself)
{
    // We try to request the point to mark itself.

    sesh_marker_handle_t m = checked_mark_roff (sesh_point (text), 0);

    fail_unless (sesh_itor_eq_p (sesh_iom (sesh_point (text)), sesh_iom (m)));

    fail_unless (SESH_SUCCESS == sesh_destroy_marker (m));
}
END_TEST

START_TEST (mark_rend)
{
    /* Taking the point which at the end of the sequence, we try to mark the
     * rend of the sequence. */

    sesh_marker_handle_t m = checked_mark_roff (sesh_point (text), -1);

    fail_unless (-1 == sesh_itor_rdist (sesh_iom (m),
					sesh_iom (sesh_point (text))));

    fail_unless (SESH_SUCCESS == sesh_destroy_marker (m));
}
END_TEST

START_TEST (from_rend_mark_itself)
{
    // After having moved the point to the rend, request it to mark itself.

    sesh_marker_handle_t point = sesh_point (text);
    fail_unless (SESH_SUCCESS == sesh_bwd_marker (point));

    sesh_marker_handle_t m = checked_mark_roff (sesh_point (text), 0);

    fail_unless (sesh_itor_eq_p (sesh_iom (point), sesh_iom (m)));

    fail_unless (SESH_SUCCESS == sesh_destroy_marker (m));
}
END_TEST

START_TEST (from_rend_mark_end)
{
    /* After having moved the point to the rend, request it to mark end of the
     * sequence. */

    sesh_marker_handle_t point = sesh_point (text);
    fail_unless (SESH_SUCCESS == sesh_bwd_marker (point));

    sesh_marker_handle_t m = checked_mark_roff (sesh_point (text), 1);

    fail_unless (1 == sesh_itor_rdist (sesh_iom (m), sesh_iom (point)));

    fail_unless (SESH_SUCCESS == sesh_destroy_marker (m));
}
END_TEST


void
marking_tcase (TCase *tcase, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tcase, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_empty_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tcase, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_empty_utf8_text, teardown_text);
	break;
    }

    tcase_add_test (tcase, clone_point);
    tcase_add_test (tcase, mark_itself);
    tcase_add_test (tcase, mark_rend);
    tcase_add_test (tcase, from_rend_mark_itself);
    tcase_add_test (tcase, from_rend_mark_end);
}
