/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef TEST_OBCACHE_H_
#define TEST_OBCACHE_H_

#include <check.h>

#include <libutils/obcache.h>

// Test fixture states.
extern obcache_ctx_t *obcache_ctx;
extern obcache_t *obcache;

struct foo
{
    int bar;
    char baz;
};

static inline void
setup_obcache_ctx (void)
{
    obcache_ctx = create_obcache_ctx ();
    fail_if (NULL == obcache_ctx);
}

static inline void
teardown_obcache_ctx (void)
{
    fail_unless (OBCACHE_SUCCESS == destroy_obcache_ctx (obcache_ctx));
}

static inline void
setup_ctx_free_obcache (void)
{
    obcache = create_obcache (NULL, sizeof (struct foo), 0, NULL);
    fail_if (NULL == obcache);
}

static inline void
setup_ctx_bound_obcache (void)
{
    obcache = create_obcache (obcache_ctx, sizeof (struct foo), 0, NULL);
    fail_if (NULL == obcache);
}

static inline void
teardown_obcache (void)
{
    destroy_obcache (obcache);
}

#endif //!TEST_OBCACHE_H_
