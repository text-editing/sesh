/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef TEST_TEXT_H_
#define TEST_TEXT_H_

// Common stuff used for text interface testing.

#include <check.h>

#include <sesh/sesh.h>
#include <sesh/text.h>
#include <sesh/marker.h>
#include <sesh/iterator.h>
#include <sesh/line.h>

#include "test-common.h"

#define NB_SESH_TAGS 8
// Number of currently supported tags.

#define NB_SESH_PROPS 5
// Number of currently supported props.

#define ENC_ID_COUNT 2
// Number of encoding currently tested.

// Offset table referencing newline positions in the GPL3 sample file.
extern sesh_off_t gpl3_nl_offsets[69];

enum enc_id
{
    ENC_ID_ASCII = 0,
    ENC_ID_UTF8 = 1
};

static const char *enc_names[ENC_ID_COUNT] = {
    "ASCII",
    "UTF-8"
};

static inline enum enc_id
get_enc_id (const char *enc)
{
    for (int i = 0; i < ENC_ID_COUNT; i++)
	if (0 == strcmp (enc_names[i], enc))
	    return i;

    fail ("get_enc_id: bad encoding name given");

    // Not reached.
    return -1;
}


// Test fixtures
extern const char *enc;
extern sesh_text_t *text;

static inline void
setup_ascii_enc (void) {
    enc = enc_names[ENC_ID_ASCII];
}

static inline void
setup_utf8_enc (void)
{
    enc = enc_names[ENC_ID_UTF8];
}

static inline void
setup_empty_ascii_text (void)
{
    text = sesh_make_text ("ASCII", NULL);
    fail_if (NULL == text);
    fail_if (sesh_check_enomem());
}

static inline void
setup_empty_utf8_text (void)
{
    text = sesh_make_text ("UTF-8", NULL);
    fail_if (NULL == text);
    fail_if (sesh_check_enomem());
}

static inline void
setup_gpl3_ascii_text (void)
{
    text = sesh_make_text ("ASCII", GPL3_FILE);
    fail_if (NULL == text);
    fail_if (sesh_check_enomem());
}

static inline void
setup_gpl3_utf8_text (void)
{
    text = sesh_make_text ("UTF-8", GPL3_FILE);
    fail_if (NULL == text);
    fail_if (sesh_check_enomem());
}

static inline void
teardown_text (void)
{
    sesh_destroy_text (text);
}

static inline void
setup_insert_gpl3 (void)
{
    fail_unless (gpl3.st.st_size == sesh_insert (text,
						 gpl3.st.st_size,
						 gpl3.ptr));
}

char checked_bytecell (sesh_iterator_t);
/* Read the (assumed) bytecell pointed by the given iterator using the six
 * available methods:
 * - sesh_cell_s();
 * - sesh_cell()
 * - sesh_bytecell();
 * - sesh_widecell();
 * - sesh_read_s() (with len == 1);
 * - sesh_read() (with len == 1).
 *
 * Check the six methods gives the same result.
 *
 * Return the read bytecell. */

char noattr_bytecell (sesh_iterator_t);
/* Wrapper on top of checked_bytecell() which also check the read cell has no
 * attribute set (no tag and properties to a null value).
 *
 * Return the read bytecell. */

char * byte_dump_text (sesh_text_t *);
/* Dump the content of the given text sequence in a newly allocated buffer
 * (using Gnulib ALLOC()) and return this buffer.
 *
 * Dumping is performed using noattr_bytecell(). So this method is only valid
 * for one-byte encodings.
 *
 * The buffer is null-terminated and can be freed with FREE().
 */


// Wrappers on top of Sesh functions which also check for some properties.

sesh_iterator_t checked_text_end (sesh_text_t *);
/* Wrapper on top of `sesh_text_end()'. It also checks for the iterator
 * properties `sesh_itor_end_p()' and `sesh_itor_off()'. */

sesh_iterator_t checked_text_rend (sesh_text_t *);
/* Wrapper on top of `sesh_text_rend()'. It also checks for the iterator
 * properties `sesh_itor_rend_p()' and `sesh_itor_off()'. */

sesh_marker_handle_t checked_mark_off (sesh_text_t *, sesh_off_t);
/* This is a wrapper on top of `sesh_mark_off()'. It also checks for the
 * marker properties `sesh_marker_owner()' and `sesh_mark_off()'. */

sesh_marker_handle_t checked_mark_roff (const sesh_marker_handle_t,
					sesh_off_t);
/* This is a wrapper on top of `sesh_mark_roff()'. It also checks for the
 * marker properties `sesh_marker_owner()' and `sesh_mark_off()'. */

#endif //!TEST_TEXT_H_
