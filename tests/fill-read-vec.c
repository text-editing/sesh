/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

/* This test program:
 * - create a couple of vectors;
 * - grow these vectors so that they are big enough to store the file's
     content;
 * - fill the vectors with the file's content given in first argument:
 *   + the first vector is filled in one shot;
 *   + the second vector is filled char per char.
 * - reread both vectors  into the four left output files:
 *   + reread first in one shot;
 *   + reread secondly char per char.
 * - delete both vectors.
 */

#include <check.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <safe-alloc.h>

#include "test-common.h"
#include "test-vec.h"

START_TEST (atomic_fill_atomic_read)
{
    fail_unless (gpl3.st.st_size <= grow_vec (vec, gpl3.st.st_size));
    fail_unless (vec_size (vec) >= gpl3.st.st_size);

    vecp_t vp = vec_begin (vec);

    write_vec (vp, gpl3.st.st_size, gpl3.ptr);

    char *output;
    fail_if (-1 == ALLOC_N (output, (size_t)gpl3.st.st_size));

    // vp still points on the beginning of the vector.
    read_vec (gpl3.st.st_size, output, vp);

    fail_unless (0 == memcmp (output, gpl3.ptr, (size_t)gpl3.st.st_size));

    FREE (output);
}
END_TEST

START_TEST (basic_fill_read)
{
    fail_unless (gpl3.st.st_size <= grow_vec (vec, gpl3.st.st_size));
    fail_unless (vec_size (vec) >= gpl3.st.st_size);

    vecp_t vp = vec_begin (vec);

    for (long i = 0; i < gpl3.st.st_size; i++) {
 	write_vec (vp, 1, &gpl3.ptr[i]);
 	vp = vecp_next (vp);
    }

    char *output;
    fail_if (-1 == ALLOC_N (output, (size_t)gpl3.st.st_size));

    vp = vec_begin (vec);
    for (long i = 0; i < gpl3.st.st_size; i++) {
	output[i] = vecp_byte (vp);
	vp = vecp_next (vp);
    }

    fail_unless (0 == memcmp (output, gpl3.ptr, (size_t)gpl3.st.st_size));

    FREE (output);
}
END_TEST


TCase *
fill_read_vec_tcase (void)
{
    static TCase *tcase = NULL;

    if (tcase)
	return tcase;

    tcase = tcase_create ("fill-read-vec");

    tcase_add_checked_fixture (tcase, setup_vec_ctx, teardown_vec_ctx);
    tcase_add_checked_fixture (tcase, setup_vec, teardown_vec);
    tcase_add_checked_fixture (tcase,
			       setup_gpl3_sample, teardown_gpl3_sample);

    tcase_add_test (tcase, atomic_fill_atomic_read);
    tcase_add_test (tcase, basic_fill_read);

    return tcase;
}
