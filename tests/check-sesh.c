/* Copyright 2012 Ludovic Stordeur
 *
 * This file is part of Sesh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. */

// Test driver for the C interface. Built on top of the Check framework.

#include <stdlib.h>
#include <check.h>

#include "test-obcache.h"
#include "test-vec.h"
#include "test-text.h"


// Test cases for the Obcache interface testsuite.
TCase * obcache_ctx_tcase (void);
TCase * ctx_free_obcache_tcase (void);
TCase * ctx_bound_obcache_tcase (void);

TCase * ob_ctx_free_tcase (void);
TCase * ob_ctx_bound_tcase (void);
TCase * ob_link_ctx_free_tcase (void);
TCase * ob_link_ctx_bound_tcase (void);

// Test cases for the Vector interface testsuite.
TCase * vec_ctx_tcase (void);
TCase * vec_basic_tcase (void);
TCase * fill_read_vec_tcase (void);
TCase * vecp_double_scan_tcase (void);
TCase * double_vecp_scan_tcase (void);

// Test cases for the Sesh interface testsuite.
void create_text_empty_tcase (TCase *, const char *enc);
void create_text_from_file_tcase (TCase *, const char *enc);
void create_text_errors_tcase (TCase *);
void mark_off_tcase (TCase *, const char *enc);
void mark_off_errors_tcase (TCase *, const char *enc);
void marking_tcase (TCase *, const char *enc);
void marking_errors_tcase (TCase *, const char *enc);
void moving_marker_tcase (TCase *, const char *enc);
void moving_marker_errors_tcase (TCase *, const char *enc);
void iterator_reading_tcase (TCase *, const char *enc);
void marker_reading_tcase (TCase *, const char *enc);
void iterator_double_scan_tcase (TCase *, const char *enc);
void marker_double_scan_tcase (TCase *, const char *enc);
void insert_tcase (TCase *, const char *enc);
void insert_in_file_tcase (TCase *, const char *enc);
void erase_tcase (TCase *, const char *enc);
void bidirectionnal_upcase_tcase (TCase *, const char *enc);
void tag_tcase (TCase *, const char *enc);
void untag_tcase (TCase *, const char *enc);
void set_prop_tcase (TCase *, const char *enc);
void iterator_nl_matching_tcase (TCase *, const char *enc);
void iterator_nl_matching_errors_tcase (TCase *, const char *enc);
void marker_nl_matching_tcase (TCase *, const char *enc);
void marker_nl_matching_errors_tcase (TCase *, const char *enc);


// Global fixture states.
struct gpl3 gpl3;
obcache_ctx_t *obcache_ctx;
obcache_t *obcache;
struct vec_ctx *vec_ctx;
struct vec *vec;
const char *enc;
sesh_text_t *text;

#define REGISTER_TCASE(testsuite, name)		      \
    {						      \
	TCase *tcase = tcase_create (#name);	      \
	name ## _tcase (tcase);			      \
	suite_add_tcase (testsuite, tcase);	      \
    }

#define REGISTER_PARAM_2TCASES(testsuite, name, arg1, arg2)		\
    {									\
	TCase *tcase1 = tcase_create (#name "[" #arg1 "]");		\
	TCase *tcase2 = tcase_create (#name "[" #arg2 "]");		\
	name ## _tcase (tcase1, arg1);					\
	name ## _tcase (tcase2, arg2);				        \
	suite_add_tcase (testsuite, tcase1);			        \
	suite_add_tcase (testsuite, tcase2);			        \
    }

int
main (int argc, char *argv[])
{
    // Create the Obcache testsuite.
    Suite *obcache_suite = suite_create ("Obcache");
    suite_add_tcase (obcache_suite, obcache_ctx_tcase ());
    suite_add_tcase (obcache_suite, ctx_free_obcache_tcase ());
    suite_add_tcase (obcache_suite, ctx_bound_obcache_tcase ());
    suite_add_tcase (obcache_suite, ob_ctx_free_tcase ());
    suite_add_tcase (obcache_suite, ob_ctx_bound_tcase ());
    suite_add_tcase (obcache_suite, ob_link_ctx_free_tcase ());
    suite_add_tcase (obcache_suite, ob_link_ctx_bound_tcase ());

    // Create the Vector testsuite.
    Suite *vec_suite = suite_create ("Vector");
    suite_add_tcase (vec_suite, vec_ctx_tcase ());
    suite_add_tcase (vec_suite, vec_basic_tcase ());
    suite_add_tcase (vec_suite, fill_read_vec_tcase ());
    suite_add_tcase (vec_suite, vecp_double_scan_tcase ());
    suite_add_tcase (vec_suite, double_vecp_scan_tcase ());

    // Create the Text testsuite.
    Suite *text_suite = suite_create ("Text");

    REGISTER_PARAM_2TCASES (text_suite, create_text_empty, "ASCII", "UTF-8");
    REGISTER_PARAM_2TCASES (text_suite,
    			    create_text_from_file, "ASCII", "UTF-8");
    REGISTER_TCASE (text_suite, create_text_errors);
    REGISTER_PARAM_2TCASES (text_suite, mark_off, "ASCII", "UTF-8");
    REGISTER_PARAM_2TCASES (text_suite, mark_off_errors, "ASCII", "UTF-8");
    REGISTER_PARAM_2TCASES (text_suite, marking, "ASCII", "UTF-8");
    REGISTER_PARAM_2TCASES (text_suite, marking_errors, "ASCII", "UTF-8");
    REGISTER_PARAM_2TCASES (text_suite, moving_marker, "ASCII", "UTF-8");
    REGISTER_PARAM_2TCASES (text_suite,
    			    moving_marker_errors, "ASCII", "UTF-8");
    REGISTER_PARAM_2TCASES (text_suite, iterator_reading, "ASCII", "UTF-8");
    REGISTER_PARAM_2TCASES (text_suite, marker_reading, "ASCII", "UTF-8");
    REGISTER_PARAM_2TCASES (text_suite,
    			    iterator_double_scan, "ASCII", "UTF-8");
    REGISTER_PARAM_2TCASES (text_suite,
    			    marker_double_scan, "ASCII", "UTF-8");
    REGISTER_PARAM_2TCASES (text_suite, insert, "ASCII", "UTF-8");
    REGISTER_PARAM_2TCASES (text_suite,
    			    insert_in_file, "ASCII", "UTF-8");
    REGISTER_PARAM_2TCASES (text_suite, erase, "ASCII", "UTF-8");
    REGISTER_PARAM_2TCASES (text_suite,
    			    bidirectionnal_upcase, "ASCII", "UTF-8");
    REGISTER_PARAM_2TCASES (text_suite, tag, "ASCII", "UTF-8");
    REGISTER_PARAM_2TCASES (text_suite, untag, "ASCII", "UTF-8");
    REGISTER_PARAM_2TCASES (text_suite, set_prop, "ASCII", "UTF-8");
    REGISTER_PARAM_2TCASES (text_suite,
			    iterator_nl_matching, "ASCII", "UTF-8");
    REGISTER_PARAM_2TCASES (text_suite,
			    iterator_nl_matching_errors, "ASCII", "UTF-8");
    REGISTER_PARAM_2TCASES (text_suite,
			    marker_nl_matching, "ASCII", "UTF-8");
    REGISTER_PARAM_2TCASES (text_suite,
			    marker_nl_matching_errors, "ASCII", "UTF-8");

    SRunner *obcache_srunner = srunner_create (obcache_suite);
    srunner_run_all (obcache_srunner, CK_NORMAL);

    SRunner *vec_srunner = srunner_create (vec_suite);
    srunner_run_all (vec_srunner, CK_NORMAL);

    SRunner *text_srunner = srunner_create (text_suite);
    srunner_run_all (text_srunner, CK_NORMAL);

    int nfailed = 0;
    nfailed += srunner_ntests_failed (obcache_srunner);
    nfailed += srunner_ntests_failed (vec_srunner);
    nfailed += srunner_ntests_failed (text_srunner);

    srunner_free (obcache_srunner);
    srunner_free (vec_srunner);
    srunner_free (text_srunner);

    return (nfailed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
