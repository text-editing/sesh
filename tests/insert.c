/* sesh - A text sequence implementation library
 * Copyright (C) 2012 Ludovic Stordeur */

#include <check.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

#include <safe-alloc.h>

#include "test-text.h"

START_TEST (check_size)
{
    /* This test inserts the content of a file in a text sequence through its
     * point then check the size of the text sequence. */

    fail_unless (gpl3.st.st_size == sesh_text_size (text));
}
END_TEST

START_TEST (check_len)
{
    /* This test inserts the content of a file in a text sequence through its
     * point then check the length of the text sequence. */

    fail_unless (gpl3.st.st_size == sesh_text_len (text));
}
END_TEST

START_TEST (point_persistance)
{
    /* This test inserts the content of a file at the end and check that the
     * point did not get perturbated. */

    fail_unless (sesh_itor_eq_p (sesh_iom (sesh_point (text)),
				 checked_text_end (text)));
}
END_TEST

START_TEST (iterator_forward_reading)
{
    /* This test inserts the content of a file in a text sequence through its
     * point. Then, it reads it back using an iterator. */

    char *output = byte_dump_text (text);
    fail_if (NULL == output);

    fail_unless (0 == memcmp (gpl3.ptr, output, (size_t)gpl3.st.st_size));

    FREE (output);
}
END_TEST

START_TEST (iterator_backward_reading)
{
    /* This test inserts the content of a file in a text sequence through its
     * point. Then, it reversely reads it back using an iterator. */

    char *output;
    fail_if (-1 == ALLOC_N (output, (size_t)gpl3.st.st_size));

    sesh_iterator_t it = sesh_text_rbegin (text);

    for (sesh_off_t i = 0; i < gpl3.st.st_size; sesh_bwd_itor (&it), i++)
	output[gpl3.st.st_size-1-i] = noattr_bytecell (it);

    fail_unless (sesh_itor_eq_p (checked_text_rend (text), it));

    fail_unless (0 == memcmp (gpl3.ptr, output, (size_t)gpl3.st.st_size));

    FREE (output);
}
END_TEST


void
insert_tcase (TCase *tcase, const char *enc)
{
    int enc_id = get_enc_id (enc);

    switch (enc_id) {
    case ENC_ID_ASCII:
	tcase_add_checked_fixture (tcase, setup_ascii_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_empty_ascii_text, teardown_text);
	break;
    case ENC_ID_UTF8:
	tcase_add_checked_fixture (tcase, setup_utf8_enc, NULL);
	tcase_add_checked_fixture (tcase,
				   setup_empty_utf8_text, teardown_text);
    }

    tcase_add_checked_fixture (tcase,
			       setup_gpl3_sample, teardown_gpl3_sample);
    tcase_add_checked_fixture (tcase, setup_insert_gpl3, NULL);

    tcase_add_test (tcase, check_size);
    tcase_add_test (tcase, check_len);
    tcase_add_test (tcase, point_persistance);
    tcase_add_test (tcase, iterator_forward_reading);
    tcase_add_test (tcase, iterator_backward_reading);
}
